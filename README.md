# moonhttpserver

#### 介绍
Moon Http Server(MHS) 是一个使用Pascal脚本的高性能Web服务器。 
该仓库中所有代码均为运行于MHS之上的官方站点代码。 
代码包括： 
公用库、教程、站点、演示、数据库等内容，可根据需要进行下载查看。 
如有更多意见或建议，可加入QQ群（67861996）进行讨论。
或者可以参阅官方网站中的博客：http://www.moonserver.cn/mhs/blog-style-1.pp

#### 软件架构
本仓库为站点代码，包含所有源码，其架构为：
bin
  ├wwwroot(Web主目录)
    ├admin(后台管理模块)
      ├...
    ├lessons(演示教程模块)
      ├...
    ├mhs(站点主模块)
      ├...
    ├public(脚本框架通用代码模块)
      ├...
    ├uploads(文件上传目录,目前存放网站图片资源)
      ├...    
  ├config.ini(MHS配置文件)
  ├ContentType.txt(Web内容配置文件)
  ├dedemos.db3(网站使用的数据库)
  ├doc(文档、帮助、说明目录)
    ├...


#### 安装教程

1. 请下载最新版MoonHttpServer可执行文件mhs.exe,并将该可执行文件放入下载后的bin目录中。
2. 点击mhs.exe并运行,出现服务器正常启动后窗口后,请在浏览器中输入web地址进行访问.
3. 请确认整个bin目录的路径中不包含中文字符串.

#### 使用说明

1. 目前仅有Windows版本，支持从WinXP SP2以上的操作系统版本。
2. 
3. 

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)