unit data;

interface

uses
  SysUtils, Classes, Variants, DB,
  BaseQuery in '..\public\BaseQuery.pp';

function GetArticle(AID: Integer; var AQuery: TWmQuery): Integer;

implementation

function GetArticle(AID: Integer; var AQuery: TWmQuery): Integer;
begin
  Result := TBaseQry.DbQuery(AQuery, 'select * from article where (id=:id)', VarArrayOf([AID]));
  if Result < 0 then
    Writeln(Format('Err(data.GetArticle):%d,%s', [AID, Request.RequestInfo.InContent]));
end;
{
function GetArticle(AID: Integer; var AQuery: TWmQuery): Integer;
begin
  Result := -1;
  
  AQuery := wm.NewQuery('');
  if AQuery.Active then
    AQuery.Close;

  AQuery.SQL.Text := 'select * from article where (id=:id)';
  AQuery.ParamByName('id').Value := AID;
  try
    AQuery.Open;
	
	Result := AQuery.RecordCount;
  except
    on E: Exception do
	begin
	  writeln(Format('Err(data.GetArticle):%s,%s', [E.Message, Request.RequestInfo.InContent]));
	end;
  end;  
end;
}

end.