<?
unit uView_login;

interface

uses
  SysUtils, Classes,  DB,
  BaseQuery in '..\public\BaseQuery.pp', 
  Variants;
  
type
  TLoginView = class
  public
    class function ResponseCookie(AName, AValue: string; ATimeOutSecond: Integer = 3600; 
      ADomain: string = ''; APath: string = ''; ASecure: Boolean = False): Integer;
    class procedure Redirect(AUrl: string);
  public
	class function IsLogin: Boolean;
	class function CheckLogin(AUserName, APassword: string; var sErr: string): Boolean;
	class function Logout: Boolean;
  end;
  
implementation

class function TLoginView.ResponseCookie(AName, AValue: string; ATimeOutSecond: Integer = 3600; 
  ADomain: string = ''; APath: string = ''; ASecure: Boolean = False): Integer;
const
  C_ONESECOND = 1 / (24 * 3600);  
begin
  with Response.Cookies.Add do
  begin
    Name := AName;
    Value := AValue;
    if ATimeOutSecond <> 0 then
      Expires := Now + C_ONESECOND * ATimeOutSecond;
    //Domain := '';
    //Path := '';
    //Secure := False; 
  end; 
  Result := 0;  
end;  

class procedure TLoginView.Redirect(AUrl: string);
begin
  Response.CustomHeaders.Text := 'location: ' + AUrl;
  Response.StatusCode := 302;
end;

class function TLoginView.CheckLogin(AUserName, APassword: string; var sErr: string): Boolean;
var
  ASessionID: string;
  ASession: TScpSession;
  sName, sPassword: string;
  sTemp: string;
  iSessionTimes: Integer;
  AQuery: TwmQuery;
begin
  sErr := '';
  
  Result := False;

  if (AUserName = '') or (APassword = '') then
    Exit;
  
  //Result := (AUserName = 'admin') and (APassword = 'admin');
  sPassword := MD5Hex('mhs' + APassword + 'server'); //加盐后:admin=2922EF425907819588FE5FA209520C9D

  TBaseQry.DbQuerySel(AQuery, 'id', 'users', '(name=:name) and (password=:password) and (enabled=1)', VarArrayOf([AUserName, sPassword]));
  Result := AQuery.RecordCount = 1;
  
  if Result then //写入Session,返回cookie
  begin
    iSessionTimes := 3600 * 4; //One hour

    //创建sessionid
    ASessionID := LowerCase(Format('%x', [rand64]));//'mytestsessionid';

    ASession := gSessions.StartSession(ASessionID, iSessionTimes); //Time out after one hour
	ASession.Values['Name'] := AUserName;
	//需要从数据库中查找并填充的其它信息
	ASession.Values['UserID'] := AUserName;
	
	//写入并返回cookie
	ResponseCookie('sessionid', ASessionID, iSessionTimes);
    //多个Session值需要自己手工处理
    //sTemp := ASessionID + '&' + 'testid=' + IntToStr(Random($7FFFFFFF));
	//ResponseCookie('sessionid', sTemp, iSessionTimes);
  end
  else
  begin
    sErr := '用户名或者密码错误.';
  end;
end;

class function TLoginView.Logout: Boolean;
var
  sID: string;
begin
  Result := False;
  
  if Request.CookieFields.Count > 0 then
  begin
    sID := Request.CookieFields.Values['sessionid'];
    gSessions.EndSession(sID);
	
	Result := True;
  end;
end;

class function TLoginView.IsLogin: Boolean;
var
  sID: string;
  ASession: TScpSession;
begin
  Result := False;
  //根据cookie查找session,如果找到则返回true
  if Request.CookieFields.Count > 0 then
  begin
    sID := Request.CookieFields.Values['sessionid'];
	
	ASession := gSessions.GetSession(sID);
	Result := Assigned(ASession);
	
    {
	if Result then
	  Writeln('Get Session OK')
	else
	  Writeln('Session fail.');
    //}  
  end;
end;


end. 
?>