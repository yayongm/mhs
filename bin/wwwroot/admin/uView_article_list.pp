<?
unit uView_article_list;

interface

uses
  SysUtils, Classes,  DB,
  BaseQuery in '..\public\BaseQuery.pp',
  Variants;
  
type
  TArticleListView = class
  public
    class function GetArticleList(const ACatalog: Integer = -1): Integer;
  end;
  
implementation

  function GetCatalogText(const AValue: Integer): string;
  begin
    case AValue of
      0: Result := '幻灯';
      1: Result := '新闻';
      2: Result := '博客';
      3: Result := '教程';
      4: Result := '演示';
      5: Result := '高级';
      6: Result := '联系我们';
      7: Result := '关于';
      else
        Result := '未知';
    end;
  end;
  function GetTypeText(const AValue: Integer): string;
  begin
    case AValue of
      0: Result := '固顶';
      1: Result := '重要';
      2: Result := '普通';
      3: Result := '隐藏';
      4: Result := '删除';
      else
        Result := '未知';
    end;
  end;

class function TArticleListView.GetArticleList(const ACatalog: Integer): Integer;
var
  AQuery: TWmQuery;
  iCount: Integer;
  iCatalog: Integer;
  sTemp: string;
  
  i: Integer;
  iCountPerPage: Integer;
begin
  Result := -1;
  iCountPerPage := 100;

  if ACatalog < 0 then
    sTemp := '(1=1)'
  else
    sTemp := Format('(catalog=%d)', [ACatalog]);  
  
  if TBaseQry.DbQuerySel(AQuery, 'Count(*)', 'article', sTemp, 
    VarArrayOf([])) < 1 then
    Exit;
  Result := AQuery.Fields[0].AsInteger;  

  if TBaseQry.DbQuerySel(AQuery, '*', 'article', sTemp + ' order by id desc limit :Start, :End', 
    VarArrayOf([0, iCountPerPage])) < 1 then
    Exit;
  
  try  
    if Result < 1 then  //no data
    begin
      writeln('no data.');
      Exit;
    end;
  
    AQuery.DataSet.First;
    while not AQuery.DataSet.Eof do
    begin
      println '<tr>';
      println '<td>' + AQuery.FS['id'] + '</td>';
      println '<td><a href="article.pp?id=' + AQuery.FS['id'] + '">' + AQuery.FS8['title'] + '</a></td>';
      println '<td>' + GetCatalogText(AQuery.FI['catalog']) + '</td>';
      println '<td>' + GetTypeText(AQuery.FI['type']) + '</td>';
      println '<td>' + FormatDateTime('yyyy-mm-dd hh:nn:ss', AQuery.FD['createtime']) + '</td>';
      println '</tr>';    

      AQuery.DataSet.Next;
    end;

  finally  
    FreeAndNil(AQuery);
  end;
  
end;

end. 
?>