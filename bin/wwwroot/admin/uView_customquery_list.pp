<?
unit uView_customquery_list;

interface

uses
  SysUtils, Classes, DB,
  SuperObject,
  BaseQuery in '..\public\BaseQuery.pp',
  Variants;
  
type
  TCustomQueryView = class
  public
    class function GetDataList(const ASQL: string = ''): Integer;
    class function GetDataListAjax(const ASQL: string = ''): Integer;
  end;
  
implementation

class function TCustomQueryView.GetDataList(const ASQL: string): Integer;
var
  AQuery: TWmQuery;
  iCount: Integer;
  iCatalog: Integer;
  sTemp: string;
  
  i: Integer;
  iCountPerPage: Integer;
  
  sSQL: string;
  sError: string;
begin
  Result := -1;
  iCountPerPage := 100;

  sSQL := Trim(ASQL);
  if sSQL = '' then
    Exit;
  
  sError := '';  
  
  if TBaseQry.DbQueryWithError(AQuery, sSQL, VarArrayOf([]), sError) < 0 then
  begin
    println sError;
    Exit;
  end;  

  try  
    println '                                      <tr>';
    for i := 0 to AQuery.DataSet.FieldCount - 1 do
    begin
      print '                                            <th>' + DbUtf8(AQuery.DataSet.Fields[i].FieldName) + '</th>';
    end;
    print '                                        </tr>';
    
    if AQuery.RecordCount < 1 then  //no data
    begin
      writeln('no data.');
      Exit;
    end;

    print '                                        <tbody>';

    AQuery.DataSet.First;
    while not AQuery.DataSet.Eof do
    begin
      println '<tr>';

      for i := 0 to AQuery.DataSet.FieldCount - 1 do
      begin
        println '<td>' + DbUtf8(AQuery.Fields[i].AsString) + '</td>';
      end;  
      //println '<td>' + FormatDateTime('yyyy-mm-dd hh:nn:ss', AQuery.FD['createtime']) + '</td>';

      println '</tr>';    

      AQuery.DataSet.Next;
    end;
    print '                                        </tbody>';
  finally  
    FreeAndNil(AQuery);
  end;
end;

function GetErrJson(AError: string; ACode: Integer = 0): string;
var
  ASO: ISuperObject;
begin
  ASO := SO('{}');
  try
    ASO.I['result'] := ACode;
    ASO.S['error'] := AError;
    Result := ASO.AsString;

    //Result := Result;
  finally
    ASO := nil;
  end;
end;

function GetTableFieldJson(ADataSet: TDataSet): string;
var
  ASO, AFT: ISuperObject;
  i: Integer;
  AField: TField;
begin
  ASO := SO('{}');
  try
    //先加入字段描述
    ASO.O['colist'] := SA([]);
    for i := 0 to ADataSet.FieldCount - 1 do
    begin
      AFT := SO('{}');
      AFT.S['column_en'] := ADataSet.Fields[i].FieldName;
      AFT.S['view_name_cn'] := ADataSet.Fields[i].FieldName;
      ASO.A['colist'].Add(AFT);
    end;
    
    //加入数据
    ASO.O['data'] := SA([]);

    ADataSet.First;
    while not ADataSet.Eof do
    begin
      AFT := SO('{}');
      for i := 0 to ADataSet.FieldCount - 1 do
      begin
        AField := ADataSet.Fields[i];
        
        AFT.S[AField.FieldName] := AField.AsString;
      end;
      ASO.A['data'].Add(AFT);
    
      ADataSet.Next;
    end;
    
    //返回结果为成功
    ASO.I['result'] := 1;
    
    Result := ASO.AsString;
    //注意:当字符串中出现特殊符号时是非常坑爹的!!!!
    Result := StringReplaceAll(Result, #9, '%x09');//制表符
    Result := StringReplaceAll(Result, #13, '%x0D');//回车
    Result := StringReplaceAll(Result, #10, '%x0A');//换行符
    
    Result := DbUtf8(Result);
  finally
    ASO := nil;
  end;
end;

class function TCustomQueryView.GetDataListAjax(const ASQL: string): Integer;
var
  AQuery: TWmQuery;
  iCount: Integer;
  iCatalog: Integer;
  sTemp: string;
  
  i: Integer;
  iCountPerPage: Integer;
  
  sSQL: string;
  sError: string;
begin
  Result := -1;
  iCountPerPage := 100;

  sSQL := Trim(ASQL);
  if sSQL = '' then
    Exit;
  
  sError := '';  
  
  if TBaseQry.DbQueryWithError(AQuery, sSQL, VarArrayOf([]), sError) < 0 then
  begin
    println GetErrJson(sError);
    Exit;
  end;  

  try
    
    println GetTableFieldJson(AQuery.DataSet);

  finally  
    FreeAndNil(AQuery);
  end;
end;

end. 
?>