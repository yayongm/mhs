<?
uses
  SysUtils, Classes, Variants, DB,
  BaseQuery in '..\public\BaseQuery.pp', 
  BaseDebug in '..\public\BaseDebug.pp',
  data, view;

function UpdateArticle(AID: Integer): Integer;
var
  AQuery: TWmQuery;
  bIsNew: Boolean;
begin
  Result := GetArticle(AID, AQuery);
  
  if Result < 0 then  
  begin
    writeln('Err(add.UpdateArticle):DataSet is not Open.');
    
	Exit;
  end;	
	
  bIsNew := AQuery.RecordCount < 1;	
  
  if bIsNew then
  begin
	AQuery.DataSet.Append;
  end
  else
  begin  
    AQuery.DataSet.Edit;
  end;	
	
  //根据ContentFields填充字段
  TBaseQry.FillRecordFromReq(AQuery, False);
  
  //填充Key
  if bIsNew then
  begin
    AQuery.FV['id'] := TBaseQry.GetMaxID('article', 'id') + 1;
	AQuery.FV['createtime'] := Now;
	AQuery.FV['modifytime'] := Now;
  end
  else
  begin
	AQuery.FV['modifytime'] := Now;
  end;  
	
  try
    AQuery.Post;
	Result := AQuery.FV['id'];
  except
    on E: Exception do
	begin
      writeln(Format('Info(UpdateArticle):E:%s,Value:%s', [E.Message, Request.RequestInfo.InContent]));
	end;
  end;  
end;

procedure DoArticle;
var
  iID, iTemp: Integer;
  sTemp: string;
begin  
  iID := StrToIntDef(Request.QueryFields.Values['id'], -1);
  iTemp := StrToIntDef(Request.ContentFields.Values['id'], -1);

  //print TBaseDebug.DebugReq;

  //{
  if (iID = iTemp) then //check the data
    iID := UpdateArticle(iID);
  
  ViewArticle(iID);
  //}
end;

DoArticle;

 
?>