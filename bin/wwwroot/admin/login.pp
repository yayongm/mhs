<?
uses
  SysUtils, Classes, Variants,
  BaseDebug in '..\public\BaseDebug.pp',
  uView_login;

var
  sLoginErr: string;
  
function DoLogin: Boolean;
var
  sAct: string;
begin
  Result := False;
  
  sAct := Request.QueryFields.Values['act'];
  //判断是否登出
  if SameText(sAct, 'logout') then
  begin
    TLoginView.Logout;
  end
  else //登录
  begin
    //判断是否已登录
    if TLoginView.IsLogin then
    begin
      //登录成功,重定向
      TLoginView.Redirect('index.pp');
    
      Result := True;
      
      Exit;
    end;

    //判断密码是否正确
    if TLoginView.CheckLogin(Request.ContentFields.Values['email'], Request.ContentFields.Values['password'], sLoginErr) then
    begin
      //登录成功,重定向
      TLoginView.Redirect('index.pp');
    
      Result := True;
    
      Exit;
    end;
  end;
end;

procedure DoShowLoginErr(AError: string);
begin
  if sLoginErr <> '' then
  begin
?>
    <script>
        $(document).ready(function(){
            showError("<? print AError; ?>");
        });
    </script>
<?
  end;
end;

if DoLogin then
begin
  Exit;
end
else
begin
  
print wm.Include('header.tpl', '{title}=欢迎来到MTH | 登录');

?>
</head>

<? {print TBaseDebug.DebugReq; //} ?>

<body>

<?
  //登录失败,显示错误.
  DoShowLoginErr(sLoginErr);
?>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">欢迎来到Moon Http Server</h3>
                    </div>
                    <div class="panel-body">
                        <form id="form" role="form" method="post" action="login.pp?act=login">
                            <fieldset>

                                <!--
                                <div class="alert alert-info alert-dismissable">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                    用户名:admin<br>
									密码:admin
                                </div>
                                -->
                            

                                <div class="form-group">
                                    <input class="form-control" placeholder="用户名/邮箱名" name="email" type="email" autofocus>
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="密码" name="password" id="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember">下次自动登录
                                    </label>
                                </div>


								<div id="infotext" class="alert alert-danger" style="display: none;">
								</div>


                                <!-- Change this to a button or input when using this as a form -->
                                <a class="btn btn-lg btn-success btn-block" onclick="checkForm();">登录</a>
								
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<script>
function showErrorEx(ALabelName, AText){
    if (AText == null){
        $(ALabelName).removeAttr("style");
    }else{
        $(ALabelName).text(AText).attr("style", "");
    }
}

function showError(AText){
  showErrorEx("#infotext", AText);
}

function checkForm(){
	var sTemp = $("input[name='email']").val();
	if (sTemp == "") {
		$("#infotext").text("用户名不能为空").attr("style", "");
		return false;
	}

	var sTemp = $("input[name='password']").val();
	if (sTemp == "") {
		$("#infotext").text("密码不能为空").attr("style", "");
		return false;
	}

	$("#form").submit();
}

$('#password').bind('keydown',function(event){
	            if(event.keyCode == "13")    
	            {
	             	checkForm();
	            }
    });

</script>

<?
  print wm.Include('footer.tpl');
?>

</body>

</html>

<?
end;
?>
