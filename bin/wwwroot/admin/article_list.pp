<?
uses
  SysUtils, Classes, Variants,
  //BaseDebug in '..\public\BaseDebug.pp',
  uView_index, uView_article_list,
  uView_login;
  
//判断是否已登录
if not TLoginView.IsLogin then
begin
  TLoginView.Redirect('login.pp');
  
  Exit;
end;

print wm.Include('header.tpl', '{title}=MTH管理控制台 | 首页');

?>

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

</head>

<? {print TBaseDebug.DebugReq; //} ?>

<body>


    <div id="wrapper">

        <? TIndexView.GetNavigation; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">文档列表</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            文档列表
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <p>
                                <button type="button" class="btn btn-primary" onclick="location.href='article.pp'">新建</button>      
                            </p>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover width:100%" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>标题</th>
                                            <th>目录</th>
                                            <th>类型</th>
                                            <th>创建时间</th>
                                        </tr>
                                        <tbody>
                                        <? 
                                          TArticleListView.GetArticleList(StrToIntDef(Request.QueryFields.Values['catalog'], -1)); 
                                        ?>
                                        </tbody>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?
  print wm.Include('footer.tpl');
?>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true,
            aLengthMenu: [[25, 10, 50, 100, -1], [25, 10, 50, 100, "All"]],
            order: [ 0, 'dsc' ]
        });
    });
    </script>

</body>

</html>