﻿<?
unit uView_article;

interface

uses
  SysUtils, Classes,  DB,
  BaseQuery in '..\public\BaseQuery.pp',
  BaseDebug in '..\public\BaseDebug.pp',
  BaseUI in '..\public\BaseUI.pp',
  Variants;
  
type
  TArticleView = class
  public
    class function GetArticle(const AID: Integer; var AQuery: TWmQuery): Integer;
    class function UpdateArticle(AID: Integer): Integer;
    class procedure DoArticle;
    class function ViewArticle(AID: Integer = -1): string;
  end;
  
implementation



class function TArticleView.GetArticle(const AID: Integer; var AQuery: TWmQuery): Integer;
begin
  Result := TBaseQry.DbQuery(AQuery, 'select * from article where (id=:id)', VarArrayOf([AID]));
  if Result < 0 then
    Writeln(Format('Err(data.GetArticle):%d,%s', [AID, Request.RequestInfo.InContent]));
end;

class function TArticleView.UpdateArticle(AID: Integer): Integer;
var
  AQuery: TWmQuery;
  bIsNew: Boolean;
begin
  Result := GetArticle(AID, AQuery);
  
  if Result < 0 then  
  begin
    writeln('Err(add.UpdateArticle):DataSet is not Open.');
    
	Exit;
  end;	
	
  bIsNew := AQuery.RecordCount < 1;	
  
  if bIsNew then
  begin
	AQuery.DataSet.Append;
  end
  else
  begin  
    AQuery.DataSet.Edit;
  end;	
	
  //根据ContentFields填充字段
  TBaseQry.FillRecordFromReq(AQuery, False);
  
  //填充Key
  if bIsNew then
  begin
    AQuery.FI['id'] := TBaseQry.GetMaxID('article', 'id') + 1;
	AQuery.FI['wordcount'] := 0;
    AQuery.FI['viewcount'] := 0;
    AQuery.FD['createtime'] := Now;
	AQuery.FD['modifytime'] := Now;
  end
  else
  begin
	AQuery.FD['modifytime'] := Now;
  end;  
	
  try
    AQuery.Post;
	Result := AQuery.FI['id'];
  except
    on E: Exception do
	begin
      writeln(Format('Info(UpdateArticle):E:%s,Value:%s', [E.Message, Request.RequestInfo.InContent]));
	end;
  end;  
end;

class procedure TArticleView.DoArticle;
var
  iID, iTemp: Integer;
  sTemp: string;
begin  
  iID := StrToIntDef(Request.QueryFields.Values['id'], -1);
  iTemp := StrToIntDef(Request.ContentFields.Values['id'], -1);

  //print TBaseDebug.DebugReq;

  sTemp := Request.ContentFields.Values['title'];
  //if (iID = iTemp) and (iID > 0) then //check the data
  if (iID = iTemp) and (Length(sTemp) > 0) then
    iID := TArticleView.UpdateArticle(iID);
  
  TArticleView.ViewArticle(iID);
end;

class function TArticleView.ViewArticle(AID: Integer = -1): string;

  function GetCatalogCombo(ASelValue: string): string;
  begin
    Result := TBaseUI.GetSelCombo(VarArrayOf(['0', '1', '2', '3', '4', '5', '6', '7']), 
      MnVarArrayOf(['幻灯', '新闻', '博客', '教程', '案例', '高级', '联系我们', '关于']), 
	  ASelValue, 'name="catalog" id="catalog"  class="form-control"'); 
  end;
  function GetTypeCombo(ASelValue: string): string;
  begin
    Result := TBaseUI.GetSelCombo(VarArrayOf(['0', '1', '2', '3', '4']), 
      MnVarArrayOf(['固顶', '重要', '普通', '隐藏', '删除']), 
	  ASelValue, 'name="type" id="type" class="form-control"'); 
  end;
  
var
  AQuery: TWmQuery;
  iID: Integer;
  sID: string;
begin
  if AID > 0 then  
    iID := AID
  else
    iID := StrToIntDef(Request.QueryFields.Values['id'], -1);
  
  if iID > 0 then  
    sID := IntToStr(iID)
  else
    sID := '';  
  
  GetArticle(iID, AQuery);
?>

  <form role="form" method="post" action="article.pp?id=<? print sID; ?>">
  <input type="hidden" name="id" id="id" value="<? print sID; ?>"/>
    <div class="form-group">
        <label>标题：</label>
        <input class="form-control" placeholder="请输入文档标题" name="title" id="title" value="<? print DbUtf8(AQuery.FS['title']); ?>"/>
    </div>
    <div class="form-group">
        <label>关键字：</label>
        <input class="form-control" placeholder="请输入关键字，多个关键字用英文逗号分割" name="keywords" id="keywords" value="<? print DbUtf8(AQuery.FS['keywords']); ?>"/>
    </div>
    <div class="form-group">
        <label>标准图：</label>
        <input class="form-control" placeholder="图片链接" name="normalimg" id="normalimg" value="<? print DbUtf8(AQuery.FS['normalimg']); ?>"/>
    </div>
    <div class="form-group">
        <label>缩略图：</label>
        <input class="form-control" placeholder="缩略图链接" name="thumbnail" id="thumbnail" value="<? print DbUtf8(AQuery.FS['thumbnail']); ?>"/>
    </div>

    <div class="form-group">
        <label>概要：</label>
        <textarea class="form-control" rows="3" name="summary" id="summary"/><? print DbUtf8(AQuery.FS['summary']); ?></textarea>
    </div>
    <div class="form-group">
        <label>内容：</label>
        <textarea class="form-control" rows="30" name="content" id="content"/><? print DbUtf8(AQuery.FS['content']); ?></textarea>
    </div>
    <div class="form-group">
        <label>目录：</label>
        <? print GetCatalogCombo(AQuery.FS['catalog']); ?>
    </div>
    <div class="form-group">
        <label>状态：</label>
        <? print GetTypeCombo(AQuery.FS['type']); ?>
    </div>
    <div class="form-group">
        <label>字数：</label>
        <input class="form-control" placeholder="字数" name="wordcount" id="wordcount" value="<? print DbUtf8(AQuery.FS['wordcount']); ?>"/>
    </div>
    <div class="form-group">
        <label>浏览数：</label>
        <input class="form-control" placeholder="浏览数" name="viewcount" id="viewcount" value="<? print DbUtf8(AQuery.FS['viewcount']); ?>">
    </div>
    
<input type="submit"/>

</form>

		<script type="text/javascript">
			window.onload = function()
				{
					CKEDITOR.replace('content');
				};
		</script>   

<? 
  if Assigned(AQuery) then
    FreeAndNil(AQuery); //释放,如果不释放则由wm(WebModule)自动释放
	
end;

end. 
?>