<?
uses
  SysUtils, Classes, 
  //BaseDebug in '..\..\public\BaseDebug.pp',
  Variants;

procedure DoUploadImage;
var
  sTemp, sExt: string;
  
  sData, sDataFileName, sContentType: string;
  sTest: string;
  vFiles, AFile: Variant;
begin
  Response.ContentType := 'application/json';
  if ExtractUploadFile(Request.RequestInfo.InContent, vFiles) > 0 then
  begin
    {
	AUFI.Data, AUFI.DataFileName, AUFI.Content_Type,
    AUFI.DataTypeName, AUFI.DataName, AUFI.Key, AUFI.Content_Dispositon
	}
	AFile := vFiles[0];//First file
	sData := VariantToRawStr(AFile[0]);
	sDataFileName := AFile[1];
	sContentType := AFile[2];
	
	sDataFileName := StringReplaceAll(sDataFileName, '"', '');
	{
	sExt := ExtractFileExt(sDataFileName);
	sDataFileName := FormatDateTime('yyyymmdd_hhnnss', Now) + sExt;
	//}
	sTemp := FullAppFile('_$PATHUPLOADS_' + sDataFileName);
	if StrToFile(sData, sTemp) > 0 then //ok
	begin
	  sTest := '{"fileName":"' + Utf8Encode(sDataFileName) + '","uploaded":1,"url":' + 
	    '"../uploads/' + Utf8Encode(sDataFileName) + '"}';
	  
	  print sTest;
	end;
  end;
end;

DoUploadImage;
?>