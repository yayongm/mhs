<?
uses
  SysUtils, Classes, Variants,
  //BaseDebug in '..\public\BaseDebug.pp',
  uView_index, uView_article,
  uView_login;
  
//判断是否已登录
if not TLoginView.IsLogin then
begin
  TLoginView.Redirect('login.pp');
  
  Exit;
end;

print wm.Include('header.tpl', '{title}=MTH管理控制台 | 编辑文档');

?>

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <script src="ckeditor/ckeditor.js"></script>
    
</head>

<? {print TBaseDebug.DebugReq; //} ?>

<body>


    <div id="wrapper">

        <? TIndexView.GetNavigation; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">编辑文档</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>



        <? TArticleView.DoArticle; ?>



        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?
  print wm.Include('footer.tpl');
?>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true
        });
    });
    </script>

</body>

</html>