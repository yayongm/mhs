<?
unit view;

interface

uses
  SysUtils, Classes, Variants, DB,
  BaseQuery in '..\public\BaseQuery.pp', 
  BaseUI in '..\public\BaseUI.pp',
  data;

function ViewArticle(AID: Integer = -1): string;

implementation


?>


<?
function ViewArticle(AID: Integer): string;
  
  function GetCatalogCombo(ASelValue: string): string;
  begin
    Result := TBaseUI.GetSelCombo(VarArrayOf(['0', '1', '2', '3', '4', '5', '6', '7']), 
      MnVarArrayOf(['首页幻灯', '首页', '博客', '教程', '演示', '高级', '联系我们', '关于']), 
	  ASelValue, 'name="catalog" id="catalog"'); 
  end;
  function GetTypeCombo(ASelValue: string): string;
  begin
    Result := TBaseUI.GetSelCombo(VarArrayOf(['0', '1', '2', '3', '4']), 
      MnVarArrayOf(['固顶', '重要', '普通', '隐藏', '删除']), 
	  ASelValue, 'name="type" id="type"'); 
  end;
  
var
  AQuery: TWmQuery;
  iID: Integer;
begin
  if AID > 0 then  
    iID := AID
  else	
    iID := StrToIntDef(Request.QueryFields.Values['id'], -1);
  
  if GetArticle(iID, AQuery) > 0 then
  begin
?>
<html>
<head>
<meta charset="utf-8">
<script src="ckeditor/ckeditor.js"></script>
<title><? print DbUtf8(AQuery.FieldByName('title').AsString); ?></title>
</head>
<body>

  <form method="post" action="add.pp?id=<? print iID; ?>">
  <input type="hidden" name="id" id="id" value="<? print iID; ?>"/>
标题：<input type="text" name="title" id="title" value="<? print DbUtf8(AQuery.FieldByName('title').AsString); ?>"/><br>
关键字：<input type="text" name="keywords" id="keywords" value="<? print DbUtf8(AQuery.FieldByName('keywords').AsString); ?>"/><br>
标准图：<input type="text" name="normalimg" id="normalimg" value="<? print DbUtf8(AQuery.FieldByName('normalimg').AsString); ?>"/><br>
缩略图：<input type="text" name="thumbnail" id="thumbnail" value="<? print DbUtf8(AQuery.FieldByName('thumbnail').AsString); ?>"/><br>
概要：<textarea name="summary" id="summary"/><? print DbUtf8(AQuery.FieldByName('summary').AsString); ?></textarea><br>
内容：<textarea name="content" id="content"/><? print DbUtf8(AQuery.FieldByName('content').AsString); ?></textarea><br>
目录：<? print GetCatalogCombo(AQuery.FieldByName('catalog').AsString); ?>
类型：<? print GetTypeCombo(AQuery.FieldByName('type').AsString); ?>
字数：<input type="text" name="wordcount" id="wordcount" value="<? print DbUtf8(AQuery.FieldByName('wordcount').AsString); ?>"/><br>
浏览数：<input type="text" name="viewcount" id="viewcount" value="<? print DbUtf8(AQuery.FieldByName('viewcount').AsString); ?>"/><br>
<input type="submit"/>

</form>

		<script type="text/javascript">
			window.onload = function()
				{
					CKEDITOR.replace('content');
				};
		</script>   
</body>
</html>
<?  
  end
  else
  begin
?>
<html>
<head>
<meta charset="utf-8">
<script src="ckeditor/ckeditor.js"></script>
<title>Hello,World</title>
</head>
<body>

  <form method="post" action="add.pp">
  <input type="hidden" name="id" id="id" value=""/>
标题：<input type="text" name="title" id="title"/><br>
关键字：<input type="text" name="keywords" id="keywords"/><br>
标准图：<input type="text" name="normalimg" id="normalimg"/><br>
缩略图：<input type="text" name="thumbnail" id="thumbnail"/><br>
概要：<textarea name="summary" id="summary"/></textarea><br>
内容：<textarea name="content" id="content"/></textarea><br>
目录：<? print GetCatalogCombo('0'); ?>
类型：<? print GetTypeCombo('0'); ?>
字数：<input type="text" name="wordcount" id="wordcount"/><br>
浏览数：<input type="text" name="viewcount" id="viewcount"/><br>
<input type="submit"/>

</form>

		<script type="text/javascript">
			window.onload = function()
				{
					CKEDITOR.replace('content');
				};
		</script>   

</body>
</html>
<? 
  if Assigned(AQuery) then
    FreeAndNil(AQuery); //释放,如果不释放则由wm(WebModule)自动释放
	
  end;
end;

end.
?>