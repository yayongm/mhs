<?
uses
  SysUtils, Classes, Variants,
  //BaseDebug in '..\public\BaseDebug.pp',
  BaseUI in '..\public\BaseUI.pp',
  uView_index, uView_customquery_list,
  uView_login;
  
//判断是否已登录
if not TLoginView.IsLogin then
begin
  TLoginView.Redirect('login.pp');
  
  Exit;
end;

print wm.Include('header.tpl', '{title}=MTH管理控制台 | 自定义查询');

?>

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

</head>

<? {print TBaseDebug.DebugReq; //} ?>

<body>


    <div id="wrapper">

        <? TIndexView.GetNavigation; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">自定义查询</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                    <label>常用SQL语句</label>
                                    <select id="default_sql" class="form-control">
                                        <option> </option>
                                        <option>select id,ip,url,refer,createtime from visit_log order by id desc limit 0,100;</option>
                                        <option>select id,ip,url,useragent,createtime from visit_log order by id desc limit 0,100;</option>
                                        <option>select id,title,catalog,viewcount,createtime from article order by id desc;</option>
                                        <option>select count(id) from visit_log where (createtime>'<? TBaseUI.PrintDateTime(Trunc(Now)); ?>');</option>
                                        <option>select count(id) from visit_log where (createtime>'<? TBaseUI.PrintDateTime(Trunc(Now - 1)); ?>') and (createtime<'<? TBaseUI.PrintDateTime(Trunc(Now)); ?>');</option>
                                        <option>select * from usercomment order by id desc limit 0,100;</option>
                                    </select>
                                    <form role="form" action="customquery_list.pp" method="post">
                                        <div class="form-group">
                                            <label>自定义SQL语句</label>
                                            <textarea class="form-control" id="sql" name="sql" rows="5"><? print mRPV('sql'); ?></textarea>
                                        </div>
                                        <button type="submit" class="btn btn-default">查询</button>
                                    </form>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <p>
                                <!--
                                <button type="button" class="btn btn-primary" onclick="location.href='article.pp'">新建</button>      
                                -->
                            </p>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover width:100%" id="dataTables-example">
                                    <thead>
                                        <?
                                        TCustomQueryView.GetDataList(mRPV('sql'));
                                        ?>
                                    </thead>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?
  print wm.Include('footer.tpl');
?>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    $(document).ready(function() {
        $('#dataTables-example').DataTable({
            responsive: true,
            paging : true,
            bLengthChange: true,
            aLengthMenu: [[25, 10, 50, 100, -1], [25, 10, 50, 100, "All"]],
            order: [ 0, 'dsc' ],
            language: {
                "lengthMenu": "每页_MENU_ 条记录",
                "zeroRecords": "没有找到记录",
                "info": "第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                "infoEmpty": "无记录",
                "search": "搜索：",
                "infoFiltered": "(从 _MAX_ 条记录过滤)",
                "paginate": {
                    "previous": "上一页",
                    "next": "下一页"
                }
            }
        });
    });
    
    $('#default_sql').on('change',function(event){
        $('#sql').val($('#default_sql').val());
        });
    
    </script>

</body>

</html>