<?
uses
  SysUtils, Classes, Variants,
  //BaseDebug in '..\public\BaseDebug.pp',
  BaseUI in '..\public\BaseUI.pp',
  uView_index, uView_customquery_list,
  uView_login;
  
//判断是否已登录
if not TLoginView.IsLogin then
begin
  TLoginView.Redirect('login.pp');
  
  Exit;
end;

if mRPV('act')='ajax' then
begin
  //Response.Content := '{"ret": 0,"error": "错误的内容","colist":["name","value"]}';
  //Response.Content := '{"result": 1,"colist":[{"column_en":"id","view_name_cn":"编号"},{"view_name_cn":"来源"},{"view_name_cn":"客户名称"},{"view_name_cn":"故障主题"}],"data":[{"id":1}]}';

  TCustomQueryView.GetDataListAjax(mRPV('usersql'));
  Response.ContentType := 'application/json';
  
  Exit;
end;

print wm.Include('header.tpl', '{title}=MTH管理控制台 | 自定义查询');

?>

    <!-- DataTables CSS -->
    <link href="vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

</head>

<? {print TBaseDebug.DebugReq; //} ?>

<body>


    <div id="wrapper">

        <? TIndexView.GetNavigation; ?>

        <div id="page-wrapper">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">自定义查询</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>

            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                                    <label>常用SQL语句</label>
                                    <select id="default_sql" class="form-control">
                                        <option> </option>
                                        <option>select id,ip,url,refer,createtime from visit_log order by id desc limit 0,100;</option>
                                        <option>select id,ip,url,useragent,createtime from visit_log order by id desc limit 0,100;</option>
                                        <option>select id,title,catalog,viewcount,createtime from article order by id desc;</option>
                                        <option>select count(id) from visit_log where (createtime>'<? TBaseUI.PrintDateTime(Trunc(Now)); ?>');</option>
                                        <option>select count(id) from visit_log where (createtime>'<? TBaseUI.PrintDateTime(Trunc(Now - 1)); ?>') and (createtime<'<? TBaseUI.PrintDateTime(Trunc(Now)); ?>');</option>
                                        <option>select * from usercomment order by id desc limit 0,100;</option>
                                    </select>
                                            <label>自定义SQL语句</label>
                                            <textarea class="form-control" id="sql" rows="5">select * from users</textarea>
                                            <br>
                                            <button class="btn btn-default" id="searchBtn">查询</button>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <p>
                                <!--
                                <button type="button" class="btn btn-primary" onclick="location.href='article.pp'">新建</button>      
                                -->
                            </p>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover width:100%" id="dataTables-example">
                                        <thead>
                                            <tr id="colTr">
                                           </tr>
                                        </thead>
                                        <tbody id="colTb">  
                                        </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

<?
  print wm.Include('footer.tpl');
?>

    <!-- DataTables JavaScript -->
    <script src="vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Page-Level Demo Scripts - Tables - Use for reference -->
    <script>
    
//datatable表头
var dataTable;
$(function(){
    //查询按钮                
    $("#searchBtn").click(function(){
        //debugger;
        init();
    });
})
function init(){
    
    var id= $("input:hidden[name='id']").val(),//input type为hidden时的取值方式
        name= $('#name').val();//input为text时的取值
        usersql= $('#sql').val();//用户SQL
        
    var param = {
            id: id,
            name: name,
            usersql: usersql
            };
    $.ajax({
        url  : '?act=ajax',//url
        type : 'POST',
        data : param,
        success : function(resdata) {
            //console.log(resdata);
            if (resdata.result == 0)
            {
                alert(resdata.error);
                return;
            }
            //请求成功后 如果存在datatable结构销毁
            if(dataTable){
                dataTable.destroy();
            }
            //清空table数据 
            $("#colTb").html("");
        
            var colsDef = [];//定义表头列名
    
            var obj = resdata.colist;//获取表头列名List
            var tableData = resdata.data;//获取table数据
            var cols = obj.length;
                    
            if(cols>0){//构建jsp的table表头显示
                $("#colTr").html((function (cols){
                    var html = "";
                    var colsNameCN;
                    for(var i=0;i<cols;i++){ //这里遍历后台返回的字段信息集合
                         //console.log(obj[i].column_cn);
                         colsNameCN=obj[i].view_name_cn;//这里取的字段信息的中文名
                         var html1=""
                         if(colsNameCN.length>15){//字段中文名过长截取显示
                             html1 += "<span title='"+colsNameCN+"'>"+ colsNameCN.substr(0, 14) + "...</span>";//title鼠标悬浮显示全部字段中文名
                             html += $("<th></th>").append(html1).prop("outerHTML");//构建table表头的html
                             }else{
                             html += $("<th></th>").append(colsNameCN).prop("outerHTML");//构建table表头的html
 
                         }
                    }    
                     
                     return html;
                })(cols));    
            }
          
           if(cols>0){
                for(var i=0;i<cols;i++){
                    var colsNameEN;
                        colsNameEN=obj[i].column_en;//这里取的字段信息的英文名 和tabledata的key值对应
                        colsDef.push((function (colsNameEN){
                            var colItem = {
                                    data:colsNameEN,
                                    render: function( data, type, full, meta ) {
                                        if(data){
                                             return data;
                                        }else{
                                             return "";
                                        }
                                    }
                            }
                            return colItem;
                        })(colsNameEN));
            }
        }
                //datatable初始化
                dataTable = $('#dataTables-example').DataTable({
                    "ordering": false,
                    "info": true,
                    "bLengthChange": false,
                    "iDisplayLength":25,
                    "bFilter": true,
                    "retrieve": true,
                    "processing": true,
                    "scrollX": true,
                    "fixedColumns": true,
                    "bScrollAutoCss": true,
                    "language": {
                        "search": "过滤：",
                        "lengthMenu": "每页 _MENU_ 条记录",
                        "loadingRecords": "请等待，数据正在加载中......",
                        "zeroRecords": "没有找到记录",
                        "info": "从 _START_ 到  _END_ 条记录 总记录数为 _TOTAL_ 条，第 _PAGE_ 页 ( 总共 _PAGES_ 页 )",
                        "infoEmpty": "没有数据",
                        "infoFiltered": "(从 _MAX_ 条数据中检索)",
                        "oPaginate": {
                            "processing": "正在查询中...",
                            "sFirst": "首页",
                            "sPrevious": "前一页",
                            "sNext": "后一页",
                            "sLast": "尾页"
                       }
                     },
                     data:tableData,
                     columns: colsDef
                     
                });
               
                //全选    
                $('#checkAll').click(function() {
                    $('[name=id]:checkbox').prop('checked', this.checked);
                    });
        },
         error: function () {
             
            alert("请求失败")
     }
    });
   } 

    $('#default_sql').on('change',function(event){
        $('#sql').val($('#default_sql').val());
        });
    </script>

</body>

</html>