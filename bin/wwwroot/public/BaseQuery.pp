﻿unit BaseQuery;

interface

uses
  SysUtils, Classes, Variants, DB;

type
  TBaseQry = class
  public
    class function DebugDataSet(ADataSet: TDataSet): string;
  public
    class function SqliteSynchronous(ADbName: string = ''): Boolean;
    
    class function GetQuery(AName: string; const CanNew: Boolean = True; const ADbName: string = ''): TwmQuery;
    class function DbExec(const ASQL: string; AValues: Variant; const ADbName: string = ''): Integer;
    class function DbExecUpd(const ATableName, AFields, AWhereCondition: string; AValues: Variant; const ADbName: string = ''): Integer;

	class function DbQueryWithError(var AQuery: TWmQuery; const ASQL: string; AValues: Variant; var AError: string; const ADbName: string = ''): Integer;
	class function DbQuery(var AQuery: TWmQuery; const ASQL: string; AValues: Variant; const ADbName: string = ''): Integer;
    class function DbQuerySel(var AQuery: TWmQuery; const AFields, ATableName, AWhereCondition: string; AValues: Variant; const ADbName: string = ''): Integer;

    class function GetMaxID(ATableName, AFieldName: string; const AWhereCondition: string = ''; const ADbName: string = ''): Integer;
	class function GetRecordCount(ATableName: string; const AWhereCondition: string = ''; const ADbName: string = ''): Integer;
    class function FillRecord(AQuery: TWmQuery; AFieldNames: array of string; AFieldValues: array of Variant): Integer;
    class function FillRecordFromReq(AQuery: TWmQuery; IncludeQueryFields: Boolean = False; NeedDecode: Boolean = True): Integer;
    class function FillRecordFromReqEx(AQuery: TWmQuery; AFieldNames: array of string; IncludeQueryFields: Boolean = False; NeedDecode: Boolean = True): Integer;
  end;

function DbUtf8(const Value: string; IsEncode: Boolean = True): string; //将数据库字段转为Utf8,为了兼容Access
function DbDateToStr(const Value: TDateTime; const AFmt: string = 'yy-mm-dd hh:nn:ss'): string;

implementation

var
  _gIsDbDebug: Boolean;

function DbUtf8(const Value: string; IsEncode: Boolean): string; 
begin
  //如果不是Access数据库,直接返回源数据Value即可.
  if IsEncode then
    Result := Utf8Encode(Value)
  else
    Result := Utf8Decode(Value);  
end;

function DbDateToStr(const Value: TDateTime; const AFmt: string): string;
begin
  Result := FormatDateTime(AFmt, Value);
end;

function DebugDataSetHeader(ADS: TDataSet): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to ADS.FieldCount - 1 do
  begin
    Result := Result + '<th>' + DbUtf8(ADS.Fields[i].FieldName) + '</th>' + sLineBreak;
  end;
end;

function DebugDataSetData(ADS: TDataSet): string;
var
  i: Integer;
begin
  Result := '';
  if ADS.RecordCount < 1 then
    Exit;

  ADS.First;
  while not ADS.Eof do
  begin
    Result := Result + '<tr>';
    for i := 0 to ADS.FieldCount - 1 do
    begin
      Result := Result + '<td>' + DbUtf8(ADS.Fields[i].AsString) + '</td>';
    end;
    Result := Result + '</tr>' + sLineBreak;

    ADS.Next;
  end;
end;

class function TBaseQry.DebugDataSet(ADataSet: TDataSet): string;
begin
  Result := '';
  if not ADataSet.Active then
    Exit;

  Result := '<table border="1">' + sLineBreak;
  Result := Result + DebugDataSetHeader(ADataSet);
  Result := Result + DebugDataSetData(ADataSet);

  Result := Result + '</table>';
end;

class function TBaseQry.SqliteSynchronous(ADbName: string): Boolean;
var
  AQuery: TwmQuery;
  sConnectStr: string;
begin
  if gServerVar.IndexOf('SqliteSynchronous') < 0 then
  begin
    AQuery := wm.NewQuery('', ADbName);//默认数据库
    try
      if not Assigned(AQuery) then
      begin
        WriteLnLog('Err(TBaseQry.DbQuery):Database connection err.', []);
        Exit;
      end;
      
      sConnectStr := LowerCase(AQuery.GetConnectString); //获取连接字符串
      if Pos('provider name=sqlite', sConnectStr) > 0 then
      begin
        //关于此句的作用,请自行百度
        TBaseQry.DbExec('PRAGMA synchronous = OFF', Null);
        
        //WriteLnLog('Set PRAGMA synchronous OK', []);
      end;  
    finally
      if Assigned(AQuery) then 
        FreeAndNil(AQuery);
    end;

    gServerVar.S['SqliteSynchronous'] := '1';  
  end;
  Result := True;  
end;  

class function TBaseQry.GetQuery(AName: string; const CanNew: Boolean; const ADbName: string): TwmQuery;
begin
  //从当前WebModule中查找命名Query,此处为
  Result := TwmQuery(wm.Objects[AName]);
  if (not Assigned(Result)) and CanNew then
    Result := wm.NewQuery(AName, ADbName);
end;

class function TBaseQry.DbExec(const ASQL: string; AValues: Variant; const ADbName: string): Integer;
var
  AQuery: TWmQuery;
begin
  Result := TBaseQry.DbQuery(AQuery, ASQL, AValues, ADbName);
  if Assigned(AQuery) then 
  begin
    if gServerVar.B['IsDbDebug'] then
      WritelnLog('%d Rows affected.', [Result]);
    FreeAndNil(AQuery);
  end;  
end;

class function TBaseQry.DbExecUpd(const ATableName, AFields, AWhereCondition: string; AValues: Variant; const ADbName: string): Integer;
var
  sSQL: string;
begin
  sSQL := 'UPDATE ' + ATableName + ' SET ' + AFields + ' WHERE ' + AWhereCondition;

  Result := TBaseQry.DbExec(sSQL, AValues, ADbName);
end;

class function TBaseQry.DbQuerySel(var AQuery: TWmQuery; const AFields, ATableName, AWhereCondition: string; AValues: Variant; const ADbName: string): Integer;
var
  sSQL: string;
begin
  sSQL := 'SELECT ' + AFields + ' FROM ' + ATableName + ' WHERE ' + AWhereCondition;
  
  Result := TBaseQry.DbQuery(AQuery, sSQL, AValues, ADbName);  
end;

class function TBaseQry.DbQueryWithError(var AQuery: TWmQuery; const ASQL: string; AValues: Variant; var AError: string; const ADbName: string = ''): Integer;
var
  i, iPos, iCount: Integer;
  dTime: Cardinal;
  bIsSelect: Boolean;
begin
  dTime := GetTickCount;
  Result := -1;
  if not Assigned(AQuery) then
  begin
    AQuery := wm.NewQuery('', ADbName);
    if not Assigned(AQuery) then
    begin
      AError := Format('Err(TBaseQry.DbQuery):Database connection err.', []);
      WriteLnLog(AError, []);
      Exit;
    end;
  end  
  else
  begin
    AQuery.Active := False;  
  end;  

  if gServerVar.B['IsDbDebug'] then
    WritelnLog('Info(TBaseQry.DbQuery):' + ASQL, []); 
    
  iPos := Pos('select', LowerCase(Trim(ASQL)));  
  bIsSelect := (iPos < 6) and (iPos > 0);
  if Length(ASQL) < 1 then
  begin
    //如果没有SQL则很容易出现List Index Out Of 错误,当然数据库打开失败也会出现
    AError := Format('Err(TBaseQry.DbQuery):No SQL SENTENCE in Query.', []);
    WritelnLog(AError, []);
    FreeAndNil(AQuery);
    AQuery := nil;
    
    Exit;
  end;
  
  AQuery.SQL.Text := ASQL;
	
  if VarHasValue(AValues) then
  begin  
    iCount := Length(AValues);
    if iCount > AQuery.ParamCount then
      iCount := AQuery.ParamCount;
    for i := 0 to iCount - 1 do
    begin
      AQuery.ParamByIndex(i).Value := AValues[i];
    end;
  end;  

  try
    if bIsSelect then
    begin
      AQuery.Open;
    
      if AQuery.Active then
        Result := AQuery.RecordCount;
    end
    else
    begin
      AQuery.ExecSQL;
      
      Result := AQuery.RowsAffected;
    end;
  except
    on E: Exception do
    begin
      AError := Format('Err(BaseQuery.TBaseQry.DbQuery):%s,%s', [E.Message, ASQL]);
      WritelnLog(AError, []);
    end;
  end;
  
  {
  dTime := GetTickCount - dTime;

  if gServerVar.B['IsDbDebug'] then
    WritelnLog('Elapsed %d ms.', [dTime]);
  //}
end;

class function TBaseQry.DbQuery(var AQuery: TWmQuery; const ASQL: string; AValues: Variant; const ADbName: string): Integer;
var
  sError: string;
begin
  Result := TBaseQry.DbQueryWithError(AQuery, ASQL, AValues, sError, ADbName);
end;

class function TBaseQry.GetMaxID(ATableName, AFieldName: string; const AWhereCondition: string; const ADbName: string): Integer;
var
  AQuery: TWmQuery;
  sSQL: string;
begin
  Result := 0;
  try
    sSQL := Format('SELECT Max(%s) FROM %s', [AFieldName, ATableName]);
    if Length(AWhereCondition) > 0 then
      sSQL := sSQL + ' WHERE ' + AWhereCondition;
    if DbQuery(AQuery, sSQL, Null, ADbName) > -1 then
	  Result := AQuery.Fields[0].AsInteger;
  finally
    if Assigned(AQuery) then
      FreeAndNil(AQuery);
  end;
end;

class function TBaseQry.GetRecordCount(ATableName: string; const AWhereCondition: string; const ADbName: string): Integer;
var
  AQuery: TWmQuery;
  sSQL: string;
begin
  try
    sSQL := Format('SELECT Count(*) FROM %s', [ATableName]);
	if AWhereCondition <> '' then
	  sSQL := sSQL + ' WHERE ' + AWhereCondition; 
	
    if DbQuery(AQuery, sSQL, Null, ADbName) > -1 then
	  Result := AQuery.Fields[0].AsInteger;
  finally
    if Assigned(AQuery) then
      FreeAndNil(AQuery);
  end;
end;

{
function GetMaxID(ATableName, AFieldName: string): Integer;
var
  AQuery: TWmQuery;
begin
  AQuery := wm.NewQuery('');
  try
    AQuery.SQL.Text := Format('select Max(%s) from %s', [AFieldName, ATableName]);
	AQuery.Open;
	Result := AQuery.Fields[0].AsInteger;
  finally
    FreeAndNil(AQuery);
  end;
end;

function GetRecordCount(ATableName: string; AWhereCondition: string = ''): Integer;
var
  AQuery: TWmQuery;
  sSQL: string;
begin
  AQuery := wm.NewQuery('');
  try
    sSQL := Format('select Count(*) from %s', [ATableName]);
	if AWhereCondition <> '' then
	  sSQL := sSQL + ' where (' + AWhereCondition + ')'; 
    AQuery.SQL.Text := sSQL;
	AQuery.Open;
	Result := AQuery.Fields[0].AsInteger;
  finally
    FreeAndNil(AQuery);
  end;
end;
}

class function TBaseQry.FillRecord(AQuery: TWmQuery; AFieldNames: array of string; AFieldValues: array of Variant): Integer;
var
  i: Integer;
  sFieldName, sFieldValue: string;
  AField: TField;
begin
  Result := 0;
  if not (Assigned(AQuery) and AQuery.Active) then 
    Exit;

  //if not (AQuery.DataSet.State in [dsEdit, dsInsert]) then
  //  Exit;
	
  for i := 0 to High(AFieldNames) do
  begin
    AField := AQuery.FieldByName(AFieldNames[i]);
    if not Assigned(AField) then
      Continue;
      
    if i >= Length(AFieldValues) then
      break;
      
    AField.Value := AFieldValues[i];  
	  
	Inc(Result);
  end;
end;

class function TBaseQry.FillRecordFromReq(AQuery: TWmQuery; IncludeQueryFields: Boolean = False; NeedDecode: Boolean = True): Integer;
var
  i: Integer;
  sFieldName, sFieldValue: string;
  AField: TField;
begin
  Result := 0;
  if not (Assigned(AQuery) and AQuery.Active) then 
    Exit;

  //if not (AQuery.DataSet.State in [dsEdit, dsInsert]) then
  //  Exit;
	
  for i := 0 to AQuery.Fields.Count - 1 do
  begin
    AField := AQuery.Fields[i];
    sFieldName := AField.FieldName;
    sFieldValue := Request.ContentFields.Values[sFieldName];
    if (Length(sFieldValue) < 1) and IncludeQueryFields then
	  sFieldValue := Request.QueryFields.Values[sFieldName];
	if (Length(sFieldValue) < 1) then
      Continue;

    if NeedDecode then
      AField.Value := DbUtf8(UrlDecode(sFieldValue), False)
	else
      AField.Value := DbUtf8(sFieldValue, False);
	  
	Inc(Result);
  end;
end;

class function TBaseQry.FillRecordFromReqEx(AQuery: TWmQuery; AFieldNames: array of string; IncludeQueryFields: Boolean = False; NeedDecode: Boolean = True): Integer;
var
  i: Integer;
  sFieldName, sFieldValue: string;
  AField: TField;
begin
  Result := 0;
  if not (Assigned(AQuery) and AQuery.Active) then 
    Exit;

  //if not (AQuery.DataSet.State in [dsEdit, dsInsert]) then
  //  Exit;
	
  for i := 0 to Length(AFieldNames) - 1 do
  begin
    AField := AQuery.FieldByName(AFieldNames[i]);
    if not Assigned(AField) then 
      Continue;
      
    sFieldName := AFieldNames[i];
    sFieldValue := Request.ContentFields.Values[sFieldName];
    if (Length(sFieldValue) < 1) and IncludeQueryFields then
	  sFieldValue := Request.QueryFields.Values[sFieldName];
	if (Length(sFieldValue) < 1) then
      Continue;

    if NeedDecode then
      AField.Value := DbUtf8(UrlDecode(sFieldValue), False)
	else
      AField.Value := DbUtf8(sFieldValue, False);
	  
	Inc(Result);
  end;
end;


initialization
  //如果是Sqlite数据库,为了提高性能,执行特殊操作,有待优化
  TBaseQry.SqliteSynchronous;
  //设置调试状态
  gServerVar.B['IsDbDebug'] := True;

finalization

end.