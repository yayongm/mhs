unit BaseUI;

interface

uses
  SysUtils, Classes, Variants;
  
type
  TBaseUI = class
  public
    //基础,客户端是否支持压缩
    class function IsAcceptZip: Boolean;
    class function Redirect(AUrl: string; ACode: Integer = 302): Boolean;
  public
    class procedure PrintDateTime(Value: Double = 0.0; const AFmt: string = 'yyyy-mm-dd hh:nn:ss');  
  public
    //<select>,combobox
    class function GetSelComboItem(Values, Names: Variant; ASelValue: Variant): string;
	class function GetSelCombo(Values, Names: Variant; ASelValue: Variant; ANameTag: string = ''): string;
  end;

//获取Request.QueryFields的值
function mRQV(const AName: string; NeedDecode: Boolean = True): string;
//获取Request.ContentFields的值
function mRCV(const AName: string; NeedDecode: Boolean = True): string;
//获取参数值
function mRPV(const AName: string; NeedDecode: Boolean = True): string;

implementation

function mRQV(const AName: string; NeedDecode: Boolean): string;
begin
  if NeedDecode then
    Result := Utf8Decode(UrlDecode(Request.QueryFields.Values[AName]))
  else  
    Result := Utf8Decode(Request.QueryFields.Values[AName]);
end;  

function mRCV(const AName: string; NeedDecode: Boolean): string;
begin
  if NeedDecode then
    Result := Utf8Decode(UrlDecode(Request.ContentFields.Values[AName]))
  else  
    Result := Utf8Decode(Request.ContentFields.Values[AName]);
end;

function mRPV(const AName: string; NeedDecode: Boolean): string;
var
  iIndex: Integer;
begin
  if Request.QueryFields.IndexOfName(AName) >= 0 then
    Result := mRQV(AName, NeedDecode)
  else
    Result := mRCV(AName, NeedDecode);
end;

class function TBaseUI.IsAcceptZip: Boolean;
begin
  Result := Pos('deflate', Request.RequestInfo.AcceptEncoding) > 0;
end;

class function TBaseUI.Redirect(AUrl: string; ACode: Integer = 302): Boolean;
begin
  Response.CustomHeaders.Text := 'location: ' + AUrl;
  Response.StatusCode := ACode;
  Result := True;
end;

class procedure TBaseUI.PrintDateTime(Value: Double; const AFmt: string);  
begin
  if Value = 0.0 then
    print FormatDateTime(AFmt, Now)
  else
    print FormatDateTime(AFmt, Value);
end;

class function TBaseUI.GetSelComboItem(Values, Names: Variant; ASelValue: Variant): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(Values) - 1 do
  begin
    Result := Result + '  <option value="' + VarToStr(Values[i]) + '"';
	if Values[i] = ASelValue then
	  Result := Result + ' selected="selected">'
	else
      Result := Result + '>';	
	
    Result := Result + VariantToRawStr(Names[i]) + '</option>' + sLineBreak;	
  end;
end;

class function TBaseUI.GetSelCombo(Values, Names: Variant; ASelValue: Variant; ANameTag: string = ''): string;
begin
  if ANameTag <> '' then  
    Result := '<select ' + ANameTag + '>' + sLineBreak
  else	
    Result := '<select>' + sLineBreak;
	
  Result := Result + GetSelComboItem(Values, Names, ASelValue) + '</select>' + sLineBreak;	
end;

end.  