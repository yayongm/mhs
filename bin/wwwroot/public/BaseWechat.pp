unit BaseWechat;

interface

uses
  SysUtils, Classes,
  BaseDebug,
  SuperObject, SimpleXml;
  
type
  //接收到的消息
  //文本消息
  TWxUserMsgText = packed record
    URL: string;
    ToUserName: string;
    FromUserName: string;
    CreateTime: Int64;
    MsgType: string;
    Content: string;
    MsgId: Int64;
  end;
  
  //回复消息
  //文本消息
  TWxRetMsgText = packed record
    ToUserName: string;
    FromUserName: string;
    CreateTime: Int64;
    MsgType: string;
    
    Content: string;
  end;
  //图文消息,简单只支持一个,需支持多个的请自行处理
  TWxRetNewsText = packed record
    ToUserName: string;
    FromUserName: string;
    CreateTime: Int64;
    MsgType: string;
    
    Title,Description,PicUrl,Url: string;
  end;

  TWechatMsgFn = class
  public
    class function WxXmlToUserMsgText(const Value: string; var AUMT: TWxUserMsgText): Boolean;
    class function WxUserMsgTextToXml(const AUMT: TWxUserMsgText): string; 
    //文本消息
    class function WxRetMsgTextToXml(const AMsg: TWxRetMsgText): string;
    //图文消息
    class function WxRetMsgNewsToXml(const AMsg: TWxRetNewsText): string;
  public
    class function ReturnMsgText(ToUserOpenId, FromUserName: string; AContent: string): string;  
    class function ReturnNewsText(ToUserOpenId, FromUserName, Title, Description, PicUrl, Url: string): string;
  end;

type
  TBaseWechat = class
  public
    //获取AccessToken
    class function get_access_token(var ErrMsg: string): string;
    class function get_server_list(var ErrMsg: string): string;
    //上传文件,类型包括:分别有图片（image）、语音（voice）、视频（video）和缩略图（thumb）
    class function media_upload(const AType: string; const AFileName: string; var ErrMsg: string): string;
  public
    //验证消息真实性
    class function CheckSignature: Boolean;
    //自定义菜单,0为成功,其它为错误编号
    class function menu_create(const AMenuString: string; var ErrMsg: string): Integer; 
    class function menu_get(var ErrMsg: string): string; 
    class function menu_delete(var ErrMsg: string): Integer; 
  public
    //客服消息,向用户发送消息
    //普通文本消息,Json格式
    class function message_custom_send(ToUserOpenId: string; AMsgType: string; AContent: string; var ErrMsg: string): Integer;
  public
    //用户接口
    //用户管理: 获取关注者列表接口
    class function user_get(var ATotal, ACount: Integer; var ANextOpenid, ErrMsg: string): Integer;    
  public
    //网页开发
    //2.通过code换取网页授权access_token,同时获得用户openid等信息
    class function sns_oauth2_get_openid(ACode: string; var ErrMsg: string): string;
  
  public
    class function AutoRefreshToken: Boolean;
  end;

//请自行替换为您自己的appid和secretid  
const
  C_Wechat_AppID = 'appid';//'appid';
  C_Wechat_Secret = 'secretid';//'secretid';  
  
const
  C_MyServer_Token = 'testtoken';  
 
//记录中的全局变量名 
const
  C_Wechat_Token_Name = 'MhsWechatToken';
  C_Wechat_Token_Expires = 'MhsWechatTokenExpires';
  C_Wechat_Token_LastTime = 'MhsWechatTokenLastTime';

function MnCreateXmlDocument(const aRootElementName: string = ''): IXmlDocument;

implementation

//导出本函数是为了避免少传参数时出错
function MnCreateXmlDocument(const aRootElementName: string): IXmlDocument;
begin
{//原函数
function CreateXmlDocument(const aRootElementName: String; const aVersion: String;
    const anEncoding: String; const aNameTable: IXmlNameTable): IXmlDocument;
}
  Result := CreateXmlDocument(aRootElementName, '', '', nil);
end;                           

class function TWechatMsgFn.WxXmlToUserMsgText(const Value: string; var AUMT: TWxUserMsgText): Boolean;
var
  ADoc: IXmlDocument;
  ANode: IXmlNode;
begin
  Result := False;

  ADoc := LoadXmlDocumentFromXML(Value);
  try
    ANode := ADoc.NeedChild('xml');
    if not Assigned(ANode) then
      Exit;

    AUMT.URL := ANode.Values['URL'];
    AUMT.ToUserName := ANode.Values['ToUserName'];
    AUMT.FromUserName := ANode.Values['FromUserName'];
    AUMT.CreateTime := ANode.Values['CreateTime'];
    AUMT.MsgType := ANode.Values['MsgType'];
    AUMT.Content := ANode.Values['Content'];
    AUMT.MsgId := ANode.Values['MsgId'];

    Result := True;
  finally
    ADoc := nil;
  end;
end;

class function TWechatMsgFn.WxUserMsgTextToXml(const AUMT: TWxUserMsgText): string;
var
  ADoc: IXmlDocument;
  ANode, AChild: IXmlNode;
  AXS: IXmlCDATASection;
begin
  Result := '';

  ADoc := MnCreateXmlDocument();
  try
    ANode := ADoc.AppendElement('xml');
      AChild := ANode.AppendElement('ToUserName');
      AChild.AppendCDATA(AUMT.ToUserName);
      AChild := ANode.AppendElement('FromUserName');
      AChild.AppendCDATA(AUMT.FromUserName);

      ANode.Values['CreateTime'] := AUMT.CreateTime;

      AChild := ANode.AppendElement('MsgType');
      AChild.AppendCDATA(AUMT.MsgType);
      AChild := ANode.AppendElement('Content');
      AChild.AppendCDATA(AUMT.Content);

      ANode.Values['MsgId'] := AUMT.MsgId;

    Result := ADoc.XML;
  finally
    ADoc := nil;
  end;
end;

class function TWechatMsgFn.WxRetMsgTextToXml(const AMsg: TWxRetMsgText): string;
var
  ADoc: IXmlDocument;
  ANode, AChild: IXmlNode;
  AXS: IXmlCDATASection;
begin
  Result := '';

  ADoc := MnCreateXmlDocument();
  try
    ANode := ADoc.AppendElement('xml');
      AChild := ANode.AppendElement('ToUserName');
      AChild.AppendCDATA(AMsg.ToUserName);
      AChild := ANode.AppendElement('FromUserName');
      AChild.AppendCDATA(AMsg.FromUserName);

      ANode.Values['CreateTime'] := AMsg.CreateTime;

      AChild := ANode.AppendElement('MsgType');
      AChild.AppendCDATA('text');
      //AChild.AppendCDATA(AMsg.MsgType);
      AChild := ANode.AppendElement('Content');
      AChild.AppendCDATA(AMsg.Content);

    Result := ADoc.XML;
  finally
    ADoc := nil;
  end;
end;

class function TWechatMsgFn.WxRetMsgNewsToXml(const AMsg: TWxRetNewsText): string;
var
  ADoc: IXmlDocument;
  ANode, AChild, AArtilces, AItem: IXmlNode;
  AXS: IXmlCDATASection;
begin
  Result := '';

  ADoc := MnCreateXmlDocument();
  try
    ANode := ADoc.AppendElement('xml');
      AChild := ANode.AppendElement('ToUserName');
      AChild.AppendCDATA(AMsg.ToUserName);
      AChild := ANode.AppendElement('FromUserName');
      AChild.AppendCDATA(AMsg.FromUserName);

      ANode.Values['CreateTime'] := AMsg.CreateTime;

      AChild := ANode.AppendElement('MsgType');
      AChild.AppendCDATA('news');
      //AChild.AppendCDATA(AMsg.MsgType);
      
      ANode.Values['ArticleCount'] := 1;
      
      AArtilces := ANode.AppendElement('Articles');
        AItem := AArtilces.AppendElement('item');
          AChild := AItem.AppendElement('Title');
          AChild.AppendCDATA(AMsg.Title);
          AChild := AItem.AppendElement('Description');
          AChild.AppendCDATA(AMsg.Description);
          AChild := AItem.AppendElement('PicUrl');
          AChild.AppendCDATA(AMsg.PicUrl);
          AChild := AItem.AppendElement('Url');
          AChild.AppendCDATA(AMsg.Url);
      
    Result := ADoc.XML;
  finally
    ADoc := nil;
  end;
end;


class function TWechatMsgFn.ReturnMsgText(ToUserOpenId, FromUserName: string; AContent: string): string; 
var
  AMsg: TWxRetMsgText;
begin
  AMsg.ToUserName := ToUserOpenId;
  AMsg.FromUserName := FromUserName;
  AMsg.CreateTime := DateTimeToUnixTime(Now);

  AMsg.Content := AContent;
  
  Result := TWechatMsgFn.WxRetMsgTextToXml(AMsg);
end;

class function TWechatMsgFn.ReturnNewsText(ToUserOpenId, FromUserName, Title, Description, PicUrl, Url: string): string;
var
  AMsg: TWxRetNewsText;
begin
  AMsg.ToUserName := ToUserOpenId;
  AMsg.FromUserName := FromUserName;
  AMsg.CreateTime := DateTimeToUnixTime(Now);
  
  AMsg.Title := Title;
  AMsg.Description := Description;
  AMsg.PicUrl := PicUrl;
  AMsg.Url := Url;
  
  Result := TWechatMsgFn.WxRetMsgNewsToXml(AMsg);
end;

//-------------------------------------

function CheckSignatureFunc(const AToken: string): Boolean;
var
  signature, timestamp, nonce, echostr: String;
  
  ASL: TStringList;
  sTemp: String;
begin
  ASL := TStringList.Create;
  try
    signature := Request.QueryFields.Values['signature'];
    timestamp := Request.QueryFields.Values['timestamp'];
    nonce := Request.QueryFields.Values['nonce'];

    echostr := Request.QueryFields.Values['echostr'];
    ASL.Add(AToken);
    ASL.Add(timestamp);
    ASL.Add(nonce);
    ASL.Sort;
    sTemp := StringReplaceAll(ASL.text, #13#10, '');
    Result := SHA1(sTemp, True) = signature;
  finally
    FreeAndNil(ASL);
  end;
end;

class function TBaseWechat.get_access_token(var ErrMsg: string): string;
const
  C_ACCESS_TOKEN = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s';
var
  sUrl: string;   
  sResult: string;
  ASO: ISuperObject;
begin
  ErrMsg := '';
  
  sUrl := Format(C_ACCESS_TOKEN, [C_Wechat_AppID, C_Wechat_Secret]);
  sResult := HttpRequestExecute(sUrl, '', '', 443, False);
  if gServerVar.B['MhsWechatDebug'] then
  begin
    //writeln(sResult);
  end;
  
  ASO := SO(sResult);
  try
    if ASO.I['errcode'] > 0 then
    begin
      ErrMsg := Format('%d|%s', [ASO.I['errcode'], ASO.S['errmsg']]);
      Result := '';
    end
    else
    begin
      ErrMsg := '';
      Result := ASO.S['access_token'];
      
      //将获取到的AccessToken保存到缓存中
      gServerVar.S[C_Wechat_Token_Name] := Result;
      gServerVar.I[C_Wechat_Token_Expires] := ASO.I['expires_in'];
      gServerVar.D[C_Wechat_Token_LastTime] := Now;
    end;
  finally
    ASO := nil;
  end;
end;

class function TBaseWechat.get_server_list(var ErrMsg: string): string;
const
  C_SERVERIP_URL = 'https://api.weixin.qq.com/cgi-bin/getcallbackip?&access_token=%s';
var
  sUrl: string;   
  sResult: string;
  ASO: ISuperObject;
  i: Integer;
  ASA: TSuperArray;
begin
  ErrMsg := '';
  
  sUrl := Format(C_SERVERIP_URL, [gServerVar.S[C_Wechat_Token_Name]]);
  sResult := HttpRequestExecute(sUrl, '', '', 443, False);
  
  if gServerVar.B['MhsWechatDebug'] then
  begin
    //writeln(sResult);
  end;
  
  ASO := SO(sResult);
  try
    if ASO.I['errcode'] > 0 then
    begin
      ErrMsg := Format('%d|%s', [ASO.I['errcode'], ASO.S['errmsg']]);
      Result := '';
    end
    else
    begin
      ErrMsg := '';
      
      Result := '';
      ASA := ASO.A['ip_list'];
      if not Assigned(ASA) then  
        Exit;
        
      for i := 0 to ASA.Length - 1 do
      begin
        Result := Result + ASA.GetS(i) + sLineBreak;
      end;  
    end;
  finally
    ASO := nil;
  end;
end;

class function TBaseWechat.media_upload(const AType: string; const AFileName: string; var ErrMsg: string): string;
const
  C_MEDIA_UPLOAD = 'http://file.api.weixin.qq.com/cgi-bin/media/upload?access_token=%s&type=%s';
var
  sUrl, sFormData: string;  
  sResult: string;
  ASO: ISuperObject;
begin
  //未实现,请自行实现
  sUrl := Format(C_MEDIA_UPLOAD, [gServerVar.S[C_Wechat_Token_Name], AType]);
  //请自行组织文件内容并上传.Post方法,
  //form-data中媒体文件标识，有filename、filelength、content-type等信息
  sFormData := '';
  
  //请求上传
  sResult := HttpRequestExecute(sUrl, sFormData, '', 80, False);
  
  //解析上传结果
  ASO := SO(sResult);
  try
    if ASO.I['errcode'] > 0 then
    begin
      ErrMsg := Format('%d|%s', [ASO.I['errcode'], ASO.S['errmsg']]);
      Result := '';
    end
    else
    begin
      Result := '';//成功返回
    end;
  finally
    ASO := nil;
  end;
end;

class function TBaseWechat.CheckSignature: Boolean;
begin
  TBaseDebug.DebugReq;
  
  Result := CheckSignatureFunc(C_MyServer_Token);
  if Result then
    if Request.QueryFields.Values['echostr'] <> '' then
      print Request.QueryFields.Values['echostr'];
end;

//=====================================
class function TBaseWechat.menu_create(const AMenuString: string; var ErrMsg: string): Integer; 
const
  C_MENU_CREATE = 'https://api.weixin.qq.com/cgi-bin/menu/create?access_token=%s';
var
  sUrl: string;  
  sResult: string;
  ASO: ISuperObject;
begin
  sUrl := Format(C_MENU_CREATE, [gServerVar.S[C_Wechat_Token_Name]]);
  
  //请求上传
  sResult := HttpRequestExecute(sUrl, AMenuString, '', 443, False);
  
  //解析上传结果
  ASO := SO(sResult);
  try
    Result := ASO.I['errcode'];
    if Result > 0 then
    begin
      ErrMsg := Format('%d|%s', [ASO.I['errcode'], ASO.S['errmsg']]);
    end
    else
    begin
      ErrMsg := ASO.S['errmsg'];//此时为'ok';
    end;
  finally
    ASO := nil;
  end;
end;

class function TBaseWechat.menu_get(var ErrMsg: string): string;
const
  C_MENU_GET = 'https://api.weixin.qq.com/cgi-bin/menu/get?access_token=%s';
var
  sUrl: string;  
  sResult: string;
  ASO: ISuperObject;
  iErr: Integer;
begin
  sUrl := Format(C_MENU_GET, [gServerVar.S[C_Wechat_Token_Name]]);
  
  //请求上传
  sResult := HttpRequestExecute(sUrl, '', '', 443, False);
  
  //解析上传结果
  ASO := SO(sResult);
  try
    iErr := ASO.I['errcode'];
    if iErr > 0 then
    begin
      ErrMsg := Format('%d|%s', [ASO.I['errcode'], ASO.S['errmsg']]);
      Result := '';
    end
    else
    begin
      ErrMsg := '';//此时为'ok';
      Result := sResult;
    end;
  finally
    ASO := nil;
  end;
end;

class function TBaseWechat.menu_delete(var ErrMsg: string): Integer;    
const
  C_MENU_DELETE = 'https://api.weixin.qq.com/cgi-bin/menu/delete?access_token=%s';
var
  sUrl: string;  
  sResult: string;
  ASO: ISuperObject;
begin
  sUrl := Format(C_MENU_DELETE, [gServerVar.S[C_Wechat_Token_Name]]);
  
  //请求上传
  sResult := HttpRequestExecute(sUrl, '', '', 443, False);
  
  //解析上传结果
  ASO := SO(sResult);
  try
    Result := ASO.I['errcode'];
    if Result > 0 then
    begin
      ErrMsg := Format('%d|%s', [ASO.I['errcode'], ASO.S['errmsg']]);
    end
    else
    begin
      ErrMsg := ASO.S['errmsg'];//此时为'ok';
    end;
  finally
    ASO := nil;
  end;
end;

//=====================================
class function TBaseWechat.message_custom_send(ToUserOpenId: string; AMsgType: string; AContent: string; var ErrMsg: string): Integer;
const
  C_MSG_CUSTOM_SEND = 'https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=%s';
var
  sUrl: string;  
  sResult, sSend: string;
  ASend, ASO: ISuperObject;
  dTime: Cardinal;
begin
  sUrl := Format(C_MSG_CUSTOM_SEND, [gServerVar.S[C_Wechat_Token_Name]]);
  
  ASend := SO('{}'); 
  try
    ASend.S['touser'] := ToUserOpenId;
    if AMsgType = '' then
    begin
      ASend.S['msgtype'] := 'text';
      ASend.O['text'] := SO('{}');
      ASend.O['text'].S['content'] := AContent;
    end
    else
    begin
      ASend.S['msgtype'] := AMsgType;
      ASend.O['text'] := SO('{}');
      ASend.O['text'].S['content'] := AContent;
    end;
    sSend := ASend.AsString;
  finally
    ASend := nil;
  end;
  
  if gServerVar.B['MhsWechatDebug'] then
  begin
    println sSend; println '<br>';
  end;
  
  //请求上传
  //dTime := GetTickCount;
  sResult := HttpRequestExecute(sUrl, sSend, '', 443, False);
  //dTime := GetTickCount - dTime;
  //print(Format('Elapsed %d ms.', [dTime]));//比较耗时
  
  //解析上传结果
  ASO := SO(sResult);
  try
    Result := ASO.I['errcode'];
    if Result > 0 then
    begin
      ErrMsg := Format('%d|%s', [ASO.I['errcode'], ASO.S['errmsg']]);
    end
    else
    begin
      ErrMsg := ASO.S['errmsg'];
    end;
  finally
    ASO := nil;
  end;
end;

//=====================================
class function TBaseWechat.user_get(var ATotal, ACount: Integer; var ANextOpenid, ErrMsg: string): Integer;
const
  C_USER_GET = 'https://api.weixin.qq.com/cgi-bin/user/get?access_token=%s&next_openid=%s';
var
  sUrl: string;  
  sResult: string;
  ASO: ISuperObject;
begin
  sUrl := Format(C_USER_GET, [gServerVar.S[C_Wechat_Token_Name], ANextOpenid]);

  //请求上传
  sResult := HttpRequestExecute(sUrl, '', '', 443, False);
  
  //解析上传结果
  ASO := SO(sResult);
  try
    Result := ASO.I['errcode'];
    if Result > 0 then
    begin
      ErrMsg := Format('%d|%s', [ASO.I['errcode'], ASO.S['errmsg']]);
    end
    else
    begin
      ATotal := ASO.I['total'];
      ACount := ASO.I['count'];
      ANextOpenid := ASO.S['next_openid'];
    end;
  finally
    ASO := nil;
  end;
end;

//=====================================
class function TBaseWechat.sns_oauth2_get_openid(ACode: string; var ErrMsg: string): string;
const
  C_SNS_OAUTH2_GET_OPENID = 'https://api.weixin.qq.com/sns/oauth2/access_token?appid=%s&secret=%s&code=%s&grant_type=authorization_code';
var
  sUrl: string;  
  sResult: string;
  sAccessToken, sRefreshToken, sOpenId, sScope: string;
  iExpiresIn, iTemp: Integer;
  ASO: ISuperObject;
begin
  sUrl := Format(C_SNS_OAUTH2_GET_OPENID, [C_Wechat_AppID, C_Wechat_Secret, ACode]);
  
  //请求上传
  sResult := HttpRequestExecute(sUrl, '', '', 443, False);
  
  //解析上传结果
  ASO := SO(sResult);
  try
    iTemp := ASO.I['errcode'];
    if iTemp > 0 then
    begin
      ErrMsg := Format('%d|%s', [ASO.I['errcode'], ASO.S['errmsg']]);
      Result := '';
    end
    else
    begin
      ErrMsg := '';
      sOpenId := ASO.S['openid'];
      {
      sAccessToken := ASO.S['access_token'];
      sRefreshToken := ASO.S['refresh_token'];
      sScope := ASO.S['scope'];
      iExpiresIn := ASO.I['expires_in'];
      //}
      
      Result := sOpenId;
    end;
  finally
    ASO := nil;
  end;
end;

//=====================================
class function TBaseWechat.AutoRefreshToken: Boolean;
var
  sErr, sTemp: string;
  iTime: Cardinal;
  
  bIsDebug: Boolean;
begin
  bIsDebug := gServerVar.B['MhsWechatDebug'];
  
  if bIsDebug then
    writeln('AutoRefreshToken:');
  
  iTime := DateTimeToUnixTime(Now) - DateTimeToUnixTime(gServerVar.D[C_Wechat_Token_LastTime]);
  if bIsDebug then
    writeln(iTime);
  
  sTemp := gServerVar.S[C_Wechat_Token_Name];
  if (sTemp = '') or (iTime > ((gServerVar.I[C_Wechat_Token_Expires] * 3) div 4)) then //Wechat过期值的3/4=7200*3/4=5400秒
    sTemp := TBaseWechat.get_access_token(sErr);

  Result := Length(sTemp) > 0;

  if bIsDebug then
  begin
    println 'Current Token:<br>';
    print sTemp; println '<br>';
  end;

  writeln(Format('Wechat_Access_Token:%s,%s', [sTemp, sErr]));
end;

initialization
  gServerVar.B['MhsWechatDebug'] := True; //设置调试模式

finalization


end.