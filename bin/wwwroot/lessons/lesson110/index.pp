<html>
<title>Hello,World</title>
<body>
<?
uses
  SysUtils;
  
function MyWriteLnFmt(const S: string; const Args: array of const): string;
begin
  Result := Format(S, Args);
  WriteLn(Result);
  WriteRuntimeLogFile(Result + sLineBreak);
end;

println '<h1>Hello,World!</h1><br>';
println '<div>See the console and the log file.</div><br>';

MyWriteLnFmt('Hello,World!', []);
?>
</body>
</html>
