<html>
<title>Session</title>
<body>
<h2>Session in Server</h2>
<div>
Control the session is very easy!<br>
For instance:<br><br>
</div>

<?
uses
  SysUtils, Classes;

var
  ASessionID: string;
  ASession: TScpSession;
  sName: string;
begin  
  ASessionID := 'mytestsessionid';
  
  ASession := gSessions.GetSession(ASessionID);
  if ASession = nil then
  begin
    ASession := gSessions.StartSession(ASessionID, 3600); //Time out after one hour
  
    sName := ASession.Values['Name'];
    println 'First set the session values:<br>';

    ASession.Values['Name'] := 'TestName';
    ASession.Values['ID'] := 123456;
  end
  else
  begin
    println 'Get session values:<br>';

    print 'The name value of the test session is '; println ASession.Values['Name']; print '<br>';
    print 'The id value of the test session is '; println ASession.Values['ID']; print '<br>';
	
	//Set Session
	ASession.Values['ID'] := Random($FFFF);
  end;
  
  //you can delete the session by:
  //gSessions.EndSession(ASessionID);
end;
?>

</body>
</html>
