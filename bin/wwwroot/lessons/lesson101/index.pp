<html>
<title>Object Oriented</title>
<body>
<div>Object oriented demo.</div><br>
<?
uses
  SysUtils;

type
  TParent = class(TObject)
  public
    function SayName: string; virtual;
  end;
  
  TChild = class(TParent)
  public
    function SayName: string; override; 
  end;

function TParent.SayName: string;
begin
  Result := 'Parent';
end;

function TChild.SayName: string;
begin
  Result := 'Child' + '->' + inherited SayName;
end;

function TestObj: Boolean;
var
  oP, oC: TParent;
begin
  oP := TParent.Create;
  try
    print 'Parent:<br>';
    print oP.SayName + '<br>';
  finally
    FreeAndNil(oP);
  end;
  
  print '<br>';
  
  oC := TChild.Create;
  try
    print 'Child:<br>';
	print oC.SayName + '<br>';
  finally
    FreeAndNil(oC);
  end;
  
  print '<br>';
end; 

//Main entrance here.
TestObj;
?>

</body>
</html>
