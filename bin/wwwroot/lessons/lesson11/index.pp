<html>
<title>Local Memory Cache</title>
<body>
<h2>Local Memory Cache in Server</h2>
<div>
You can cache everything in server. the data is thread safe and can access everywhere.<br>
For instance:<br><br>
</div>
<?
uses
  SysUtils, Classes; //The gServerVar is registered in classes unit. so you have to uses it first.

var
  sMyDataName: string;
  
begin
  sMyDataName := 'MyTestVar';

  println 'Save data to cache:<br>';
  gServerVar.S[sMyDataName] := '你好，张三！';
  
  println 'Get data from cache:'; println gServerVar.S[sMyDataName] + '<br>';
  println 'Index of the data:'; println gServerVar.IndexOf(sMyDataName); println '<br>';
  println 'Delete the data: OK!<br>'; gServerVar.Delete(sMyDataName);
  println 'Now, the data index is '; println gServerVar.IndexOf(sMyDataName); 
end;
?>
</body>
</html>
