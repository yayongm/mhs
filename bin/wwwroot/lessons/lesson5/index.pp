<html>
<title>Lesson5</title>
<body>
<?
uses 
  SysUtils, Classes, Variants;

//(ARequest: TIdHTTPAppRequest)
function IsAcceptZip: Boolean;
begin
  if Assigned(Request) and Assigned(Request.RequestInfo) then
    Result := Pos('deflate', Request.RequestInfo.AcceptEncoding) > 0
  else 
    Result := False;  
end;

function StrToHtml(Value: string): string;
begin
  Result := Value;
  Result := StringReplace(Result, ' ', '&nbsp;', [rfReplaceAll]);
  //Result := StringReplaceAll(Result, ' ', '&nbsp;');
  Result := Result + '<br>';
end;

procedure PrintText(Value: string; IsPrintLn: Boolean = False);
var
  sValue: string;
  ASL: TStrings;
  i: Integer;
begin
  ASL := TStringList.Create;
  try
    ASL.Text := Value;
    for i := 0 to ASL.Count - 1 do
      println StrToHtml(ASL.Strings[i]);
  finally
    FreeAndNil(ASL);
  end;
end;

procedure PrintHeader(AName, v1, v2: Variant);
begin
  print '<tr>';
  print '<th width="20%">' + VarToStr(AName) + '</th>';
  print '<th width="40%">' + VarToStr(v1) + '</th>';
  print '<th width="40%">' + VarToStr(v2) + '</th>';
  println '<tr>';
end;

procedure PrintRow(AName, v1, v2: Variant);
begin
  print '<tr>';
  print '<td>' + VarToStr(AName) + '</td>';
  print '<td>' + VarToStr(v1) + '</td>';
  print '<td>' + VarToStr(v2) + '</td>';
  println '<tr>';
end;

procedure PrintRequestInfo;
const
  C_NULL = '(Null)';
begin
  println '<table border="1">';

  PrintHeader('Name', 'Request', 'Request.RequestInfo');
  PrintRow('ClassName', Request.ClassName, Request.RequestInfo.ClassName);

  PrintRow('CharSet', C_NULL, Request.RequestInfo.CharSet);
  PrintRow('Connection', Request.Connection, Request.RequestInfo.Connection);
  PrintRow('ContentEncoding', Request.ContentEncoding, Request.RequestInfo.ContentEncoding);
  PrintRow('ContentLanguage', C_NULL, Request.RequestInfo.ContentLanguage);
  PrintRow('ContentLength', Request.ContentLength, Request.RequestInfo.ContentLength);
  PrintRow('ContentType', Request.ContentType, Request.RequestInfo.ContentType);
  PrintRow('ContentVersion', Request.ContentVersion, Request.RequestInfo.ContentVersion);
  PrintRow('Date', Request.Date, Request.RequestInfo.Date);
  PrintRow('Expires', Request.Expires, Request.RequestInfo.Expires);
  PrintRow('ETag', C_NULL, Request.RequestInfo.ETag);
  PrintRow('LastModified', C_NULL, Request.RequestInfo.LastModified);
  PrintRow('Pragma', C_NULL, Request.RequestInfo.Pragma);
  PrintRow('TransferEncoding', C_NULL, Request.RequestInfo.TransferEncoding);
  PrintRow('Accept', Request.Accept, Request.RequestInfo.Accept);
  PrintRow('AcceptCharSet', C_NULL, Request.RequestInfo.AcceptCharSet);
  PrintRow('AcceptEncoding', C_NULL, Request.RequestInfo.AcceptEncoding);
  PrintRow('AcceptLanguage', C_NULL, Request.RequestInfo.AcceptLanguage);
  PrintRow('From', Request.From, Request.RequestInfo.From);
  PrintRow('Password', C_NULL, Request.RequestInfo.Password);
  PrintRow('Referer', Request.Referer, Request.RequestInfo.Referer);
  PrintRow('UserAgent', Request.UserAgent, Request.RequestInfo.UserAgent);
  PrintRow('UserName', C_NULL, Request.RequestInfo.UserName);
  PrintRow('Host', Request.Host, Request.RequestInfo.Host);
  PrintRow('ProxyConnection', C_NULL, Request.RequestInfo.ProxyConnection);
  PrintRow('Range', C_NULL, Request.RequestInfo.Range);
  PrintRow('AuthExists', C_NULL, Request.RequestInfo.AuthExists);

  //PrintRow('Cookies', Request.CookieFields, Request.RequestInfo.Cookies.Count);
  PrintRow('Cookies.CookieFileds', Request.CookieFields.Text, C_NULL);
  PrintRow('Cookies.Count', Request.CookieFields.Count, C_NULL);

  PrintRow('Params', Request.ContentFields.Text, Request.RequestInfo.Params.Text);
  //PrintRow('PostStream', C_NULL, Request.RequestInfo.PostStream);
  PrintRow('RawHTTPCommand', C_NULL, Request.RequestInfo.RawHTTPCommand);
  PrintRow('RemoteIP', Request.RemoteAddr, Request.RequestInfo.RemoteIP);
  PrintRow('RemoteHost', Request.RemoteHost, C_NULL);

  PrintRow('Session', C_NULL, C_NULL);
  PrintRow('URL', Request.URL, Request.RequestInfo.Document);
  PrintRow('Method', Request.Method, Request.RequestInfo.Command);
  PrintRow('Version', Request.ProtocolVersion, Request.RequestInfo.Version);
  PrintRow('AuthUsername', C_NULL, Request.RequestInfo.AuthUsername);

  PrintRow('Query', Request.Query, C_NULL);
  PrintRow('QueryFields', Request.QueryFields.Text, C_NULL);
  PrintRow('QueryFields.Count', Request.QueryFields.Count, C_NULL);

  PrintRow('Content', Request.Content, C_NULL);
  PrintRow('ContentFields.Text', Request.ContentFields.Text, C_NULL);
  PrintRow('ContentFields.Count', Request.ContentFields.Count, C_NULL);

  PrintRow('PathInfo', Request.PathInfo, C_NULL);
  PrintRow('PathTranslated', Request.PathTranslated, C_NULL);
  PrintRow('InternalPathInfo', Request.InternalPathInfo, C_NULL);

  PrintRow('Files.Count', Request.Files.Count, C_NULL);
  PrintRow('Title', Request.Title, C_NULL);
  PrintRow('IfModifiedSince', Request.IfModifiedSince, C_NULL);
 

  //PrintRow('', Request., Request.RequestInfo.);

  print '</table>';
end;

procedure PrintResponseInfo;
const
  C_NULL = '(Null)';
begin
  println '<table border="1">';

  PrintHeader('Name', 'Response', 'Response.ResponseInfo');
  PrintRow('ClassName', Response.ClassName, Response.ResponseInfo.ClassName);

  //PrintRow('Cookies', Response.Cookies, Response.ResponseInfo.Cookies);
  PrintRow('Version', Response.Version, C_NULL);
  PrintRow('ReasonString', Response.ReasonString, C_NULL);
  PrintRow('Server', Response.Server, Response.ResponseInfo.Server);
  //PrintRow('WWWAuthenticate', Response.WWWAuthenticate, Response.ResponseInfo.WWWAuthenticate);
  PrintRow('Realm', Response.Realm, Response.ResponseInfo.AuthRealm);
  PrintRow('Allow', Response.Allow, C_NULL);
  PrintRow('Location', Response.Location, Response.ResponseInfo.Location);
  PrintRow('ContentEncoding', Response.ContentEncoding, Response.ResponseInfo.ContentEncoding);
  PrintRow('ContentType', Response.ContentType, Response.ResponseInfo.ContentType);
  PrintRow('ContentVersion', Response.ContentVersion, Response.ResponseInfo.ContentVersion);
  PrintRow('DerivedFrom', Response.DerivedFrom, C_NULL);
  PrintRow('Title', Response.Title, C_NULL);
  PrintRow('StatusCode', Response.StatusCode, Response.ResponseInfo.ResponseNo);
  PrintRow('ContentLength', Response.ContentLength, Response.ResponseInfo.ContentLength);
  PrintRow('Date', Response.Date, Response.ResponseInfo.Date);
  PrintRow('Expires', Response.Expires, Response.ResponseInfo.Expires);
  PrintRow('LastModified', Response.LastModified, Response.ResponseInfo.LastModified);
  PrintRow('Content', Length(Response.Content), Length(Response.ResponseInfo.ResponseText));
  
  //PrintRow('ContentStream', Response.ContentStream, Response.ResponseInfo.ContentStream);
  PrintRow('CustomHeaders.Text', Response.CustomHeaders.Text, C_NULL);
  PrintRow('CustomHeaders.Count', Response.CustomHeaders.Count, C_NULL);
  PrintRow('FreeStream', Response.FreeStream, C_NULL);

  PrintRow('CacheControl', C_NULL, Response.ResponseInfo.CacheControl);
  PrintRow('CharSet', C_NULL, Response.ResponseInfo.CharSet);
  PrintRow('Connection', C_NULL, Response.ResponseInfo.Connection);
  PrintRow('ContentDisposition', C_NULL, Response.ResponseInfo.ContentDisposition);
  PrintRow('ContentEncoding', Response.ContentEncoding, Response.ResponseInfo.ContentEncoding);
  PrintRow('ContentLanguage', C_NULL, Response.ResponseInfo.ContentLanguage);
  PrintRow('ContentLength', C_NULL, Response.ResponseInfo.ContentLength);
  PrintRow('ETag', C_NULL, Response.ResponseInfo.ETag);
  PrintRow('Pragma', C_NULL, Response.ResponseInfo.Pragma);
  PrintRow('TransferEncoding', C_NULL, Response.ResponseInfo.TransferEncoding);
  PrintRow('AcceptRanges', C_NULL, Response.ResponseInfo.AcceptRanges);
  PrintRow('ProxyConnection', C_NULL, Response.ResponseInfo.ProxyConnection);
  //PrintRow('ProxyAuthenticate', C_NULL, Response.ResponseInfo.ProxyAuthenticate);
  PrintRow('CloseConnection', C_NULL, Response.ResponseInfo.CloseConnection);
  PrintRow('HeaderHasBeenWritten', C_NULL, Response.ResponseInfo.HeaderHasBeenWritten);
  PrintRow('ServerSoftware', C_NULL, Response.ResponseInfo.ServerSoftware);

  //PrintRow('', Response., Response.ResponseInfo.);
  
  print '</table>';
end;

procedure PrintWebModuleInfo;
const
  C_NULL = '(Null)';
begin
  println '<table border="1">';

  PrintHeader('Name', 'WebModule', 'Description');
  PrintRow('ClassName', WM.ClassName, '');

  PrintRow('Values', '', 'Some variant of current page');
  PrintRow('ContentZipType', WM.Values['ContentZipType'], 'Control the content to compress.0=auto;1=NotZip;2=ForceZip');
  PrintRow('MinZipSize', WM.Values['MinZipSize'], 'Minimized compress content size; Load from config; you can customize it in current page.0=NotZip');
  PrintRow('MaxCacheFileSize', WM.Values['MaxCacheFileSize'], 'Cached the file size;Load from config; do not change.');
  //PrintRow('', WM., '');
  
  print '</table>';
end;

?>

<h2>Request</h2>
<div>

Request object is a class of TIdHTTPAppRequest. <br>
It's inherited from TWebRequest, but add original RequestInfo and ResponseInfo property.<br><br>
<?
PrintText(''+
'type' + sLineBreak +
'  TIdHTTPAppRequest = class(TWebRequest)' + sLineBreak +
'  public' + sLineBreak +
'    property RequestInfo: TIdHTTPRequestInfo read FRequestInfo;' + sLineBreak +
'    property ResponseInfo: TIdHTTPResponseInfo read FResponseInfo;' + sLineBreak +
'  end;' + sLineBreak +
'');

print '<br>';

print 'The runtime Request information is:<br>';
PrintRequestInfo;
?>

</div>

<h2>Response</h2>
<div>
Response object is a class of TIdHTTPAppResponse. <br>
It's inherited from TWebResponse, but add original RequestInfo and ResponseInfo property.<br><br>
<?
PrintText(''+
'type' + sLineBreak +
'  TIdHTTPAppResponse = class(TWebResponse)' + sLineBreak +
'  public' + sLineBreak +
'    property ResponseInfo: TIdHTTPResponseInfo read FResponseInfo;' + sLineBreak +
'    property RequestInfo: TIdHTTPRequestInfo read FRequestInfo;' + sLineBreak +
'  end;' + sLineBreak +
'');
print '<br>';

print 'The runtime Response information is:<br>';
PrintResponseInfo;

?>

</div>

<h2>WebModule</h2>
<div>
WebModule object is a class of TAppWebModule.<br>
It's inherited from TWebModule, but add a little method.<br><br>

<?
PrintText(''+
'type' + sLineBreak +
'  TAppWebModule = class(TWebModule)' + sLineBreak +
'  public' + sLineBreak +
'    function SendFile(Request: TWebRequest; Response: TWebResponse;' + sLineBreak +
'      const AFileName: string = ''; MinZipSize: Integer = 1024; MaxAge: Integer = -1): Integer;' + sLineBreak +
'    function SendContent(Request: TWebRequest; Response: TWebResponse;' + sLineBreak +
'      const AContent: string = ''; AStatusCode: Integer = 200; MinZipSize: Integer = 1024): Integer;' + sLineBreak +
'  public' + sLineBreak +
'    property Values[AName: string]: Variant read GetValues write SetValues;' + sLineBreak +
'  end;' + sLineBreak +
'');

print '<br>';

print 'The runtime WebModule information is:<br>';
PrintWebModuleInfo;

print 'yes, you can change the compress option to customize current page,ie:<br>';
print 'WM.Values[''MinZipSize''] := 512;//Only this page.<br>';

WM.Values['MinZipSize'] := 512;

if IsAcceptZip then
  print 'The client browser is support compress.<br>'
else
  print 'The client browser is not support compress.<br>';

?>
</div>

<h2>Other</h2>
<div>
<?
Response.CustomHeaders.Add('X-Powered-By: ASP.NET'); //Add Custom Header information

print 'This page is generated at:'; print FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', Now); print '<br>';
?>
</div>

</body>
</html>