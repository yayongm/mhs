<? uses SysUtils, Classes;?>
<html>
<title>Simple Template</title>
<body>
<h2>Simple Template</h2>
<div>
The server can include external file like template!<br>
For instance:<br><br>
</div>

<!-- Top Begin -->
<? print wm.include('top.tpl'); ?>
<!-- Top End -->

<div>Content here.</div>

<!-- Bottom Begin -->
<? print wm.include('bottom.tpl'); ?>
<!-- Bottom End -->

</body>
</html>
