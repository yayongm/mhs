<?
unit uHeader;

interface

uses
  SysUtils, Classes;

function GetHeaderTextA(ACurPage: string): string;
function GetHeaderText(ACurPage: string): string;

implementation

function GetHeaderTextA(ACurPage: string): string;

  function GetHeaderLi(ADestPage: string): string;
  begin
    if SameText(ACurPage, ADestPage) then
      Result := '				<li class="current">'
    else  
      Result := '				<li>'; 
  end;

var
  sTemp: string;
begin
  sTemp :=   
'	<!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->' + sLineBreak +	
'	' + sLineBreak +
'	<header id="header" class="clearfix">' + sLineBreak +
'		' + sLineBreak +
'		<a href="index.pp" id="logo"><img src="images/logo.png" alt="" title="Logo" /></a>' + sLineBreak +
'		' + sLineBreak +
'		<ul class="social-links clearfix">' + sLineBreak +
'			<li class="twitter"><a href="#">Twitter<span></span></a></li>' + sLineBreak +
'			<li class="facebook"><a href="#">Facebook<span></span></a></li>' + sLineBreak +
'			<li class="dribbble"><a href="#">Dribbble<span></span></a></li>' + sLineBreak +
'			<li class="vimeo"><a href="#">Vimeo<span></span></a></li>' + sLineBreak +
'			<li class="youtube"><a href="#">YouTube<span></span></a></li>' + sLineBreak +
'			<li class="rss"><a href="#">Rss<span></span></a></li>' + sLineBreak +
'		</ul><!--/ .social-links-->' + sLineBreak +
'		' + sLineBreak +
'		<nav id="navigation" class="navigation">' + sLineBreak +
'			' + sLineBreak +
'			<ul>' + sLineBreak +
  GetHeaderLi('index') + '<a href="index.pp">Home</a></li>' + sLineBreak +
  GetHeaderLi('blog-style-1') + '<a href="blog-style-1.pp">Blog</a>' + sLineBreak +
'					<ul>' + sLineBreak +
'						<li><a href="blog-style-1.pp">Blog Page</a></li>' + sLineBreak +
'						<li><a href="blog-style-2.pp">Alternative Blog Page</a></li>' + sLineBreak +
'						<li><a href="single-post.pp">Blog Single</a></li>' + sLineBreak +
'					</ul>' + sLineBreak +
'				</li>' + sLineBreak +
  GetHeaderLi('events') + '<a href="events.pp">Events</a>' + sLineBreak +
'					<ul>' + sLineBreak +
'						<li><a href="events.pp">Events Page</a></li>' + sLineBreak +
'						<li><a href="single-events.pp">Detailed Event</a></li>' + sLineBreak +
'					</ul>' + sLineBreak +
'				</li>' + sLineBreak +
  GetHeaderLi('Gallery') + '<a href="gallery.pp">Gallery</a></li>' + sLineBreak +
  GetHeaderLi('elements') + '<a href="elements.pp">Features</a>' + sLineBreak +
'					<ul>' + sLineBreak +
'						<li><a href="elements.pp">Elements</a></li>' + sLineBreak +
'						<li><a href="typography.pp">Typography</a></li>' + sLineBreak +
'						<li><a href="columns.pp">Columns</a></li>' + sLineBreak +
'						<li><a href="sidebar-left.pp">Sidebar Left</a></li>' + sLineBreak +
'					</ul>' + sLineBreak +
'				</li>' + sLineBreak +
  GetHeaderLi('contacts') + '<a href="contacts.pp">Contact</a></li>' + sLineBreak +
  GetHeaderLi('about') + '<a href="about.pp">About</a></li>' + sLineBreak +
'			</ul>' + sLineBreak +
'			' + sLineBreak +
'			<a href="#" class="donate">Donate</a>' + sLineBreak +
'			' + sLineBreak +
'		</nav><!--/ #navigation-->' + sLineBreak +
'		' + sLineBreak +
'	</header><!--/ #header-->' + sLineBreak +
'	' + sLineBreak +
'	<!-- - - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - - -->	' + sLineBreak +
''; 

  Result := sTemp; 
end;
?>

<?
function GetHeaderText(ACurPage: string): string;

  function GetHeaderLi(ADestPage: string): string;
  begin
    if SameText(ACurPage, ADestPage) then
      Result := '				<li class="current">'
    else  
      Result := '				<li>'; 
	
	print Result;
  end;

begin
?>
	<!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

	<header id="header" class="clearfix">

		<a href="index.pp" id="logo"><img src="images/logo.png" alt="" title="Logo" /></a>

		<ul class="social-links clearfix">
			<li class="twitter"><a href="#">Twitter<span></span></a></li>
			<li class="facebook"><a href="#">Facebook<span></span></a></li>
			<li class="dribbble"><a href="#">Dribbble<span></span></a></li>
			<li class="vimeo"><a href="#">Vimeo<span></span></a></li>
			<li class="youtube"><a href="#">YouTube<span></span></a></li>
			<li class="rss"><a href="#">Rss<span></span></a></li>
		</ul><!--/ .social-links-->

		<nav id="navigation" class="navigation">

			<ul>
  <? GetHeaderLi('index'); ?><a href="index.pp">Home</a></li>
  <? GetHeaderLi('blog-style-1'); ?><a href="blog-style-1.pp">Blog</a>
					<ul>
						<li><a href="blog-style-1.pp">Blog Page</a></li>
						<li><a href="blog-style-2.pp">Alternative Blog Page</a></li>
						<li><a href="single-post.pp">Blog Single</a></li>
					</ul>
				</li>
  <? GetHeaderLi('events'); ?><a href="events.pp">Events</a>'
					<ul>
						<li><a href="events.pp">Events Page</a></li>
						<li><a href="single-events.pp">Detailed Event</a></li>
					</ul>
				</li>
  <? GetHeaderLi('Gallery'); ?><a href="gallery.pp">Gallery</a></li>
  <? GetHeaderLi('elements'); ?><a href="elements.pp">Features</a>
					<ul>
						<li><a href="elements.pp">Elements</a></li>
						<li><a href="typography.pp">Typography</a></li>
						<li><a href="columns.pp">Columns</a></li>
						<li><a href="sidebar-left.pp">Sidebar Left</a></li>
					</ul>
				</li>
  <? GetHeaderLi('contacts'); ?><a href="contacts.pp">Contact</a></li>
  <? GetHeaderLi('about'); ?><a href="about.pp">About</a></li>
			</ul>

			<a href="#" class="donate">Donate</a>

		</nav><!--/ #navigation-->

	</header><!--/ #header-->

	<!-- - - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - - -->
<?
end;

end.
?>
