<html>
<title>Lesson109</title>
<head>
    <script src="../vendor/jquery/jquery.min.js"></script>
</head>
<body>
<?
println '<h1>Hello,World!</h1><br>';
?>

<div id="myDiv"><h2>通过 AJAX 改变文本</h2></div>
<button id="b01" type="button">改变内容</button>

<script>


$(document).ready(function(){
  $("#b01").click(function(){
    $.ajax({
        type: "POST",
        url: "test.pp",
        data: "name=garfield&age=18&cmp=myDiv",
        success: function(data){

				var r;
				r=data.split('|');
				if(r.length!=1)
				{
					if(r[0]!='')
					{
						document.getElementById(r[1]).innerHTML=r[0];
					}
					if(r[2].trim()!='')
					{
						alert(r[2]);
					}
				}
				else
				{
					document.getElementById('ajaxarea').innerHTML=data;
				}
        }
    });
  });
});


</script>

</body>
</html>
