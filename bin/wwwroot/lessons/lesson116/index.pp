<html>
<title>PerlRegEx</title>
<body>
<?
uses
  SysUtils, Classes, PerlRegEx;
  
procedure DoTestRegEx;
var
  reg: TPerlRegEx;
begin
  reg := TPerlRegEx.Create;
  try
    //不区分大小写,中文会出错
    //reg.Options := [preCaseLess];
    
    //转义字符
    //查找数字
    reg.Subject := 'CodeGear Delphi 2007 for Win32';
    reg.RegEx := '\d';
    if reg.Match then
      println '找到了' 
    else
      println '没找到';

    println '<br>';

    //边界
    //替换
    reg.Subject := 'Delphi Delphi2007 MyDelphi';
    reg.RegEx := '\bDelphi\b';
    reg.Replacement := '◆';
    reg.ReplaceAll;
    println reg.Subject;
    
    println '<br>';
    
    //贪婪匹配，结果为“◆”
    reg.Subject := '<html><head><title>标题</title></head><body>内容</body></html>';
    reg.RegEx   := '<.*>'; //将会全部匹配, 因为两头分别是: < 和 >
    reg.Replacement := '◆';
    reg.ReplaceAll;
    println reg.Subject;
    
    println '<br>';
    
    //非贪婪匹配，结果为“◆”
    reg.Subject := '<html><head><title>标题</title></head><body>内容</body></html>';
    reg.RegEx   := '<.*?>'; //将会全部匹配, 因为两头分别是: < 和 >
    reg.Replacement := '◆';
    reg.ReplaceAll;
    println reg.Subject;
    
    println '<br>';
    
    //引用子表达式
    reg.Subject := 'one two three ten';
    reg.RegEx := '(t)(\w+)';
    reg.Replacement := '[\1-\2:\0]'; // \1\2 分别引用对应的子表达式; \0 引用整个表达式
    reg.ReplaceAll;
    println reg.Subject;
    
    println '<br>';
    
    //临界匹配
    reg.Subject := 'Delphi 6; Delphi 7; Delphi 2007; Delphi Net';
    reg.RegEx   := 'Delphi (?=2007)'; // ?=
    reg.Replacement := '◆';
    reg.ReplaceAll;
    println reg.Subject;
    
    println '<br>';
    
    reg.Subject := '111, 222, ￥333, ￥444';
    reg.RegEx   := '(?<=￥)\d{3}'; // ?<=
    reg.Replacement := '◆';
    reg.ReplaceAll;
    println reg.Subject;
    
    println '<br>';
    
    reg.Subject := '111, 222, ￥333, ￥444';
    reg.RegEx   := '(?<!￥)\d{3}'; // ?<!
    reg.Replacement := '◆';
    reg.ReplaceAll;
    println reg.Subject;
    
    println '<br>';
  finally
    FreeAndNil(reg);
  end;
end;


println '<h1>Hello,PerlRegEx!</h1><br>';

DoTestRegEx;

?>
</body>
</html>
