<html>
<title>Hello,World</title>
<body>
<?
uses
  SysUtils, Classes, SimpleXml;

function XmlDocumentToStr(AXmlDoc: IXmlDocument): string;
var
  AMS: TMemoryStream;
begin
  Result := '';
  if not Assigned(AXmlDoc) then
    Exit;

  AMS := TMemoryStream.Create;
  try
    AXmlDoc.Save(AMS);
    AMS.Position := 0;
    if AMS.Size > 0 then
    begin
      SetLength(Result, AMS.Size);
      AMS.Read(Result[1], AMS.Size);
    end;
  finally
    FreeAndNil(AMS);
  end;
end;

procedure DoXmlTest;
var
  ADoc: IXmlDocument;
  AElement: IXmlElement;
  ANode, AChild, ATmpNode: IXmlNode;
  AText: IXmlText;

  sTemp: AnsiString;
begin
//
  //注意:
  //1,参数不能少.
  //2.该XML引用的单元为SimpleXml.pas,该原始单元PAS文件可以在网上下载到.所以有关详细使用方法可以参考SimpleXml.pas
  ADoc := CreateXmlDocument('studentcareer', '', '', nil);
  try
    ANode := ADoc.NeedChild(('studentcareer'));
      AElement := ANode.AppendElement(('period'));
        AChild := AElement.AppendElement(('StartTime'));
        AChild.Text := '2012';

        AChild := AElement.AppendElement(('EndTime'));
        AChild.Text := '2015';

        AChild := AElement.AppendElement(('School'));
        AChild.Text := '育红小学';

    //读取值
    ATmpNode := ADoc.SelectSingleNode('studentcareer\period\School');
    if Assigned(ATmpNode) then
    begin
      writeln(ATmpNode.Text);
      print ATmpNode.Text; print '<br>';
    end;

    //方法1:直接获取XML字符串
    sTemp := ADoc.Xml;
    writeln(sTemp);
    print sTemp; print '<br>';
    
    //方法2:可以保存XML文件流
    sTemp := XmlDocumentToStr(ADoc);
    sTemp := Utf8Decode(sTemp);
    writeln(sTemp);
    print sTemp; print '<br>';
    
    //方法3:可以直接保存文件,请参考SimpleXml.pas
    
  finally
    ADoc := nil;
    AChild := nil;
  end;
end;

println '<h1>XML Document test.</h1><br>';

DoXmlTest; print '<br>';


?>
</body>
</html>
