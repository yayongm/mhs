<html>
<title></title>
<body>
<?
uses SysUtils;

var
  b: Boolean;
  s: RawByteString;

//(ARequest: TIdHTTPAppRequest)
//{
function IsAcceptZip: Boolean;
begin
  //Server support deflate compress method now.
  Result := Pos('deflate', Request.RequestInfo.AcceptEncoding) > 0;
end;
//}

println '<h2>Welcome to Moon Http Server world!</h2>';
println 'Congratulations,your server is runing!<br>';
println 'Server time is:' + FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', Now) + '<br>';

//WM.Values['MinZipSize'] := 16384; //Compress if current content size > 16384 Bytes;

  b := IsAcceptZip;

  print 'Your browser supported compress type:';
  print Request.RequestInfo.AcceptEncoding; print '<br>';
  b := Pos('deflate', Request.RequestInfo.AcceptEncoding) > 0;
  if b then
    print 'Server can auto compress your content.<br>';

?>


<h2><a href="Lesson1/">Lesson1 - Hello,World!</a></h2>
<div>It's a simple script.</div>

<h2><a href="Lesson2/">Lesson2 - Static html file</a></h2>
<div>It's a standard html file use default processor.</div>

<h2><a href="Lesson3/">Lesson3 - Huge html file and resource</a></h2>
<div>It's a huge html file to transfer with compress and cache. The server will return compress file at first time, and retrun 304 code at second time. yes, it's very fast and very cool!</div>

<h2><a href="Lesson4/">Lesson4 - Json format</a></h2>
<div>Use superobject as Json libary, is it cool?</div>

<h2><a href="Lesson5/">Lesson5 - Request/Response/Webmodule</a></h2>
<div>About basic web parameters.</div>

<h2><a href="Lesson6/">Lesson6 - Error and Debug</a></h2>
<div>Error information of the script.</div>

<h2><a href="Lesson7/">Lesson7 - Script libary and uses</a></h2>
<div>Use script libary in script.</div>

<h2><a href="Lesson8/">Lesson8 - File stream test</a></h2>
<div>Test file stream.</div>

<h2><a href="Lesson9/">Lesson9 - Database and Query</a></h2>
<div>Get data from database.</div>

<h2><a href="Lesson11/">Lesson11 - Native Memory Cache</a></h2>
<div>Use native memory cache in script.</div>

<h2><a href="Lesson12/">Lesson12 - Session!</a></h2>
<div>Sesson in server, Please refresh page and show the session result.</div>

<h2><a href="Lesson13/">Lesson13 - Template?</a></h2>
<div>Include static file and orgnized like template.</div>

<h2><a href="Lesson14/">Lesson14 - Function extention!</a></h2>
<div>Some advanced function in server.</div>

<h2><a href="Lesson15/">Lesson15 - Performance!</a></h2>
<div>Loop,Sum and Sum(Sqrt) in 1E8 times.</div>

<h2><a href="Lesson16/">Lesson16 - Graphics!</a></h2>
<div>Draw graphic in server and sent to browser.</div>


<br><br><br><br>
<div>
<?
  print 'You can find all script <b>source</b> in wwwroot, Powered by <a href="../mhs/"><b>MoonHttpServer</b></a>, Yeah!';
?>
</div>
</body>
</html>
