<html>
<title>Hello,World</title>
<body>
<form method="post" action="job/add">
        招聘岗位： <input type="text" name="position" id="position"/>
        招聘人数： <input type="text" name="quantity" id="quantity"/>
        学历要求： <input type="text" name="education" id="education"/>
        薪资： <input type="text" name="salary" id="salary"/>
        联系人： <input type="text" name="contact" id="contact"/>
        联系电话： <input type="text" name="telephone" id="telephone"/>
        描述： <textarea name="description" id="description"/></textarea>
        <input type="submit"/>
    </form>
	
<?
uses
  SysUtils, Classes;

procedure TestUnicode;
var
  s: string;
begin
  s := 
'<form method="post" action="job/add">' + sLineBreak +
'        招聘岗位：<input type="text" name="position" id="position"/>' + sLineBreak +
'        招聘人数：<input type="text" name="quantity" id="quantity"/>' + sLineBreak +
'        学历要求：<input type="text" name="education" id="education"/>' + sLineBreak +
'        薪资：<input type="text" name="salary" id="salary"/>' + sLineBreak +
'        联系人：<input type="text" name="contact" id="contact"/>' + sLineBreak +
'        联系电话：<input type="text" name="telephone" id="telephone"/>' + sLineBreak +
'        描述：<textarea name="description" id="description"/></textarea>' + sLineBreak +
'        <input type="submit"/>' + sLineBreak +
'</form>' + sLineBreak + '';

  print s;  
  
end;

println '<h1>Hello,World!</h1><br>';

TestUnicode;
?>
</body>
</html>
