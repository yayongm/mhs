<html>
<title>Hello,Formula Parser</title>
<body>
<?
uses
  SysUtils, Classes, FormulaParser;

//测试类,为方便使用内置类方法而采用的封装  
type  
  TMyTestParser = class
  public
    FParser: TParser;
    procedure DoParserVariable(Sender: TObject; const VarName: string; var Value: Extended; var SetValue: Boolean);
  public
    constructor Create;
    destructor Destroy; override;
  end;
  
constructor TMyTestParser.Create;
begin
  inherited;
  
  FParser := TParser.Create(nil);
  FParser.OnParserVariable := DoParserVariable;
end;
 
destructor TMyTestParser.Destroy;
begin
  if Assigned(FParser) then
    FreeAndNil(FParser);
  
  inherited;  
end; 

procedure TMyTestParser.DoParserVariable(Sender: TObject; const VarName: string; var Value: Extended; var SetValue: Boolean);
begin
  if SameText(VarName, 'MyRnd') then
  begin
    Value := Random(10);
    SetValue := True;
  end;  
end;
  
//测试函数  
procedure DoAdvParserTest;
var
  AParser: TMyTestParser;
  AResult: Extended;
begin
  AParser := TMyTestParser.Create;
  try
    //AParser.Expression := '(sin(1)^2*7)/(5*6+6)-pi';
    AParser.FParser.Expression := '1*pi*MyRnd';
    AResult := AParser.FParser.Value;
    println FloatToStr(AResult); println '<br>';
    println(Format('%.17f', [AResult]));
  finally
    FreeAndNil(AParser);
  end;
end;  

//主入口
begin
println '<h1>Hello,Formula Parser!</h1><br>';

DoAdvParserTest;
end.
?>
</body>
</html>
