<html>
<title>Hello,Formula Parser</title>
<body>
<?
uses
  SysUtils, Classes, FormulaParser;
  
function MyParser(const AExpression: string): Extended;
var
  AParser: TParser;
  AResult: Extended;
begin
  AParser := TParser.Create(nil);
  try
    AParser.Expression := AExpression;
    Result := AParser.Value;
  finally
    FreeAndNil(AParser);
  end;
end;  
  
procedure DoParserTest;
var
  AResult: Extended;
begin
  AResult := MyParser('(sin(1)^2*7)/(5*6+6)-pi');
  
  //println FloatToStr(AResult); println '<br>';
  println(Format('%.17f', [AResult])); println '<br>';
  
  AResult := MyParser('sqrt(2)');
  println(Format('%.17f', [AResult])); println '<br>';

  AResult := MyParser('pi*Power(3,2)');//pi*r^2
  println(Format('%.17f', [AResult])); println '<br>';
end;  

begin
println '<h1>Hello,Formula Parser!</h1><br>';

DoParserTest;
end.
?>
</body>
</html>
