﻿<html>
<title>Some functions</title>
<body>
<h2>Some functions</h2>
<div>
The functions in server!<br>
For instance:<br><br>
</div>

<?
uses
  SysUtils, Classes;
  
function TestStringFunctions: Boolean;
var
  sTemp, s1, s2: string;
  iTemp: Integer;
  i64: Int64;
begin  
  print 'Path Info:<br>';
  print 'Current Page Path:_$PATHPAGE_<br>';
  print 'Root Path:_$PATHROOT_<br>';
  print 'WWW Path:_$PATHWWW_<br>';
  print 'APP Path:_$PATHAPP_<br>';
  print '<br><br>';

  sTemp := '世界，你好！联系人：';
  print 'Original string:'; print sTemp; print '<br><br>';
  
  print 'Base64 function:<br>';
  s1 := B64Encode(sTemp); 
  print s1; print '<br>';
  s2 := B64Decode(s1); 
  print s2; print '<br><br>';
  
  s1 := 'Name=张三&Friends=李四,王五';
  s2 := UrlEncode(s1);
  print 'function UrlEncode(const svar: string): string; | ' + s2 + '<br>';
  print 'function UrlDecode(U: string): string; | ' + UrlDecode(s2) + '<br>';
  
  print 'Hash64 function(murmur method):<br>';
  i64 := HashOf64(sTemp); 
  print Format('%u', [i64]); print '<br><br>';
  
  print 'GetTickCount:<br>';
  print Format('%u', [GetTickCount]); print '<br><br>';
  
  print 'MD5:<br>';
  print MD5Hex(sTemp); print '<br><br>';

  print 'function SHA1(Input: string; const IsLowerCase: Boolean = True): string;<br>';
  print SHA1(Utf8Decode(sTemp)); print '<br>';

  {//下述函数需要ssl的32位dll支持,如有需要,请到群中下载.
  print SHA224(Utf8Decode(sTemp)); print '<br>';
  print SHA256(Utf8Decode(sTemp)); print '<br>';
  print SHA384(Utf8Decode(sTemp)); print '<br>';
  print SHA512(Utf8Decode(sTemp)); print '<br>';
  //}
  
  print 'QueryPerformanceFrequency:<br>';
  QueryPerformanceFrequency(i64);
  print Format('%u', [i64]); print '<br><br>';
  print 'QueryPerformanceCounter:<br>';
  QueryPerformanceCounter(i64);
  print Format('%u', [i64]); print '<br><br>';
end;

function TestHttpFunctions: Boolean;
var
  sTemp: string;
begin
  {
  print 'HttpGet:<br>';
  print 'function HttpRequestExecute(const URL, QueryData: string; UserAgent: string; Port: Integer; IsBase64: Boolean): string;<br>';
  sTemp := HttpRequestExecute('http://www.baidu.com/', '', '', 80, False);
  print Format('RecvLength: %d', [Length(sTemp)]);  print '<br>';
  print 'function HttpReqExec(const URL, QueryData: string; UseUtf8: Boolean; UserAgent: string; Port: Integer): string;<br>';
  sTemp := HttpReqExec('http://www.baidu.com/', '', False, '', 80);
  print Format('RecvLength: %d', [Length(sTemp)]);  print '<br><br>';
  //}
end;


function TestSnowAlgorithmFunctions: Boolean;
var
  i64: Int64;
begin
  print 'Snow Algorithm from Facebook:<br>';
  print 'function SnowInit(const ACurMachineID: Word): Boolean;<br>';
  print 'Init current server id, It will be execute once.<br>';
  SnowInit(1);
  print 'function SnowID: Int64;<br>';
  print 'Generate ID:<br>';
  i64 := SnowID;
  print Format('%u', [i64]); print '<br>';
  i64 := SnowID;
  print Format('%u', [i64]); print '<br><br>';
end;

function TestFileFunctions: Boolean;
var
  sTemp: string;
begin
  print 'File Functions:<br>';
  sTemp := wm.FullFileName('_$PATHPAGE_/index.pp');
  print 'FileName:' + sTemp + '<br>';
  print 'FilePath:' + ExtractFilePath(sTemp) + '<br>';
  print 'FileAge:' + IntToStr(FileAge(sTemp)) + '<br>';
  print 'FileAgeToDateTime:' + Format('%.8f', [FileAgeToDateTime(sTemp)]) + '<br>';
  print 'FileSize:' + IntToStr(FileSize(sTemp)) + '<br><br>';
end;

function TestDateTimeFunctions: Boolean;
var
  dNow: TDateTime;
  iUnixTime: Int64;
begin
  print 'Time Functions:<br>';
  dNow := NowUTC;
  print 'function NowUTC: TDateTime; | ' + Format('%.8f', [dNow]) + ' | ' + DateTimeToStr(dNow) + '<br>';
  print 'function UnixTimeUTC: Int64; | ' + Format('%u', [UnixTimeUTC]) + '<br>';
  dNow := Now;
  print 'Now:' + Format('%.8f', [dNow]) + '<br>';
  iUnixTime := DateTimeToUnixTime(dNow);
  print 'function DateTimeToUnixTime(const AValue: TDateTime): Int64; | ' + Format('%u', [iUnixTime]) + '<br>';
  print 'function UnixTimeToDateTime(const UnixTime: Int64): TDateTime; | ' + Format('%.8f', [UnixTimeToDateTime(iUnixTime)]) + '<br>';
  
  print '<br>';
end;

function TestSystemInfoFunctions: Boolean;
var
  AGuid: TGuid;
  s1, s2: string;
begin
  print 'SystemInfo Functions:<br>';
  print 'function GetCurrentThreadID: Cardinal; | ' + IntToStr(GetCurrentThreadID) + '<br>';
  print 'function GetTickCount64: Int64; | ' + Format('%u', [GetTickCount64]) + '<br>';
  print 'function Random32(max: cardinal): cardinal; | ' + Format('%u', [Random32($FFFFFFFF)]) + '<br>';
  print 'function Rand64(const IsUInt64: Boolean = False): Int64; | ' + Format('%x', [Rand64(True)]) + '<br>';
  
  print 'function GUIDToString(const guid: TGUID): string;' + '<br>';
  AGuid := NewGuid;
  print 'function NewGuid: TGUID; | ' + GUIDToString(AGuid) + '<br>';
  print 'function NewGuidStr(IsLowerCase: Boolean = False): string; | ' + NewGuidStr(True) + '<br>';
  
  
  print '<br>';
end;

function TestCompressFunctions: Boolean;
var
  s: string;
begin
  s := '世界，你好！';
  print 'Original string:'; print s; print '<br><br>';
  
  print 'function GzCompress(var DataRawByteString: AnsiString; Compress: boolean): AnsiString; <br>';
  GzCompress(s, True);
  print 'Compressed:' + B64Encode(s) + '<br>';
  GzCompress(s, False);
  print 'Uncompressed:' + s + '<br><br>';
  
  print 'function DeflateCompress(var DataRawByteString: AnsiString; Compress: boolean): AnsiString; <br>';
  DeflateCompress(s, True);
  print 'Compressed:' + B64Encode(s) + '<br>';
  DeflateCompress(s, False);
  print 'Uncompressed:' + s + '<br><br>';
  
  print 'function ZLibCompress(var DataRawByteString: AnsiString; Compress: boolean): AnsiString; <br>';
  ZLibCompress(s, True);
  print 'Compressed:' + B64Encode(s) + '<br>';
  ZLibCompress(s, False);
  print 'Uncompressed:' + s + '<br><br>';
  
  
end;


begin 

  TestStringFunctions; 
  
  TestHttpFunctions;
  
  TestSnowAlgorithmFunctions;
  
  TestFileFunctions;
  
  TestDateTimeFunctions;
  
  TestSystemInfoFunctions;
  
  TestCompressFunctions;
  
  print '<br>';
end;
?>

</body>
</html>
