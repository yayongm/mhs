<html>
<title>Hello,World</title>
<body>
<?
uses
  SysUtils, Classes;

type
  TStudent = packed record
    Name: string[15];
	Sex: Byte;
	Age: Byte;
	Birth: TDateTime;
	Address: string[63];
	Mobile: string[31];
  end;
  PStudent = ^TStudent;

var
  pStu: PStudent;
  AMS: TMemoryStream;
  p: Pointer;
begin  
  New(pStu);
  try
    FillChar(pStu^, SizeOf(TStudent), 0);
	pStu.Name := '张三';
    println pStu.Name + '<br>';
	println IntToStr(SizeOf(TStudent)) + '<br>';
  finally
    FreeMem(pStu, SizeOf(TStudent));
	//Dispose(pStu);
  end;
  
  AMS := TMemoryStream.Create;
  try
    AMS.Size := 128;
	p := AMS.Memory;
	Move('Hello', p^, 5);
	//AMS.SaveToFile('c:\aaa.txt');
  finally
    FreeAndNil(AMS);
  end;
  
  println '<h1>Hello,World!</h1><br>';
end;
?>
</body>
</html>
