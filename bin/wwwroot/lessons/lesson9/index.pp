<html>
<title>Lesson9</title>
<body>
<?
uses
  SysUtils, Classes, Db;

function DebugDataSetHeader(ADS: TDataSet): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to ADS.FieldCount - 1 do
begin
  Result := Result + '<th>' + ADS.Fields[i].FieldName + '</th>' + sLineBreak;
end;

end;

function DebugDataSetData(ADS: TDataSet): string;
var
  i: Integer;
begin
  Result := '';
  if ADS.RecordCount < 1 then
    Exit;

  ADS.First;
  while not ADS.Eof do
  begin
    Result := Result + '<tr>';
    for i := 0 to ADS.FieldCount - 1 do
    begin
      Result := Result + '<td>' + ADS.Fields[i].AsString + '</td>';
    end;
    Result := Result + '</tr>' + sLineBreak;

  ADS.Next;
end;

end;

function DebugDataSet(ADS: TDataSet): string;
begin
  if not ADS.Active then
    Exit;

  Result := '<table border="1">' + sLineBreak;
  Result := Result + DebugDataSetHeader(ADS);
  Result := Result + DebugDataSetData(ADS);

  Result := Result + '</table>';
end;

function DebugTableList(ADBName: string = ''): string;
var
  AQuery: TWmQuery;
  ASL: TStrings;
  i: Integer;
  sDbName: string;
begin
  AQuery := wm.NewQuery('', ADBName);
  if not Assigned(AQuery) then
  begin
    sDbName := ADBName;
    if Length(sDbName) < 1 then 
      sDbName := 'Default';
    Writeln(Format('Err(DebugTableList):Get database(%s) connection fail.', [sDbName]));
    Exit;
  end;
  
  ASL := TStringList.Create;
  try
    Result := '';
    ASL.Text := AQuery.GetTableList;

    Result := '<table border="1">' + sLineBreak;
    Result := Result + '<th>Tables in Database</th>' + sLineBreak;
    for i := 0 to ASL.Count - 1 do
      Result := Result + '<tr><td>' + ASL.Strings[i] + '</td></tr>' + sLineBreak;
    Result := Result + '</table>' + sLineBreak;
    
  finally
    FreeAndNil(ASL);
    FreeAndNil(AQuery);
  end;
end;

function DebugTable(ATableName: string): string;
var
  AQuery: TWmQuery;
  i, iCount: Integer;
  s: string;
begin
  AQuery := wm.NewQuery('');
  
  AQuery.SQL.Text := 'select * from ' + ATableName;
  AQuery.Execute;
  if not AQuery.Active then
  begin
    print 'DataSet is not Open.';
    Exit;
  end;

  Result := DebugDataSet(AQuery.DataSet);
end;

function DebugTableJson(ATableName: string): string;
var
  AQuery: TWmQuery;
  i, iCount: Integer;
  s: string;
begin
  AQuery := wm.NewQuery('');
  
  AQuery.SQL.Text := 'select * from ' + ATableName;
  AQuery.Execute;
  if not AQuery.Active then
  begin
    print 'DataSet is not Open.';
    Exit;
  end;

  //function DataSetToJSON(Data: TDataSet; const RecNo: Integer = 0; const NeedFieldName: Boolean = True): RawByteString;
  Result := DataSetToJson(AQuery.DataSet);
end;

function TestTransaction: Boolean;
var
  AQuery: TWmQuery;
begin
  AQuery := wm.NewQuery('');
  
  try
    AQuery.StartTransaction;
    try
      AQuery.SQL.Text := 'Update employee Set Salary=Salary + 100';
      
      AQuery.Execute;
      
      if AQuery.InTransaction then
      begin
        AQuery.Commit;
        
        Result := True;
      end;
    except
      on E: Exception do
      begin
        AQuery.Rollback;
      end;
    end;
  finally
    FreeAndNil(AQuery);
  end;  
end;

?>

<h2>Database</h2>
<div>Table list:<div>
<?
  print DebugTableList;
  print 'Tablename:country,Data list:<br>';

  //TestTransaction;

  print DebugTable('country');
  print 'Tablename:employee,Data list:<br>';
  print DebugTable('employee');
  print 'Tablename:customer,Data list:<br>';
  print DebugTable('customer');
  
  print 'Tablename:Country,Data list(Json):<br>';
  print DebugTableJson('country');
?>

<br>
<div>Generate time:<? print FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', Now); ?><div>

</body>
</html>