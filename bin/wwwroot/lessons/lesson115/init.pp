uses
  SysUtils, Classes, ExtCtrls;

{//导入的两个关键函数  
  function MhsNewSysTimer(const AInterval: Integer; const AScriptFileName: string; RunImediatly: Boolean; const ATimerName: string): Boolean;
  function MhsGetSysComponent(const AName: string): TComponent;
//}

procedure DoInitTimer;
var
  ATimer: TTimer;
begin
  //创建Timer并设置初始Tick值;
  MhsNewSysTimer(30000, 'wechat.pp', True, 'tmrWechat');//半分钟一次调用
  gServerVar.I['WechatTick'] := 0;
  
  {
  //可以动态设置Timer的Interval值
  ATimer := TTimer(MhsGetSysComponent('tmrWechat'));
  if Assigned(ATimer) then
  begin
    ATimer.Interval := 1000;
  end;
  //}
end;

begin
  DoInitTimer;
end.