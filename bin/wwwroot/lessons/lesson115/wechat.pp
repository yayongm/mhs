uses
  SysUtils, Classes, ExtCtrls,
  BaseDebug in 'wwwroot\public\BaseDebug.pp', 
  BaseWechat in 'wwwroot\public\BaseWechat.pp',
  DB;
  
procedure DoQueryTest;
var
  ACn: TObject;
  AQuery: TWmQuery;
begin
  {
  //注意:
  1.因为该方法为进程级的全局方法,因此在使用时需要特别小心!!!!!
  2.必须动态获取数据库连接,因为sqlite,access是不支持多个连接的,所以必须动态申请(Lock),动态释放(Unlock),否则,必死无疑.
  3.此处仅为演示如何在进程级的定时任务中执行数据库操作,因此请特别注意!!!
  当然,因为支持了进程级的定时任务和数据库操作,所以有了更多更富想象力的强大功能!!!
  }
  ACn := MhsLockDbConnection('');//获取默认数据库连接
  try
    if not Assigned(ACn) then
      Exit;

    AQuery := MhsNewQuery('', ACn);//创建临时Query
    try
      AQuery.SQL.Text := 'SELECT Count(*) FROM article';
      AQuery.Open;
    
      writeln('Info(DoQueryTest):Article Count:' + AQuery.Fields[0].AsString);
    finally
      FreeAndNil(AQuery);
    end;
  finally
    MhsUnlockDbConnection(ACn);
  end;
end;  
  
procedure DoWechat;
begin
  //定时获取access_token
  TBaseWechat.AutoRefreshToken;
end;  

begin
  DoWechat;
  //DoQueryTest;
end.