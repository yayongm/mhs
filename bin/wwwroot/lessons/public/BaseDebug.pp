unit BaseDebug;

interface

uses
  SysUtils, Classes, Variants;

type
  TBaseDebug = class
  public
	class function DebugReq: string;
  end;

function DebugVar(const Value: Variant; const AName: string): string;

implementation

function DebugVar(const Value: Variant; const AName: string): string;
var
  i: Integer;
begin
  Result := '';
  if Length(AName) > 0 then
    Result := Result + AName + ':';
  
  if not VarHasValue(Value) then 
  begin
    Result := Result + '(null)';
	
	Exit;
  end;	
	
  if VarIsArray(Value) then
  begin
    for i := 0 to Length(Value) - 1 do
	  Result := Result + Format('[%d]:', [i]) + VarToStr(Value[i]) + sLineBreak;
  end
  else
    Result := Result + VarToStr(Value);
end;

function InnerPrintHeader(AName, v1: Variant): string;
begin
  Result := '';

  Result := Result + '<table border="1">';
  Result := Result + '<tr>';
  Result := Result + '<th width="20%">' + VarToStr(AName) + '</th>';
  Result := Result + '<th width="80%">' + VarToStr(v1) + '</th>';
  Result := Result + '<tr>' + sLineBreak;
end;

function InnerPrintRow(AName, v1: Variant): string;
begin
  Result := '';
  
  Result := Result + '<tr>';
  Result := Result + '<td>' + VarToStr(AName) + '</td>';
  Result := Result + '<td>' + VarToStr(v1) + '</td>';
  Result := Result + '<tr>' + sLineBreak;
end;

class function TBaseDebug.DebugReq: string;
const
  C_SPEC = ' | ';
var
  i, iCount: Integer;
begin
  Result := '';
  Result := Result + InnerPrintHeader('Names', 'Values');
  Result := Result + InnerPrintRow('URL', Request.URL);
  Result := Result + InnerPrintRow('Original Data', Request.RequestInfo.InContent);
  
  iCount := Request.QueryFields.Count;
  Result := Result + InnerPrintRow('Request.Method', Request.Method);
  Result := Result + InnerPrintRow('Request.QueryFields.Count', iCount);
  
  for i := 0 to iCount - 1 do
  begin
    Result := Result + InnerPrintRow(Format('&nbsp;&nbsp;&nbsp;&nbsp;QueryFields[%d]', [i]), Format('%s=%s', [Request.QueryFields.Names[i], Request.QueryFields.ValueFromIndex[i]]));
  end;
  
  iCount := Request.ContentFields.Count;
  Result := Result + InnerPrintRow('Request.ContentFields.Count', iCount);

  for i := 0 to iCount - 1 do
  begin
    Result := Result + InnerPrintRow(Format('&nbsp;&nbsp;&nbsp;&nbsp;ContentFields[%d]', [i]), Format('%s=%s', [Request.ContentFields.Names[i], Request.ContentFields.ValueFromIndex[i]]));
  end;
  
  //下面是不太重要的内容
  Result := Result + InnerPrintRow('====================', '====================');
  Result := Result + InnerPrintRow('Host', Request.Host);
  Result := Result + InnerPrintRow('RemoteIP', Request.RemoteAddr);//Request.RequestInfo.RemoteIP
  Result := Result + InnerPrintRow('Cookies.CookieFileds', Request.CookieFields.Text);
  Result := Result + InnerPrintRow('Cookies.Count', Request.CookieFields.Count);
  Result := Result + InnerPrintRow('Referer', Request.Referer); 
  Result := Result + InnerPrintRow('UserAgent', Request.UserAgent);  
  
  Result := Result + InnerPrintRow('====================', '====================');
  Result := Result + InnerPrintRow('Accept', Request.Accept);
  Result := Result + InnerPrintRow('ContentEncoding', Request.ContentEncoding + C_SPEC + Request.RequestInfo.ContentEncoding);
  Result := Result + InnerPrintRow('ContentLanguage', Request.RequestInfo.ContentLanguage);
  Result := Result + InnerPrintRow('Date', FloatToStr(Request.Date) + C_SPEC + FloatToStr(Request.RequestInfo.Date));
  Result := Result + InnerPrintRow('Expires', FloatToStr(Request.Expires) + C_SPEC + FloatToStr(Request.RequestInfo.Expires));
  Result := Result + InnerPrintRow('ETag', Request.RequestInfo.ETag);
  Result := Result + InnerPrintRow('LastModified', Request.RequestInfo.LastModified);
  Result := Result + InnerPrintRow('Pragma', Request.RequestInfo.Pragma);
  
  Result := Result + '</table>';  
end;

end.