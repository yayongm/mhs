unit BaseUI;

interface

uses
  SysUtils, Classes, Variants;
  
type
  TBaseUI = class
  public
    //基础,客户端是否支持压缩
    class function IsAcceptZip: Boolean;
  public
    //<select>,combobox
    class function GetSelComboItem(Values, Names: Variant; ASelValue: Variant): string;
	class function GetSelCombo(Values, Names: Variant; ASelValue: Variant; ANameTag: string = ''): string;
  end;

implementation

class function TBaseUI.IsAcceptZip: Boolean;
begin
  Result := Pos('deflate', Request.RequestInfo.AcceptEncoding) > 0;
end;

class function TBaseUI.GetSelComboItem(Values, Names: Variant; ASelValue: Variant): string;
var
  i: Integer;
begin
  Result := '';
  for i := 0 to Length(Values) - 1 do
  begin
    Result := Result + '  <option value="' + VarToStr(Values[i]) + '"';
	if Values[i] = ASelValue then
	  Result := Result + ' selected="selected">'
	else
      Result := Result + '>';	
	
    Result := Result + VarToStr(Names[i]) + '</option>' + sLineBreak;	
  end;
end;

class function TBaseUI.GetSelCombo(Values, Names: Variant; ASelValue: Variant; ANameTag: string = ''): string;
begin
  if ANameTag <> '' then  
    Result := '<select ' + ANameTag + '>' + sLineBreak
  else	
    Result := '<select>' + sLineBreak;
	
  Result := Result + GetSelComboItem(Values, Names, ASelValue) + '</select>' + sLineBreak;	
end;

end.  