unit BaseQuery;

interface

uses
  SysUtils, Classes, Variants, DB;

type
  TBaseQry = class
  public
    class function DbQuery(var AQuery: TWmQuery; const ASQL: string; AValues: Variant): Integer;
	class function GetMaxID(ATableName, AFieldName: string): Integer;
	class function GetRecordCount(ATableName: string; AWhereCondition: string = ''): Integer;
    class function FillRecordFromReq(AQuery: TWmQuery; IncludeQueryFields: Boolean = False; NeedDecode: Boolean = True): Integer;
  end;

function DbUtf8(const Value: string; IsEncode: Boolean = True): string; //将数据库字段转为Utf8,为了兼容Access

implementation

function DbUtf8(const Value: string; IsEncode: Boolean): string; 
begin
  //如果不是Access数据库,直接返回源数据Value即可.
  if IsEncode then
    Result := Utf8Encode(Value)
  else
    Result := Utf8Decode(Value);  
end;

class function TBaseQry.DbQuery(var AQuery: TWmQuery; const ASQL: string; AValues: Variant): Integer;
var
  i: Integer;
begin
  Result := -1;
  if not Assigned(AQuery) then
  begin
    AQuery := wm.NewQuery('');
  end  
  else
  begin
    AQuery.Active := False;  
  end;  

  AQuery.SQL.Text := ASQL;
	
  for i := 0 to Length(AValues) - 1 do
  begin
    AQuery.ParamByIndex(i).Value := AValues[i];
  end;

  try
    AQuery.Open;
    Result := AQuery.Fields[0].AsInteger;
  except
    on E: Exception do
    begin
      writeln(Format('Err(BaseQuery.TBaseQry.DbQuery):%s,%s', [E.Message, ASQL]));
    end;
  end;
end;

class function TBaseQry.GetMaxID(ATableName, AFieldName: string): Integer;
var
  AQuery: TWmQuery;
  sSQL: string;
begin
  try
    sSQL := Format('select Max(%s) from %s', [AFieldName, ATableName]);
    DbQuery(AQuery, sSQL, VarArrayOf([]));
	Result := AQuery.Fields[0].AsInteger;
  finally
    FreeAndNil(AQuery);
  end;
end;

class function TBaseQry.GetRecordCount(ATableName: string; AWhereCondition: string = ''): Integer;
var
  AQuery: TWmQuery;
  sSQL: string;
begin
  try
    sSQL := Format('select Count(*) from %s', [ATableName]);
	if AWhereCondition <> '' then
	  sSQL := sSQL + ' where (' + AWhereCondition + ')'; 
	
    DbQuery(AQuery, sSQL, VarArrayOf([]));
	Result := AQuery.Fields[0].AsInteger;
  finally
    FreeAndNil(AQuery);
  end;
end;

{
function GetMaxID(ATableName, AFieldName: string): Integer;
var
  AQuery: TWmQuery;
begin
  AQuery := wm.NewQuery('');
  try
    AQuery.SQL.Text := Format('select Max(%s) from %s', [AFieldName, ATableName]);
	AQuery.Open;
	Result := AQuery.Fields[0].AsInteger;
  finally
    FreeAndNil(AQuery);
  end;
end;

function GetRecordCount(ATableName: string; AWhereCondition: string = ''): Integer;
var
  AQuery: TWmQuery;
  sSQL: string;
begin
  AQuery := wm.NewQuery('');
  try
    sSQL := Format('select Count(*) from %s', [ATableName]);
	if AWhereCondition <> '' then
	  sSQL := sSQL + ' where (' + AWhereCondition + ')'; 
    AQuery.SQL.Text := sSQL;
	AQuery.Open;
	Result := AQuery.Fields[0].AsInteger;
  finally
    FreeAndNil(AQuery);
  end;
end;
}

class function TBaseQry.FillRecordFromReq(AQuery: TWmQuery; IncludeQueryFields: Boolean = False; NeedDecode: Boolean = True): Integer;
var
  i: Integer;
  sFieldName, sFieldValue: string;
  AField: TField;
begin
  Result := 0;
  if not (Assigned(AQuery) and AQuery.Active) then 
    Exit;

  //if not (AQuery.DataSet.State in [dsEdit, dsInsert]) then
  //  Exit;
	
  for i := 0 to AQuery.Fields.Count - 1 do
  begin
    AField := AQuery.Fields[i];
    sFieldName := AField.FieldName;
    sFieldValue := Request.ContentFields.Values[sFieldName];
    if (Length(sFieldValue) < 1) and IncludeQueryFields then
	  sFieldValue := Request.QueryFields.Values[sFieldName];
	if (Length(sFieldValue) < 1) then
      Continue;

    if NeedDecode then
      AField.Value := DbUtf8(UrlDecode(sFieldValue), False)
	else
      AField.Value := DbUtf8(sFieldValue, False);
	  
	Inc(Result);
  end;
end;


end.