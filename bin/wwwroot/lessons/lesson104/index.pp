<html>
<title>Upload file</title>
<body>
<?
uses
  SysUtils, Variant, Classes;

procedure DoTestUpload;
var
  sTemp: string;
  
  sData, sDataFileName, sContentType: string;

  vFiles, AFile: Variant;
begin
  if ExtractUploadFile(Request.RequestInfo.InContent, vFiles) > 0 then
  begin
    {
	AUFI.Data, AUFI.DataFileName, AUFI.Content_Type,
    AUFI.DataTypeName, AUFI.DataName, AUFI.Key, AUFI.Content_Dispositon
	}
	AFile := vFiles[0];//First file
	sData := VariantToRawStr(AFile[0]);
	sDataFileName := AFile[1];
	sContentType := AFile[2];
	
    sTemp := FullAppFile('_$PATHUPLOADS_' + StringReplaceAll(sDataFileName, '"', ''));
    if StrToFile(sData, sTemp) > 0 then
	begin
	  //print 'ok';
	end;
  end;
end;

procedure DoFileTest;
var
  s: string;
begin
  s := StrFromFile('c:\pno.txt');
  print s;
end;

print FullAppFile('_$PATHUPLOADS_');
print '<br>';

DoFileTest;  
?>
</body>
</html>
