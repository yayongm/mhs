<html>
<title>Lesson6</title>
<body>
<?
uses
  SysUtils, Classes;

function StrToHtml(Value: string): string;
begin
  Result := Value;
  Result := StringReplace(Result, ' ', '&nbsp;', [rfReplaceAll]);
  Result := Result + '<br>';
end;

procedure PrintText(Value: string; IsPrintLn: Boolean = False);
var
  sValue: string;
  ASL: TStrings;
  i: Integer;
begin
  ASL := TStringList.Create;
  try
    ASL.Text := Value;
    for i := 0 to ASL.Count - 1 do
      println StrToHtml(ASL.Strings[i]);
  finally
    FreeAndNil(ASL);
  end;
end;

?>

<h2>Error: Syntax Error</h2>
<div>
The following script will be get Syntax Error:<br>

<?
PrintText('' +
'uses' + sLineBreak +
'  SysUtils;' + sLineBreak +
'MyCall();' + sLineBreak +
'');
print '<br>';
?>

The WebServer will like this:<br>
<img src="<? print Request.URL + 'syntexerr.png'; ?>"></img><br>


<a href="<? print Request.URL + 'syntexerr.pp'; ?>">I want to try</a>
<div>

<h2>Error: Runtime Error</h2>
<div>
The following script will be get Runtime Error:<br>

<?
PrintText('' +
'uses' + sLineBreak +
'  SysUtils;' + sLineBreak +
'var' + sLineBreak +
'  f: Cardinal;' + sLineBreak +
'f := 123.5 / 0;' + sLineBreak +
'print f;' + sLineBreak +
'');
print '<br>';
?>

The WebServer will like this:<br>
<img src="<? print Request.URL + 'runtimeerr.png'; ?>"></img><br>

<a href="<? print Request.URL + 'runtimeerr.pp'; ?>">I want to try</a>
</div>

<h2>Description</h2>
<div>
The error text have 4 sections, Separator is "|":<br>
1.Error occur time.<br>
2.Error type and error message. Syntax error is occured in script compile, and Runtime error is in script running.<br>
3.Error file name.<br>
4.Error code line and the content. "Ln:10" is in the 10s line,and "Code:f := 123.5 / 0;" is the content of the error line.<br>
</div>

<h2>Other</h2>
<div>
Is it easy? Yeah!!! It's so easy.
</div>

</body>
</html>