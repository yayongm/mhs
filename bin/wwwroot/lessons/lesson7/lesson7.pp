unit lesson7;

interface

uses
  SysUtils, Classes;

function StrToHtml(Value: string): string;
procedure PrintText(Value: string);

implementation

function StrToHtml(Value: string): string;
begin
  Result := Value;
  Result := StringReplace(Result, ' ', '&nbsp;', [rfReplaceAll]);
  Result := Result + '<br>';
end;

procedure PrintText(Value: string);
//In the Public WebModule, you have to declare every variant with the "var" indicator.
var sValue: string;
var ASL: TStringList;
var i: Integer;
begin
  ASL := TStringList.Create;
  try
    ASL.Text := Value;
    for i := 0 to ASL.Count - 1 do
      println StrToHtml(ASL.Strings[i]);
  finally
    FreeAndNil(ASL);
  end;

  print 'Your calling is from public function at ';
  print FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', Now);
  print '<br>';
end;

end.