<html>
<title>Lesson8</title>
<body>
<h2>Stream</h2>
<?
uses
  SysUtils, Classes;

function TestFileStream(AFileName: string): Integer;
var
  AFS: TStream;
begin
  Result := -1;
  
  if not FileExists(AFileName) then
    Exit;

  AFS := TFileStream.Create(AFileName, fmOpenRead + fmShareDenyWrite);
  try
    Result := AFS.Size;
  finally
    FreeAndNil(AFS);
  end; 
end;

//println '<h1>Hello,World!</h1><br>';
?>
</body>
</html>
