<?
uses
  SysUtils, Classes,
  BaseDebug in '..\..\public\BaseDebug.pp',
  BaseWechat in '..\..\public\BaseWechat.pp';

procedure DoTestRecvText;
var
  sSrc, sTemp: string;
  AUMT: TWxUserMsgText;
  sErrMsg: string;
begin
  sSrc := Request.RequestInfo.InContent;

  //print sSrc;

  if TWechatMsgFn.WxXmlToUserMsgText(sSrc, AUMT) then
  begin
    writeln(AUMT.ToUserName);
    writeln(AUMT.URL);
    
    //返回给用户消息
    print TWechatMsgFn.ReturnMsgText(AUMT.FromUserName, AUMT.ToUserName, AUMT.Content);
    //print TWechatMsgFn.ReturnNewsText(AUMT.FromUserName, AUMT.ToUserName, 'title1', 'description1', 'picurl1', 'url1');
  end;
end;

begin
  DoTestRecvText;
end.
?>