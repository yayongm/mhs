<html>
<title>Wechat Test</title>
<body>
<?
uses
  SysUtils, Classes,
  BaseDebug in '..\..\public\BaseDebug.pp',
  BaseWechat in '..\..\public\BaseWechat.pp';

procedure DoTestGetServerList;
var
  sErr, sTemp: string;
begin
  println '<div>DoTestGetServerList:<br>';
  
  sTemp := TBaseWechat.get_server_list(sErr);
  println sErr; println '<br>';
  println sTemp; println '<br>';
  
  println '</div>';
end;

procedure DoTestMediaUpload;
var
  sErr, sTemp: string;
begin
  println '<div>DoTestMediaUpload:<br>';
  
  sTemp := TBaseWechat.media_upload('image', 'c:\aa.jpg', sErr);
  println sErr; println '<br>';
  println sTemp; println '<br>';
  
  println '</div>';
end;
  
procedure DoTestMenuCreate;
var
  sErr, sTemp, sMenu: string;
  iTemp: Integer;
begin
  println '<div>DoTestMenuCreate:<br>';
  
  sMenu := '
{
"button":[
  {
    "type":"view",
    "name":"MHS测评",
    "key":"evaluation_0011",
    "url":"http://www.moonserver.cn/"
  },
  {
    "type":"view",
    "name":"内部测评",
    "key":"evaluation_0021",
    "url":"http://www.moonserver.cn/"
  },
  {
    "name":"我的服务",
    "sub_button":[
      {
        "type":"view",
        "name":"个人中心",
        "url":"http://www.moonserver.cn/"
      },
      {
        "type":"view",
        "name":"录入保单",
        "url":"http://www.moonserver.cn/"
      },
      {
        "type":"view",
        "name":"录入进度",
        "url":"http://www.moonserver.cn/"
      },
      {
        "type":"view",
        "name":"我的保单",
        "url":"http://www.moonserver.cn/"
      }]
  }]
}
  ';
  
  iTemp := TBaseWechat.menu_create(sMenu, sErr);
  println sErr; println '<br>';
  println iTemp; println '<br>';
  
  println '</div>';
end;

procedure DoTestMenuGet;
var
  sResult, sErr: string;
begin
  println '<div>DoTestMenuGet:<br>';

  sResult := TBaseWechat.menu_get(sErr);
  if sErr <> '' then
    println sErr
  else
    println sResult;  

  println '<br>';
  println '</div>';
end; 

procedure DoTestMenuDelete;
var
  sErr: string;
  iResult: Integer;
begin
  println '<div>DoTestMenuDelete:<br>';

  iResult := TBaseWechat.menu_Delete(sErr);
  if iResult > 0 then
    println sErr
  else
    println iResult;  

  println '<br>';
  println '</div>';
end; 
 
procedure DoGetUser;
var
  iTotal, iCount: Integer;
  sNextOpenID: string;
  sErr: string;
begin
  println '<div>DoTestMenuDelete:<br>';

  if TBaseWechat.user_get(iTotal, iCount, sNextOpenID, sErr) = 0 then
  begin
    println(Format('Total:%d,Count:%d,NextOpenID:%s', [iTotal, iCount, sNextOpenID]));  
  end;

  println '<br>';
  println '</div>';
end;

procedure DoTestCustomMessage;
var
  sErr, sTemp: string;
begin
  println '<div>DoTestCustomMessage:<br>';

  sTemp := '你好！Hello,World!' + sLineBreak + '当前服务器的时间是：' + FormatDateTime('yyyy-mm-dd hh:nn:ss.zzz', Now);
  TBaseWechat.message_custom_send('oOYuB5vtlUdbYYRQOmhhcOtGiL-8', 'text', sTemp, sErr);
  println sErr; println '<br>';

  println '<br>';
  println '</div>';
end;

//测试消息数据转换
procedure DoTestUserMsgText;
var
  sSrc, sTemp: string;
  AUMT: TWxUserMsgText;
begin
sSrc := '
<xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>
  <FromUserName><![CDATA[fromUser]]></FromUserName>
  <CreateTime>1348831860</CreateTime>
  <MsgType><![CDATA[text]]></MsgType>
  <Content><![CDATA[this is a test]]></Content>
  <MsgId>1234567890123456</MsgId>
</xml>
';
  TWechatMsgFn.WxXmlToUserMsgText(sSrc, AUMT);

  sTemp := TWechatMsgFn.WxUserMsgTextToXml(AUMT);

  println sTemp;
end;

 
begin
  //测试token,此处使用的是动态调用(每次都检查调用)的方法,可以根据需要放到MHS进程的定时器事件中.
  TBaseWechat.AutoRefreshToken;
  //测试获取微信服务器IP列表
  //DoTestGetServerList;

  //测试文件上传
  //DoTestMediaUpload;  

  //测试创建菜单
  //DoTestMenuCreate;
  //测试获取菜单
  //DoTestMenuGet;
  //测试删除菜单
  //DoTestMenuDelete;

  //获取关注列表
  //DoGetUser;
  
  //测试客服消息
  //DoTestCustomMessage;
  
  DoTestUserMsgText;
end.  
  
?>
</body>
</html>
