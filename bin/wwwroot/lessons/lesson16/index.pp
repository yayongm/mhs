<html>
<title>Graphics</title>
<body>
<div>Graphics functions in server.</div><br>
<?
uses
  SysUtils, Graphics;

var
  ABmp: TBitmap;
  sJpeg, sTemp: string;
  sPng: string; 
begin
  ABmp := TBitmap.Create;
  try
    ABmp.Width := 64;
    ABmp.Height := 64;
	
	//Fill background
    ABmp.Canvas.Brush.Color := RGB(Random(255), Random(255), Random(255));
    ABmp.Canvas.FillRect(Rect(0, 0, 64, 64));

    //Draw a text
    ABmp.Canvas.Font.Color := clRed;
    ABmp.Canvas.TextOut(0, 10, 'Hello');

    //Draw a line
    ABmp.Canvas.MoveTo(0, 0);
    ABmp.Canvas.LineTo(63, 63);

    //draw a pixel
    ABmp.Canvas.Pixels[0, 63] := clBlack;
	
	print 'Jpeg image:<br>';
    //Jpeg format
    sJpeg := MnBmpToJpg(ABmp, 75); //Compress Rate;
	sTemp := '<img src="data:image/jpeg;base64,' + B64Encode(sJpeg) + '"/>';
	println sTemp;
	
	print '<br><br>';
	print 'Png image:<br>';
	//Png format
	sPng := MnBmpToPng(ABmp, clNone); //Transparent Color;
	sTemp := '<img src="data:image/png;base64,' + B64Encode(sPng) + '"/>';
	println sTemp;
	
  finally
    FreeAndNil(ABmp);
  end;
end;
?>

<br><br>
<div>I think you can create a captcha image by yourself. Maybe farther.</div>

</body>
</html>
