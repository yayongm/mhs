<html>
<title>Lesson4</title>
<body>
<?
uses
  SysUtils, SuperObject;

function GetPersonInfo(AName: string; ABirthday: TDateTime): string;
var
  ASO: ISuperObject;
  dDate: TDateTime;
begin
  ASO := SO('{}');
  try
    ASO.S['Name'] := AName;
    ASO.D['ABirthday'] := ABirthday;
    dDate := Now() - ABirthday;
    ASO.I['Age'] := Trunc(dDate / 365);
    ASO.S['生日'] := FormatDateTime('yyyy-mm-dd', ABirthday);
    Result := ASO.AsString;

    //Result := Result;
  finally
    ASO := nil;
  end;
end;
?>

<h2>Json Test</h2>
<div>Now you will get the random person information:<div>
<?
print GetPersonInfo('张三', Now - Random(10000));
?>

<br><br>
<div>Yes,Use pascal to code http server is very cool!<div>

</body>
</html>