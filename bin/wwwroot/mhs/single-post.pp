<?
uses
  SysUtils, Variants, Classes, DB,
  uView_Index, uView_single_post,  
  BaseUI in '..\public\BaseUI.pp',
  BaseQuery in '..\public\BaseQuery.pp',
  uView_Header;

function DoActions: Boolean;
var
  sAct: string;
begin
  Result := False;
  
  sAct := mRPV('act');    
  //判断是否是留言提交
  if sAct = 'postcomment' then
  begin
    if TSinlgePostView.PostComments then
    begin
      TBaseUI.Redirect(Format('single-post.pp?id=%s&catalog=%s', [mRPV('id'), mRPV('catalog')]));
      
      Result := True;
      abort;
    end;
  end;
end;

procedure DoGetContent;
var
  qryArticle: TWmQuery;
begin
  //从当前WebModule中查找命名Query,因为该Query的Owner为WebModule,故当前线程都可以用到,
  //请注意在使用时名字不要重复,否则容易内存泄漏
  qryArticle := TBaseQry.GetQuery('qryArticle');

  //查询文档并将标题赋值给页面标题
  if TSinlgePostView.GetContentById(qryArticle) > 0 then
  begin
    print wm.Include('header.tpl', Format('{title}=MHS | %s', [qryArticle.FS8['title']]) + sLineBreak + 
      Format('{keywords}=%s', [qryArticle.FS8['keywords']]));
  end    
  else
  begin
    print wm.Include('header.tpl', '{title}=MHS | 文档详情');
  end;  
end;

//先处理特殊动作
DoActions;
  
DoGetContent;

?>

</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<?
  THeaderView.GetHeaderText('blog-style-1');
?>
	
	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container sbr clearfix">
		
		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	
		
		<div class="page-header">
			
			<h1 class="page-title">
				<?  
					print TIndexView.GetCatalogName; //新闻详情
				?>
			</h1>
			
		</div><!--/ .page-header-->
		
		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	
		

		<!-- - - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->		
		
		<section id="content">
		
			<? TSinlgePostView.GetContentTextFromQuery(TBaseQry.GetQuery('qryArticle')); ?>
			
			<section id="comments">

            <? TSinlgePostView.GetComments; ?>
            
			</section>
			
			<section id="respond">

				<h5>留言</h5>

				<form method="post" action="<? print Format('?act=%s&id=%s&catalog=%s', ['postcomment', mRPV('id'), mRPV('catalog')]); ?>" id="commentform" />

					<fieldset class="input-block">
						<label for="comment-name"><strong>您的名字</strong> <span>*</span><i>(必填)</i></label>
						<input type="text" name="username" value="" id="comment-name" required />
					</fieldset>

					<fieldset class="input-block">
						<label for="comment-email"><strong>手机号</strong> <span>*</span><i>(必填)</i></label>
						<input type="text" name="usermobile" value="" id="comment-email" required />
					</fieldset>

					<fieldset class="input-block">
						<label for="comment-url"><strong>E-mail</strong></label>
						<input type="text" name="usermail" value="" id="comment-url" />
					</fieldset>

					<fieldset class="input-block">
						<label for="comment-position"><strong>您的职位</label>
						<input type="text" name="position" value="" id="comment-position" />
					</fieldset>

					<fieldset class="textarea-block">
						<label for="comment-message"><strong>留言</strong></label>
						<textarea name="commenttext" id="comment-message" cols="50" rows="4" required></textarea>
					</fieldset>
					
					<input type="submit" class="button gray" value="提交留言 &rarr;" />

					<div class="clear"></div>

				</form>

			</section><!--/ #respond-->

			
		</section><!--/ #content-->
		
		<!-- - - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - - - - - - - Sidebar - - - - - - - - - - - - - - - - -->	
		
		<aside id="sidebar">
			

			<? 
				TIndexView.GetSearchWidget;
				TIndexView.GetUpcomingEvents;
				TIndexView.GetCatalog; 
				TIndexView.GetVideoNews;
				TIndexView.GetHotNews;
				TIndexView.GetTestimonials;
			?>

			
		</aside><!--/ #sidebar-->
		
		<!-- - - - - - - - - - - - - end Sidebar - - - - - - - - - - - - - - - - -->
		
        <!--
		<ul class="pagination">
			
			<li><a class="prevpostslink" href="#">&larr; Previous</a></li>
			<li><a href="#">1</a></li>
			<li><a class="current" href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a class="nextpostslink" href="#">Next &rarr;</a></li>
			
		</ul>--><!--/ .pagination-->
		
		
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
	
<? print wm.Include('footer.tpl'); ?>
</body>
</html>
