	
	<!-- - - - - - - - - - - - - - - Footer - - - - - - - - - - - - - - - - -->	
	
	<footer id="footer" class="container clearfix">
		
		<div class="one-fourth">
			
			<div class="widget-container widget_text">
				
				<h3 class="widget-title">Moon Http Server</h3>
				
				<div class="textwidget">
					
					<p>
						Moon Http Server(MHS) 是一个使用Pascal脚本的高性能Web服务器.
					</p>	
					
				</div><!--/ .textwidget-->
				
			</div><!--/ .widget-container-->
			
		</div><!--/ .one-fourth-->
		
		<div class="one-fourth">
			
			<div class="widget-container widget_nav_menu">
				
				<h3 class="widget-title">网站地图</h3>

				<div class="menu-custom_menu-container">
					
					<ul class="menu" id="menu-custom_menu">

						<li><a href="./about.pp">关于我们</a></li>
						<li><a href="#">常见问题</a></li>
						<li><a href="./blog-style-1.pp">博客</a></li>
						<li><a target="_blank" href="../admin/login.pp">后台管理</a></li>
						<li><a target="_blank" href="../lessons/">演示</a></li>

					</ul><!--/ .menu-->		
				
				</div><!--/ .menu-custom_menu-container-->
				
			</div><!--/ .widget-container-->
			
		</div><!--/ .one-fourth-->
		
		<div class="one-fourth">
			
			<div class="widget-container widget_latest_tweets">
				
				<h3 class="widget-title">最新动态</h3>
			
				<div id="jstwitter"></div><!--/ #jstwitter-->
				
			</div><!--/ .widget-container-->
			
		</div><!--/ .one-fourth-->
		
		<div class="one-fourth last">
			
			<div class="widget-container widget_links">
			
				<h3 class="widget-title">友情链接</h3>

				<ul>

					<li><a target="_blank" href="http://www.2ccc.com">Delphi盒子</a></li>
					<li><a target="_blank" href="http://www.delphifans.com">Delphi园地</a></li>
					<li><a target="_blank" href="https://synopse.info">Synopse</a></li>
					<li><a target="_blank" href="https://forum.lazarus.freepascal.org/">FreePascal</a></li>

				</ul>				
				
			</div><!--/ .widget-container-->
			
		</div><!--/ .one-fourth .last-->
		
		<div class="clear"></div>
		
		<ul class="copyright">
			
            <li><a href="http://www.beian.miit.gov.cn/" target="blank">粤ICP备19060953号</a></li>
			<li>Copyright @ 2019</li>
			<li><a href="./">Powered by Moon Http Server</a></li>
			<li>All rights reserved</li>
			
		</ul><!--/ .copyright-->
	
	</footer><!--/ #footer-->
	
	<!-- - - - - - - - - - - - - - - end Footer - - - - - - - - - - - - - - - - -->		
	
</div><!--/ .wrap-->

<script type="text/javascript" src="js/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="sliders/flexslider/jquery.flexslider-min.js"></script>	
<!--[if lt IE 9]>
<script>window.jQuery || document.write('<script src="js/jquery-1.7.1.min.js"><\/script>')</script>
	<script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE8.js"></script>
	<script src="js/ie.js"></script>
<![endif]-->
<script type="text/javascript" src="js/custom.js"></script>
