<?
uses
  uView_Header;

print wm.Include('header.tpl', '{title}=MHS | 左侧边栏');
?>

</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<?
  THeaderView.GetHeaderText('elements');
?>
	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container sbl clearfix">
		
		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	
		
		<div class="page-header">
			
			<h1 class="page-title">About Us</h1>
			
		</div><!--/ .page-header-->
		
		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	
		

		<!-- - - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->		
		
		<section id="content" class="first">

			<!-- - - - - - - - - - - - Paragraph with images - - - - - - - - - - - -->	

			<h6 class="section-title">Curabitur pretium tincidunt</h6>

			<img src="images/blog/post-2.jpg" alt="" class="alignleft custom-frame" />

			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
				aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
				Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
				occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
				aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
				Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
				occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			
			<div class="sep"></div>
			
			<h6 class="section-title">Lorem ipsum dolor sit amet</h6>

			<img src="images/blog/post-4.jpg" alt="" class="alignright custom-frame" />

			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
				aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
				Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
				occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
				aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
				Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
				occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>

			<!-- - - - - - - - - - - end Paragraph with images - - - - - - - - - - - -->	

			<div class="sep"></div>
			
			<!-- - - - - - - - - - - - - -  Ordered Lists - - - - - - - - - - - - - - - -->

				<h6 class="section-title">Excepteur sint occaecat cupidatat</h6>

				<div class="full-width clearfix">

					<div class="one-fourth">

						<ol class="list type-1">
							<li>Lorem ipsum dolor sit amet sed do eiusmod</li>
							<li>Consectetur adipisicing sed do tempor</li>
							<li>Elit sed do eiusmod tempor magna et dolore</li>
							<li>Incididunt ut labore ipsum dolor sit amet</li>
							<li>Set magna et dolore magna magna et dolore</li>
							<li>Lorem ipsum dolor sit amet sed do eiusmod</li>
							<li>Consectetur adipisicing sed do tempor</li>
						</ol>

					</div><!--/ .one-fourth-->

					<div class="one-fourth last">

						<ul class="list type-1">
							<li>Lorem ipsum dolor sit amet magna et dolore</li>
							<li>Consectetur adipisicing sed do eiusmod temp</li>
							<li>Elit sed do eiusmod tempor labore magna et</li>
							<li>Incididunt ut labore magna et dolore magna</li>
							<li>Set magna et dolore magna sed do eiusmod.</li>
							<li>Consectetur adipisicing sed do eiusmod temp</li>
							<li>Elit sed do eiusmod tempor labore magna et</li>
						</ul>

					</div><!--/ .one-fourth .last-->

				</div><!--/ .full-width-->

			<!-- - - - - - - - - - - - end Ordered Lists - - - - - - - - - - - - - -->	
		
			
		</section><!--/ #content-->
		
		<!-- - - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - - - - - - - Sidebar - - - - - - - - - - - - - - - - -->	
		
		<aside id="sidebar">
			
			<div class="widget-container widget_search">

				<form action="#" id="searchform" method="get" role="search" />
					
					<fieldset>
						<input type="text" id="s" placeholder="Search" />
						<button type="submit" id="searchsubmit"></button>
					</fieldset>
					
				</form><!--/ #searchform-->
			
			</div><!--/ .widget-container-->
			
			<div class="widget-container eventsListWidget">

				<h3 class="widget-title">Upcoming Events</h3>

				<ul>
					
					<li>
						<a href="#"><h6>Suspendisse Potenti Consectetur</h6></a>
						<span class="widget-date">June 15, 2012</span>
					
					</li>
					<li>
						<a href="#"><h6>Mauris Vitae Adipiscing et Urna</h6></a>
						<span class="widget-date">June 14, 2012</span>
					</li>
					<li>
						<a href="#"><h6>Donec Blandit Luctus Diam</h6></a>
						<span class="widget-date">June 13, 2012</span>
					</li>
					
				</ul>
				
			</div><!--/ .widget-container-->
			
			<div class="widget-container widget_calendar">
				
					<h3 class="widget-title">Calendar</h3>
					
					<div id="calendar_wrap">
						
						<table id="wp-calendar">
							
							<caption>June 2012</caption>
							
							<thead>
								<tr>
									<th scope="col" title="Monday">M</th>
									<th scope="col" title="Tuesday">T</th>
									<th scope="col" title="Wednesday">W</th>
									<th scope="col" title="Thursday">T</th>
									<th scope="col" title="Friday">F</th>
									<th scope="col" title="Saturday">S</th>
									<th scope="col" title="Sunday">S</th>
								</tr>
							</thead>
 
							<tfoot>
								<tr>
									<td colspan="3" id="prev"><a href="#" title="">May</a></td>
									<td class="pad">&nbsp;</td>
									<td colspan="3" id="next" class="pad"><a href="#" title="">June</a></td>
								</tr>
							</tfoot>

							<tbody>
								<tr>
									<td colspan="4" class="pad">&nbsp;</td>
									<td>1</td>
									<td>2</td>
									<td>3</td>
								</tr>
								<tr>
									<td>4</td>
									<td><a href="#" title="">5</a></td>
									<td>6</td>
									<td>7</td>
									<td>8</td>
									<td id="today">9</td>
									<td>10</td>
								</tr>
								<tr>
									<td>11</td>
									<td>12</td>
									<td>13</td>
									<td>14</td>
									<td>15</td>
									<td>16</td>
									<td>17</td>
								</tr>
								<tr>
									<td>18</td>
									<td>19</td>
									<td>20</td>
									<td>21</td>
									<td>22</td>
									<td>23</td>
									<td>24</td>
								</tr>
								<tr>
									<td>25</td>
									<td>26</td>
									<td>27</td>
									<td>28</td>
									<td>29</td>
									<td>30</td>
									<td class="pad" colspan="1">&nbsp;</td>
								</tr>
							</tbody>
							
						</table><!--/ #wp-calendar-->
						
					</div><!--/ #calendar_wrap-->
				
			</div><!--/ .widget-container-->
			
			<div class="widget-container widget_popular_posts">
				
				<h3 class="widget-title">Popular Posts</h3>
				
				<ul>
					
					<li>
						<a href="#"><h6>Curabitur Posuere Nisl</h6></a>
						<span class="widget-date">June 15, 2012, 5 Comments</span>
					</li>
					<li>
						<a href="#"><h6>Vivamus Consequat Pellentesque</h6></a>
						<span class="widget-date">June 15, 2012, 5 Comments</span>
					</li>
					<li>
						<a href="#"><h6>Donec Blandit Luctus Diam</h6></a>
						<span class="widget-date">June 15, 2012, 5 Comments</span>
					</li>
					
				</ul>
				
			</div><!--/ .widget-container-->
			
		</aside><!--/ #sidebar-->
		
		<!-- - - - - - - - - - - - - end Sidebar - - - - - - - - - - - - - - - - -->
		
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
	
<? print wm.Include('footer.tpl'); ?>
</body>
</html>

