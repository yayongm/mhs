<?
uses
  uView_Header;

print wm.Include('header.tpl', '{title}=MHS | 排版');
?>

</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<?
  THeaderView.GetHeaderText('elements');
?>
	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container clearfix">
		
		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	
		
		<div class="page-header">
			
			<h1 class="page-title">排版</h1>
			
		</div><!--/ .page-header-->
		
		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - - - - - - - Headings - - - - - - - - - - - - - - - - -->		
		
			<h1>H1 Lorem ipsum dolor sit amet consectetu</h1>
			
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
				et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
				aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
				in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			
			<div class="sep"></div>
			
			<h2>H2 Lorem ipsum dolor sit amet consectetu</h2>
			
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
				et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
				aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
				in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			
			<div class="sep"></div>
			
			<h3>H3 Lorem ipsum dolor sit amet consectetu</h3>
			
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
				et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
				aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
				in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			
			<div class="sep"></div>
			
			<h4>H4 Lorem ipsum dolor sit amet consectetu</h4>
			
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
				et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
				aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
				in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			
			<div class="sep"></div>
			
			<h5>H5 Lorem ipsum dolor sit amet consectetu</h5>
			
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
				et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
				aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
				in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			
			<div class="sep"></div>
			
			<h6>H6 Lorem ipsum dolor sit amet consectetu</h6>
			
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore
				et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
				aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt
				in culpa qui officia deserunt mollit anim id est laborum.
			</p>
			
			<div class="sep"></div>
			
			
		<!-- - - - - - - - - - - - - - end Headings - - - - - - - - - - - - - - - -->
		
		<!-- - - - - - - - - - - - - -  Ordered Lists - - - - - - - - - - - - - - - -->
		
			<h6 class="section-title">Ordered & Unordered Lists</h6>
			
			
			<div class="full-width">
				
				<div class="one-fourth">

					<ol class="list type-1">
						<li>Lorem ipsum dolor sit amet</li>
						<li>Consectetur adipisicing </li>
						<li>Elit sed do eiusmod tempor </li>
						<li>Incididunt ut labore </li>
						<li>Set magna et dolore magna.</li>
					</ol>

				</div><!--/ .one-fourth-->

				<div class="one-fourth">

					<ol class="list type-2">
						<li>Lorem ipsum dolor sit amet</li>
						<li>Consectetur adipisicing </li>
						<li>Elit sed do eiusmod tempor </li>
						<li>Incididunt ut labore </li>
						<li>Set magna et dolore magna.</li>
					</ol>

				</div><!--/ .one-fourth-->		

				<div class="one-fourth">

					<ul class="list type-1">
						<li>Lorem ipsum dolor sit amet</li>
						<li>Consectetur adipisicing </li>
						<li>Elit sed do eiusmod tempor </li>
						<li>Incididunt ut labore </li>
						<li>Set magna et dolore magna.</li>
					</ul>

				</div><!--/ .one-fourth-->

				<div class="one-fourth last">

					<ul class="list type-2">
						<li>Lorem ipsum dolor sit amet</li>
						<li>Consectetur adipisicing </li>
						<li>Elit sed do eiusmod tempor </li>
						<li>Incididunt ut labore </li>
						<li>Set magna et dolore magna.</li>
					</ul>

				</div><!--/ .one-fourth .last-->
			
			</div><!--/ .full-width-->
		
		<!-- - - - - - - - - - - - end Ordered Lists - - - - - - - - - - - - - -->	
		
		<div class="sep"></div>
		
		<!-- - - - - - - - - - - - Paragraph with images - - - - - - - - - - - -->	
		
		<h6 class="section-title">Paragraph with images</h6>
		
		<img src="images/blog/post-2.jpg" alt="" class="alignleft custom-frame" />
		
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
			aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
			Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
			occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</p>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
			aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
			Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
			occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</p>
		<p>
			Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros
			bibendum elit, nec luctus magna felis sollicitudin mauris. Integer in mauris eu nibh euismod gravida. Duis ac 
			tellus et risus vulputate vehicula. Donec lobortis risus a elit. Etiam tempor. Ut ullamcorper, ligula eu tempor
			congue, eros est euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum consequat mi. Donec fermentum.
			Pellentesque malesuada nulla a mi. Duis sapien sem, aliquet nec, commodo eget, consequat quis, neque. Aliquam faucibus,
			elit ut dictum aliquet, felis nisl adipiscing sapien, sed malesuada diam lacus eget erat. Cras mollis scelerisque nunc.
			Nullam arcu. Aliquam consequat.
		</p>
		
		<img src="images/blog/post-2.jpg" alt="" class="alignright custom-frame" />
		
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
			aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
			Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
			occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</p>
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
			aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
			Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
			occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</p>
		<p>
			Curabitur pretium tincidunt lacus. Nulla gravida orci a odio. Nullam varius, turpis et commodo pharetra, est eros
			bibendum elit, nec luctus magna felis sollicitudin mauris. Integer in mauris eu nibh euismod gravida. Duis ac 
			tellus et risus vulputate vehicula. Donec lobortis risus a elit. Etiam tempor. Ut ullamcorper, ligula eu tempor
			congue, eros est euismod turpis, id tincidunt sapien risus a quam. Maecenas fermentum consequat mi. Donec fermentum.
			Pellentesque malesuada nulla a mi. Duis sapien sem, aliquet nec, commodo eget, consequat quis, neque. Aliquam faucibus,
			elit ut dictum aliquet, felis nisl adipiscing sapien, sed malesuada diam lacus eget erat. Cras mollis scelerisque nunc.
			Nullam arcu. Aliquam consequat.
		</p>
		
		<div class="aligncenter">
			<img src="images/blog/post-2.jpg" alt="" class="custom-frame" />	
		</div>
		
		<p>
			Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
			aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. 
			Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
			occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
		</p>
		
		<!-- - - - - - - - - - - end Paragraph with images - - - - - - - - - - - -->	
		
		<div class="sep"></div>
		
		<!-- - - - - - - - - - - - Highlights - - - - - - - - - - - - - -->	
		
		<h6 class="section-title">Highlights with Dropcaps</h6>
				
		<div class="one-half">
			
			<p class="highlight1">
				<span class="dropcapspot">1</span>
				In ac tellus sit amet arcu aliquam pretium. Aliquam sodales condi- mentum faucibus. Duis fringilla, augue eget tempus 
				iaculis, erat nisl consequat lacus, a <span class="highlight1">vestibulum nulla sapien</span>  nec nisl. Lorem
				ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore.
				Sunt in culpa qui officia deserunt mollit anim id est laborum elit.
			</p>
			
			<p class="highlight2">
				<span class="dropcapspot">2</span>
				Vestibulum <span class="highlight2">ante ipsum primis in</span> faucibus orci luctus et ultrices posuere cubilia Curae; Suspendisse 
				sollicitudin velit sed leo. Ut pharetra augue nec augue. Nam elit agna. Aenean nec eros.Excepteur sint 
				occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				 Orci luctus et ultrices posuere cubilia Curae pharetra augue nec.
			</p>	
		
		</div><!--/ .one-half-->

		<div class="one-half last">
			
			<p class="highlight3">
				<span class="dropcapspot">3</span>
				Aenean nec eros. Vestibulum <span class="highlight3">ante ipsum primis</span> in faucibus orci luctus et ultrices posuere cubilia Curae; 
				Suspendisse sollicitudin velit sed leo. Ut pharetra augue nec augue. Nam elit agna, endrerit sit amet, tincidunt ac. 
				 quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
				 Sintin culpa qui officia deserunt mollit anim id est nisi ut.
			</p>
			
			<p class="highlight4">
				<span class="dropcapspot">4</span>
				Quisque placerat sapien id erat pharetra ac mollis augue placerat. Vivamus et ligula 
				diam, <span class="highlight4"> sit amet eleifend justo.</span> Cum sociis natoque penatibus et magnis dis 
				parturient montes, nascetur ridiculus mus. Aenean nec eros. Excepteur sint occaecat cupidatat non proident, sunt 
				in culpa qui officia deserunt. Cum sociis natoque penatibus et magnis dis 
				parturient.
			</p>	
			
		</div><!--/ .one-half .last-->
		
		<!-- - - - - - - - - - - end Highlights - - - - - - - - - - - - -->	
		
		<div class="clear"></div>
		<div class="sep"></div>

		<!-- - - - - - - - - - - Table - - - - - - - - - - - -->	
		
		<h6 class="section-title">Table</h6>
		
		<table class="custom-table">
			<thead>
				<tr>
					<th>Header 1</th>
					<th>Header 2</th>
					<th>Header 3</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td>Et urna aliquam</td>
					<td>Donec sit amet eros</td>
					<td>Donec sit amet eros</td>
				</tr>
				<tr>
					<td>Et urna aliquam</td>
					<td>Donec sit amet eros</td>
					<td>Donec sit amet eros</td>
				</tr>
				<tr>
					<td>Erat volutpat</td>
					<td>Lorem ipsum dolor sit</td>
					<td>Lorem ipsum dolor sit</td>
				</tr>
				<tr>
					<td>Erat volutpat</td>
					<td>Lorem ipsum dolor sit</td>
					<td>Lorem ipsum dolor sit</td>
				</tr>
				<tr>
					<td>Duis ac turpis</td>
					<td>Amet consectetuer</td>
					<td>Amet consectetuer</td>
				</tr>
				<tr>
					<td>Duis ac turpis</td>
					<td>Amet consectetuer</td>
					<td>Amet consectetuer</td>
				</tr>
				<tr>
					<td>Duis ac turpis</td>
					<td>Amet consectetuer</td>
					<td>Amet consectetuer</td>
				</tr>
			</tbody>
		</table><!--/ .custom-table-->
				
		<!-- - - - - - - - - - end Table - - - - - - - - - - -->	
		
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
	
<? print wm.Include('footer.tpl'); ?>
</body>
</html>

