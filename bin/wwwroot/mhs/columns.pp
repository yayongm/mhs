<?
uses
  uView_Header;

print wm.Include('header.tpl', '{title}=MHS | 栏目');
?>

</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<?
  THeaderView.GetHeaderText('elements');
?>
	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container clearfix">
		
		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	
		
		<div class="page-header">
			
			<h1 class="page-title">Columns</h1>
			
		</div><!--/ .page-header-->
		
		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - - - - - - - Columns - - - - - - - - - - - - - - - - -->		
	
			<h4>Full width</h4>
			
			<span class="dropcap">1</span>
			<p>
				Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
				dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
				ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
				fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt
				mollit anim id est laborum. nt ut labore et dolore magna aliqua. Ut enim ad minim. Lorem ipsum dolor sit amet, 
				consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim 
				ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis 
				aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur 
				sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
			</p>

			<div class="sep"></div>

			<div class="two-third">
				
				<h4>Two thirds of the column</h4>
				<span class="dropcap">1</span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
					dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
					ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
					eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod 
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
					ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
					voluptate velit esse cillum dolore eu fugiat.
				</p>
				
			</div><!--/ .two-thirds-->
			
			<div class="one-third last">
				
				<h4>One third column</h4>
				<span class="dropcap">2</span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing 
					elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Nullam vitae tortor in felis congue
					interdum. Praesent lobortis, felis vitae commodo facilisis, tortor nibh. 
				</p>
				
			</div><!--/ .one-third .last-->

			<div class="sep"></div>

			<div class="one-third">
				
				<h4>One third column</h4>
				<span class="dropcap">1</span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing 
					elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.  Nullam vitae tortor in felis congue
					interdum. Praesent lobortis, felis vitae commodo facilisis, tortor nibh. 
				</p>
				
			</div><!--/ .one-third-->
			
			<div class="two-third last">
				
				<h4>Two thirds of the column</h4>
				<span class="dropcap">2</span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
					dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
					ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore
					eu fugiat nulla pariatur. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod 
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation
					ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in 
					voluptate velit esse cillum.
				</p>
				
			</div><!--/ two_thirds_last-->

			<div class="sep"></div>

			<div class="one-fourth">
				
				<h4>One fourth column</h4>
				<span class="dropcap">1</span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing elit, 
					sed do eiusmod tempor incididunt ut labore et tempor dolore magna aliqua pariatur. 
				</p>
				
			</div><!--/ .one-fourth-->

			<div class="one-fourth">
				
				<h4>One fourth column</h4>
				<span class="dropcap">2</span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing elit, 
					sed do eiusmod tempor incididunt ut labore et dolore magna aliqua pariatur. 
				</p>
				
			</div><!--/ .one-fourth-->

			<div class="one-fourth">
				
				<h4>One fourth column</h4>
				<span class="dropcap">3</span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing elit, 
					sed do eiusmod tempor incididunt ut labore et dolore magna aliqua pariatur. 
				</p>
				
			</div><!--/ .one-fourth-->

			<div class="one-fourth last">
				
				<h4>One fourth column</h4>
				<span class="dropcap">4</span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing elit, 
					sed do eiusmod tempor incididunt ut labore et dolore magna aliqua pariatur. 
				</p>
				
			</div><!--/ .one-fourth-->
			
			<div class="sep"></div>
			
			<div class="one-fourth">
				
				<h4>One fourth column</h4>
				<span class="dropcap">1</span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing elit, 
					sed do eiusmod tempor incididunt ut labore et tempor dolore tempor magna adipisicing aliqua. 
				</p>
				
			</div><!--/ .one-fourth-->

			<div class="one-fourth">
				
				<h4>One fourth column</h4>
				<span class="dropcap">2</span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor consectetur adipisicing elit, 
					sed do eiusmod tempor incididunt ut labore et dolore magna adipisicing aliqua. 
				</p>
				
			</div><!--/ .one-fourth-->
			
			<div class="one-half last">
				
				<h4>One half column</h4>
				<span class="dropcap">3</span>
				<p>
					Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor Lorem ipsum dolor sit amet, 
					consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad 
					dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna 
					aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
					consequat.
				</p>
				
			</div><!--/ .one-half .last-->
			
			<div class="sep"></div>
			
		<!-- - - - - - - - - - - - - - end Columns - - - - - - - - - - - - - - - -->
		
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
	
<? print wm.Include('footer.tpl'); ?>
</body>
</html>

