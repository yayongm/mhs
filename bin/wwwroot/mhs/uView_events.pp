<?
unit uView_events;

interface

uses
  SysUtils, Variants, Classes, DB,
  BaseUI in '..\public\BaseUI.pp',
  BaseQuery in '..\public\BaseQuery.pp',
  uMhsBase;
  
type
  TEventsView = class(TMhsBaseView)
  protected
    FCatalog: Integer;
  public
    function GetContentText(const AContentPageUrl: string): Integer;
    
    property Catalog: Integer read FCatalog write FCatalog;
  end;  

const
  C_DEF_EVENTS_VIEW_NAME = 'DefEventsView';

function NewCurentView(AName: string = ''): TEventsView;
function CurrentView(AName: string = ''): TEventsView;

implementation

function NewCurentView(AName: string): TEventsView;
begin
  Result := TEventsView.Create(AName, '');
  if Length(AName) > 0 then
    Result.Name := AName
  else  
    Result.Name := C_DEF_EVENTS_VIEW_NAME;
end;

function CurrentView(AName: string): TEventsView;
begin
  if Length(AName) > 0 then
    Result := TEventsView(wm.Objects(AName))
  else
    Result := TEventsView(wm.Objects(C_DEF_EVENTS_VIEW_NAME));
end;

function TEventsView.GetContentText(const AContentPageUrl: string): Integer;
var
  iID, iPage, iPageCount: Integer;
  sImg: string;
  sKey, sTemp, sSQL: string;
begin
  Result := -1;
  
  iPageCount := 10;
  iID := StrToIntDef(mRQV('id'), -1);
  iPage := StrToIntDef(mRQV('page'), 1) - 1;
  sKey := Utf8Decode(UrlDecode(mRQV('searchkey')));

  if Length(sKey) > 0 then
  begin
    sTemp := '%' + sKey + '%';

    if TBaseQry.DBQuerySel(qryMain, 'Count(*)', 'article', '(catalog=' + IntToStr(FCatalog) + ') and (type < 3) and ((title like :title) or (keywords like :keywords)) order by type, createtime desc', 
      VarArrayOf([sTemp, sTemp])) < 0 then
      Exit;
    Result := qryMain.Fields[0].AsInteger;  

    if TBaseQry.DBQuerySel(qryMain, '*', 'article', '(catalog=' + IntToStr(FCatalog) + ') and (type < 3) and ((title like :title) or (keywords like :keywords)) order by type, createtime desc limit :Start, :End', 
      VarArrayOf([sTemp, sTemp, iPage * iPageCount, iPageCount])) < 0 then
      Exit;
  end  
  else
  begin  
    if TBaseQry.DBQuerySel(qryMain, 'Count(*)', 'article', '(catalog=' + IntToStr(FCatalog) + ') and (type < 3) order by type, createtime desc', 
      VarArrayOf([])) < 0 then
      Exit;
    Result := qryMain.Fields[0].AsInteger;  

    if TBaseQry.DBQuerySel(qryMain, '*', 'article', '(catalog=' + IntToStr(FCatalog) + ') and (type < 3) order by type, createtime desc limit :Start, :End', 
      VarArrayOf([iPage * iPageCount, iPageCount])) < 0 then
      Exit;
  end;
  
  Result := qryMain.RecordCount;  
  if Result < 1 then
	Exit;

  qryMain.DataSet.First;
  while not qryMain.DataSet.Eof do
  begin
	sTemp := AContentPageUrl + 'id=' + qryMain.FS8['id'] + '&catalog=' + qryMain.FS8['catalog'];
?>  

				<article class="event-item clearfix">
					
					<h6 class="event-date"><? print FormatDateTime('yyyy-mm-dd', qryMain.FD['createtime']); ?></h6>
					<a href="<? print sTemp; ?>"><h3 class="title"><? print qryMain.FS8['title']; ?></h3></a>
					
                    <? print qryMain.FS8['summary']; ?>
				</article><!--/ .event-item-->
				
<?  
	qryMain.DataSet.Next;
  end;  
end;
?>




<?


end.
?>