<?
unit uView_single_events;

interface

uses
  SysUtils, Variants, Classes, DB,
  BaseUI in '..\public\BaseUI.pp',
  BaseQuery in '..\public\BaseQuery.pp',
  uMhsBase;
  
type
  {
  //注意:已变更为从TMhsBaseView继承,其它内容直接复制自SinglePost,实际是直接复制了SinglePost然后来改
  //该单元实现了与SinglePost完全不同的方式,请特别注意.
  //可以理解为SinglePost完全是函数式编程,而本单元更倾向于面向对象.
  //缺点:更多的依赖单元意味着编译脚本更耗时间,也意味着会占用更多一些的内存
  //优点:带来更强劲的功能也能构造更庞大的系统,必走之路,无可回避.具体选择,请自行斟酌.
  }
  TSinlgeEventsView = class(TMhsBaseView)
  public
    function GetContentById: Integer;
    procedure GetContentTextFromQuery;
    
    //下面这两个方法是从SinglePost来的,请自行更改,或者选择无视
    class procedure GetComments;
    class function PostComments: Boolean;
  end;  

const
  C_DEF_SINGLE_EVENTS_VIEW_NAME = 'DefSingleEventsView';

//如果需要更多的View来合作,这个函数名字就有问题了,当然,这里只是做个演示,实际使用中还是需要详细规划的.
function NewCurentView(AName: string = ''): TSinlgeEventsView;
function CurrentView(AName: string = ''): TSinlgeEventsView;

implementation

function NewCurentView(AName: string): TSinlgeEventsView;
begin
  Result := TSinlgeEventsView.Create(AName, '');
  if Length(AName) > 0 then
    Result.Name := AName
  else  
    Result.Name := C_DEF_SINGLE_EVENTS_VIEW_NAME;
end;

function CurrentView(AName: string): TSinlgeEventsView;
begin
  if Length(AName) > 0 then
    Result := TSinlgeEventsView(wm.Objects(AName))
  else
    Result := TSinlgeEventsView(wm.Objects(C_DEF_SINGLE_EVENTS_VIEW_NAME));
end;

function TSinlgeEventsView.GetContentById: Integer;
var
  iID: Integer;
  sSQL: string;
begin
  iID := StrToIntDef(mRQV('id'), -1);
  
  if iID < 1 then
  begin
    Result := -3;
  
    Exit;
  end;
  
  Result := TBaseQry.DBQuerySel(qryMain, '*', 'article', '(id=:id) and (type < 3)', VarArrayOf([iID]));
  //writeln(Result);
end;

procedure TSinlgeEventsView.GetContentTextFromQuery;
var
  iID: Integer;
  sSQL: string;
begin
  if not (Assigned(qryMain) and (qryMain.Active) and (qryMain.RecordCount = 1)) then
  begin
    print '主题没找到';
  
    Exit;
  end;
  
  TBaseQry.DbExecUpd('article', 'viewcount = viewcount + 1', '(id=:id)', VarArrayOf([qryMain.FI['id']]));
  
  print qryMain.FS8['summary'];
  
end;

class procedure TSinlgeEventsView.GetComments;
var
  AQuery: TWmQuery;
  iID, iCount: Integer;
  sSQL: string;
begin
  iID := StrToIntDef(mRPV('id'), -1);
  
  if iID < 1 then
  begin
    iCount := 0;
  end
  else
  begin
    if TBaseQry.DBQuerySel(AQuery, '*', 'usercomment', '(newsid=:newsid) order by id', VarArrayOf([iID])) < 0 then
      Exit;
      
    iCount := AQuery.RecordCount;
  end;
  print '				<h5>' + IntToStr(iCount) + ' Comments</h5>';
  print '				<ol class="comments-list">';

  if iCount > 0 then
  begin
  
    AQuery.DataSet.First;
    while not AQuery.DataSet.Eof do
    begin
?>
					<li class="comment">

						<article>
							<img src="images/gravatar.png" alt="avatar" class="avatar" />
							<div class="comment-entry">
								<div class="comment-meta">
									<h6 class="author"><a href="#">
                                      <? 
                                        print AQuery.FS8['username'];  
                                        if Length(AQuery.FS8['position']) > 0 then
                                          print ' - ' + AQuery.FS8['position'];
                                      ?>
                                      </a></h6>
									<p class="date"><? print DbDateToStr(AQuery.FD['createtime']); ?></p>
								</div><!--/ .comment-meta -->
								<div class="comment-body">
									<p>
										<? print AQuery.FS8['commenttext']; ?>
									</p>
								</div><!--/ .comment-body -->
							</div><!--/ .comment-entry-->

						</article>
                        
                    </li><!--/ .comment-->
<?  
      AQuery.DataSet.Next;
    end;
  end;
  
  print '				</ol><!--/ .comments-list-->';
  
end;

class function TSinlgeEventsView.PostComments: Boolean;
var
  AQuery: TWmQuery;
  iID, iCount: Integer;
  sSQL: string;
  sUserName, sUserMobile, sUserMail, sCommentText: string;
begin
  Result := False;
  
  iID := StrToIntDef(mRPV('id'), -1);
  
  if iID < 1 then
    Exit;
    
  sUserName := Trim(mRPV('username'));
  sUserMobile := Trim(mRPV('usermobile'));
  sCommentText := Copy(Trim(mRPV('commenttext')), 1, 2048);//限制长度
  sUserMail := Trim(mRPV('usermail'));

  if (sUserName = '') or (sUserMobile = '') or (sCommentText = '') then
    Exit;
    
  if TBaseQry.DBQuery(AQuery, 'select * from usercomment where (id=-1)', Null) < 0 then
    Exit;

  AQuery.Append;
  AQuery.FS['commenttext'] := StringReplaceAll(sCommentText, sLineBreak, '<br>');
  TBaseQry.FillRecord(AQuery, ['username', 'usermobile', 'usermail', 'position'], [sUserName, sUserMobile, sUserMail, mRPV('position')]);
  TBaseQry.FillRecord(AQuery, ['newsid', 'parentid', 'likecount', 'hatecount', 'createtime'], [iID, 0, 0, 0, Now]);
  AQuery.Post;
  
  Result := True;
end;


end.
?>