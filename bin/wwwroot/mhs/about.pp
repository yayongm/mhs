<?
uses
  SysUtils, Variants, Classes, DB,
  uView_Index,   
  uView_Header;

print wm.Include('header.tpl', '{title}=MHS | 关于我们');
?>

</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<?
  THeaderView.GetHeaderText('about');
?>
	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container sbr clearfix">
		
		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	
		
		<div class="page-header">
			
			<h1 class="page-title">关于我们</h1>
			
		</div><!--/ .page-header-->
		
		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	
		

		<!-- - - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->		
		
		<section id="content" class="first">

			<!-- - - - - - - - - - - - Paragraph with images - - - - - - - - - - - -->	

			<h6 class="section-title">团队介绍</h6>

			<img src="images/blog/post-2.jpg" alt="" class="alignleft custom-frame" />

			<p>
				首先要声明：左边这群帅哥美女不是我们团队的，其次要声明：这些图片、素材都是网上直接拿来的，具体说有没有版权，你懂的。
				所以为了安全起见，必须声明：这些内容都是为了学习之目的进行的，如有异议，请立即删除或者依照相关法律法规在24小时之内删除，谢谢。
			</p>
			<p>
				说起来团队，嗯，团队是啥？好吃吗？好吧，也许未来会有，也许要等到奥特曼来的那一天……
				让我们共同见证奇迹吧！
			</p>
			<p>
				
			</p>
			
			<div class="sep"></div>
			
			<h6 class="section-title">版权声明</h6>

			<img src="images/blog/post-4.jpg" alt="" class="alignright custom-frame" />

			<p>
				好像都流行这个，否则显得不专业，那咱就抄一个吧：
			</p>
			<p>
				1.版权归作者所有，无需作者同意您可以在保证打包内容完整的情况下任意发布，但不得对其中的二进制代码进行反编译操作。
			</p>
			<p>
				2.您可以任意应用于个人学习、个人网站、教育科研等有利于社会进步的方方面面且无需获取作者授权。
			</p>
			<p>
				3.如需商业使用，请知悉作者以获取商业授权。（备注：此条很纠结，本想写同2一样的内容，但还是保留吧，毕竟商人和技术控是两个不同的种类）
			</p>
			<p>
				4.作者保证您所使用的所有内容即为MoonHttpServer的全部功能，除需关心最新版本更新外，不存在技术保留。
			</p>
			<p>
				5.如需帮助，请使用QQ群。因工作紧张原因，如果不能及时回答您的问题，也请尽量海涵。
			</p>
			<p>
				6.根据实际需求，其它未尽事宜，请联系作者进行确认。
			</p>
			<p>
				7.作者对本版权声明保留变更的权利。
			</p>
			<p>
			</p>

			<div class="sep"></div>
			
			<h6 class="section-title">联系方式</h6>

			<p>
				QQ Group：67861996
			</p>
			<p>
				Email：yayongm@aliyun.com
			</p>

			<!-- - - - - - - - - - - end Paragraph with images - - - - - - - - - - - -->	

			<div class="sep"></div>
			
			<!-- - - - - - - - - - - - - -  Ordered Lists - - - - - - - - - - - - - - - -->

				<h6 class="section-title">友情赞助</h6>

				<div class="full-width clearfix">

					<div class="one-fourth">
						<!-- //先注释,以后有需求再用
						<ol class="list type-1">
							<li>Lorem ipsum dolor sit amet sed do eiusmod</li>
							<li>Consectetur adipisicing sed do tempor</li>
							<li>Elit sed do eiusmod tempor magna et dolore</li>
							<li>Incididunt ut labore ipsum dolor sit amet</li>
							<li>Set magna et dolore magna magna et dolore</li>
							<li>Lorem ipsum dolor sit amet sed do eiusmod</li>
							<li>Consectetur adipisicing sed do tempor</li>
						</ol>
						-->
					</div><!--/ .one-fourth-->

					<div class="one-fourth last">
						<!--
						<ul class="list type-1">
							<li>Lorem ipsum dolor sit amet magna et dolore</li>
							<li>Consectetur adipisicing sed do eiusmod temp</li>
							<li>Elit sed do eiusmod tempor labore magna et</li>
							<li>Incididunt ut labore magna et dolore magna</li>
							<li>Set magna et dolore magna sed do eiusmod.</li>
							<li>Consectetur adipisicing sed do eiusmod temp</li>
							<li>Elit sed do eiusmod tempor labore magna et</li>
						</ul>
						-->
					</div><!--/ .one-fourth .last-->

				</div><!--/ .full-width-->

			<!-- - - - - - - - - - - - end Ordered Lists - - - - - - - - - - - - - -->	
		
			
		</section><!--/ #content-->
		
		<!-- - - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - - - - - - - Sidebar - - - - - - - - - - - - - - - - -->	
		
		<aside id="sidebar">
			
			<? 
				TIndexView.GetSearchWidget;
				TIndexView.GetUpcomingEvents;
				TIndexView.GetCalendarWidget;
				TIndexView.GetHotNews;
			?>
			
		</aside><!--/ #sidebar-->
		
		<!-- - - - - - - - - - - - - end Sidebar - - - - - - - - - - - - - - - - -->
		
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
	
<? print wm.Include('footer.tpl'); ?>
</body>
</html>

