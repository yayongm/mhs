<?
uses
  SysUtils, Variants, Classes, DB,
  uView_Index,  
  uView_Header;

var
  iArticleCount: Integer;
  
begin
print wm.Include('header.tpl', '{title}=MHS | 首页');

?>

</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<? THeaderView.GetHeaderText('index'); ?>
	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container sbr clearfix">
	
		<!-- - - - - - - - - - - Slider - - - - - - - - - - - - - -->	

		<div id="slider" class="flexslider clearfix">

			<ul class="slides">

			    <? 
                  print TIndexView.GetSlidesText; 
                ?>

			</ul><!--/ .slides-->

		</div><!--/ #slider-->

		<!-- - - - - - - - - - - end Slider - - - - - - - - - - - - - -->
		
		<!--
		<ul class="block-with-icons clearfix">
			<li class="b1">
				<a href="#">
					<h5>活动</h5>
					<span>最近举办的活动.</span>
				</a>
			</li>
			<li class="b2">
				<a href="#">
					<h5>访问</h5>
					<span>国际性新闻.</span>
				</a>
			</li>
			<li class="b3">
				<a href="#">
					<h5>日程</h5>
					<span>即将发生的新闻.</span>
				</a>
			</li>
		</ul>--><!--/ .block-with-icons-->
		
		
		
		<!-- - - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->		
		
		<section id="content">
			
			<? 
            iArticleCount := TIndexView.GetContentText('single-post.pp?'); 
            //输出分页信息
            println TIndexView.GetPagination(iArticleCount, 5, 'index.pp?page=', 'class="pagination"');
            //println TIndexView.GetPaginationBySQL('select count(*) from article where (catalog=1) and (type < 3)', 5, 'index.pp?page=', 'class="pagination"');
            ?>
		</section><!--/ #content-->
		
		<!-- - - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - - -->	
		
		<!-- - - - - - - - - - - - - - - Sidebar - - - - - - - - - - - - - - - - -->	
		
		<aside id="sidebar">
			
			<?
                TIndexView.GetSearchWidget('index.pp');
				TIndexView.GetUpcomingEvents;
				TIndexView.GetVideoNews;
				TIndexView.GetCatalog;
				TIndexView.GetFacebookNews;
				TIndexView.GetHotNews;
			?>
			
			
		</aside><!--/ #sidebar-->
		
		<!-- - - - - - - - - - - - - end Sidebar - - - - - - - - - - - - - - - - -->
		
		
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
<? print wm.Include('footer.tpl'); ?>
</body>
</html>

<?
end.
?>
