unit uMhsBase;

interface

uses
  SysUtils, Variants, Classes, DB,
  BaseUI in '..\public\BaseUI.pp',
  BaseQuery in '..\public\BaseQuery.pp';
  
type
  //注意:为了让wm(WebModule)管理对象,对象必须从TComponent继承,注意区别
  TMhsBaseView = class(TComponent)
  public
    qryMain: TWmQuery;
    qryTemp: TWmQuery;
  public
    //注意此处自定义的Create函数跟默认的不一样
    constructor Create(AName: string; ADbName: string); overload;
    destructor Destroy; override;
  end;

implementation

constructor TMhsBaseView.Create(AName: string; ADbName: string);
begin
  //默认指定Owner
  inherited Create(wm);
  
  Self.Name := AName;

  qryMain := wm.NewQuery(ADbName);
  qryTemp := wm.NewQuery(ADbName);
end;

destructor TMhsBaseView.Destroy;
begin
  //因为qryMain的Owner为wm,所以此处不用释放qryMain,释放反而出错.
  //也就是说,TWmQuery类都由wm(WebModule)进行内存管理,除了函数中可选手动释放外,其它情况可以不管.
    
  inherited;
end;

end.
