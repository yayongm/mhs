<?
uses
  SysUtils, Variants, Classes, DB,
  uView_Index,   
  uView_Header;

print wm.Include('header.tpl', '{title}=MHS | 联系我们');
?>

</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<?
  THeaderView.GetHeaderText('contacts');
?>

	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container clearfix">
		
		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	
		
		<div class="page-header">
			
			<h1 class="page-title">Contact</h1>
			
		</div><!--/ .page-header-->
		
		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - Map - - - - - - - - -->	
		
		<div id="map"></div>
	
		<!-- - - - - - - - end Map - - - - - - - -->
		
		
		<div class="one-third">
			
			<h3>Main Office</h3>
			
			<address>
				QQ Group:         67861996<br />
				Email:         yayongm@aliyun.com<br />
				Address:     <br />
				Phone:        <br />
				FAX:             <br />
			</address>
			
		</div><!--/ .one-third-->
		
		<div class="two-third last">
			
			<div id="contact">
			
				<h3>Send an Email</h3>
				
				<form action="" class="contact-form" id="contactform" method="post" />

					<p class="input-block">
						<label for="name">Your Name: <span class="required">*</span><i>(required)</i></label>
						<input type="text" name="name" id="name" />
					</p>

					<p class="input-block">
						<label for="email">E-mail: <span class="required">*</span><i>(required)</i></label>
						<input type="text" name="email" id="email" />
					</p>

					<p class="input-block">
						<label for="web">Website</label>
						<input type="text" name="web" id="web" />
					</p>

					<p class="textarea-block">
						<label for="message">Message:</label>
						<textarea name="comments" id="message"></textarea>	
					</p>
					
					<p class="input-block">
						<label class="enter-verify" for="verify">Enter Capcha: <span class="required">*</span><i>(required)</i></label>
						<input class="verify" type="text" name="verify" />
						<iframe src="php/capcha_page.php" height="30" scrolling="no" frameborder="0" marginheight="0" marginwidth="0" class="capcha_image_frame" name="capcha_image_frame"></iframe>
					</p>						
					
					<p class="row">
						<button type="submit" class="button gray" id="submit">Send</button>
					</p>
							
				</form>	<!--/ #contactform-->
				
			</div><!--/ contact-->
			
		</div><!--/ .two-third .last-->
	
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
<!--	
<script src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script src="js/jquery.gmap.min.js"></script>
-->

<? print wm.Include('footer.tpl'); ?>

</body>
</html>

