<?
uses
  SysUtils, Variants, Classes, DB,
  uView_Index, uView_events,  
  uView_Header;

{
//说明:
//因数据库原因,主要是玩不转ckeditor,所以,把展示的内容直接弄到summary字段里了.本应分开弄到content字段里的.
//所以,请不要对此演示做不必要的联想.能玩得转的也请玩好后更新到群里一起来嗨吧.
}  

procedure DoAction;
var
  AEV: TEventsView;
begin
  AEV := NewCurentView;
  AEV.Catalog := 4;
end;

DoAction;

print wm.Include('header.tpl',  '{title}=MHS | 案例展示');
?>

</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<?
  THeaderView.GetHeaderText('events');
 
?>
	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container sbr clearfix">
		
		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	
		
		<div class="page-header">
			
			<h1 class="page-title">案例展示</h1>
			
		</div><!--/ .page-header-->
		
		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	
		

		<!-- - - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->		
		
		<section id="content">
		
			<section class="content-event">
				
                <?   CurrentView.GetContentText('single-events.pp?'); ?>
				
			</section><!--/ content-event-->
			
            <?
            //输出分页信息
            println TIndexView.GetPagination(CurrentView.qryMain.RecordCount, 5, 'events.pp?page=', 'class="pagination"');
            ?>
			
		</section><!--/ #content-->
		
		<!-- - - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - - - - - - - Sidebar - - - - - - - - - - - - - - - - -->	
		
		<aside id="sidebar">
			
			<? 
				TIndexView.GetSearchWidget;
				TIndexView.GetUpcomingEvents;
				TIndexView.GetCatalog; 
				TIndexView.GetVideoNews;
				TIndexView.GetHotNews;
			?>
			
		</aside><!--/ #sidebar-->
		
		<!-- - - - - - - - - - - - - end Sidebar - - - - - - - - - - - - - - - - -->
		
		
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
	
<? print wm.Include('footer.tpl'); ?>
</body>
</html>

