<?
uses
  SysUtils, Variants, Classes, DB,
  uView_Index,   
  uView_Header;

print wm.Include('header.tpl', '{title}=MHS | 元素');
?>


</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<?
  THeaderView.GetHeaderText('elements');
?>

	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container clearfix">
		
		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	
		
		<div class="page-header">
			
			<h1 class="page-title">Elements</h1>
			
		</div><!--/ .page-header-->
		
		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - - - - - - - Elements - - - - - - - - - - - - - - - - -->		
		
		<div class="one-half">
			
			
			<h6 class="widget-title">Tabs</h6>

			<div class="content-tabs">

				<ul class="tabs-nav">

					<li><a href="#tab1">Tab 1</a></li>
					<li><a href="#tab2">Tab 2</a></li>
					<li><a href="#tab3">Tab 3</a></li>

				</ul><!--/ .tabs-nav -->

				<div class="tabs-container">

					<div class="tab-content" id="tab1">
						Donec in velit vel ipsum auctor pulvinar. Vestibulum iaculis lacinia est. Proin dictum elementum velit. 
						Fusce euismod consequat ante. Lorem ipsum dolor sit amet, consectetuer adipisMauris accumsan nulla vel diam. 
						Sed in lacus ut enim adipiscing aliquet. Nulla venenatis. 
					</div><!--/ end #tab1 -->

					<div class="tab-content" id="tab2">
						Ut scelerisque lacus in sem malesuada porttitor. Maecenas ut enim eu elit dignissim mattis. Duis ornare 
						velit et metus pulvinar et venena tis dolor rutrum. Nulla iaculis dapibus rutrum. Etiam id sapien blandit 
						mauris pretium condimentum. Lorem ipsum dolor sit amet.
					</div><!--/ end #tab2 -->

					<div class="tab-content" id="tab3">
						Duis rutrum neque ac lacus ultricies eget faucibus metus elementum. Sed vitae justo nisi. Donec rhoncus 
						egestas risus et mattis. Vivamus pharetra interdum lectus, sit amet fringilla ligula luctus sit amet. 
						Mauris tincidunt nisi et velit tincidunt non mollis arcu tristique.
					</div><!--/ #tab3 -->

				</div><!-- .tabs-container -->	

			</div><!--/ .content-tabs-->
			
			<div class="divider"></div>

			<h6 class="widget-title">Blockquotes</h6>

			<blockquote>
				Suspendisse potenti. Praesent sit amet rhoncus nisi. Etiam tristique elis ultrices pulvinar condimentum  
				consectetur non, tincidunt malesuada lorem. Phasellus imperdiet risus augue fermentum pharetra rabitur 
				magna lacus, viverra eu laoreet sed, aliquet mauris a varius dui. Nullam quis aliquet velit. 
			</blockquote>	
			
			<div class="divider"></div>
			
			<h6 class="widget-title">Alert Boxes</h6>

			<div class="one-fourth"><p class="error">Alert Text</p></div>
			<div class="one-fourth last"><p class="success">Alert Text</p></div>
			<div class="one-fourth"><p class="info">Alert Text</p></div>
			<div class="one-fourth last"><p class="notice">Alert Text</p></div>
				
		</div><!--/ .one-half-->
		
		<div class="one-half last">
			
			<h6 class="widget-title">Toggles</h6>

			<div class="box-toggle">
				<span class="trigger">Toggle Title 1</span>
				<div class="toggle-container">
					Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. 
					Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus.
					Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque. Vivamus eget nibh. 
					Etiam cursus leo vel metus.
				</div><!--/ .toggle-container-->                                
			</div><!--/ .box-toggle-->

			<div class="box-toggle">
				<span class="trigger">Toggle Title 2</span>
				<div class="toggle-container">
					Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. 
					Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus.
					Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque. Vivamus eget nibh. 
					Etiam cursus leo vel metus.
				</div><!--/ .toggle-container-->                                
			</div><!--/ .box-toggle-->

			<div class="box-toggle">
				<span class="trigger">Toggle Title 3</span>
				<div class="toggle-container">
					Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. 
					Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus.
					Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque. Vivamus eget nibh. 
					Etiam cursus leo vel metus.
				</div><!--/ .toggle-container-->                                
			</div><!--/ .box-toggle-->
			
			<div class="divider"></div>
				
			<h6 class="widget-title">Accordion</h6>

			<span class="acc-trigger">
				<a href="#">Etiam cursus leo vel metus</a>
			</span>

			<div class="acc-container">
				<div class="content">
					Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. 
					Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus.
					Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque. Vivamus eget nibh. 
					Etiam cursus leo vel metus.
				</div>
			</div><!-- .acc-container -->

			<span class="acc-trigger">
				<a href="#">Aenean auctor wisi et urna</a>
			</span>

			<div class="acc-container">
				<div class="content">
					Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. 
					Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus.
					Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque. Vivamus eget nibh. 
					Etiam cursus leo vel metus.
				</div>
			</div><!-- .acc-container -->

			<span class="acc-trigger">
				<a href="#">Lorem ipsum dolor sit amet</a>
			</span>

			<div class="acc-container">
				<div class="content">
					Sed laoreet aliquam leo. Ut tellus dolor, dapibus eget, elementum vel, cursus eleifend, elit. 
					Aenean auctor wisi et urna. Aliquam erat volutpat. Duis ac turpis. Integer rutrum ante eu lacus.
					Vestibulum libero nisl, porta vel, scelerisque eget, malesuada at, neque. Vivamus eget nibh. 
					Etiam cursus leo vel metus.
				</div>					
			</div><!-- .acc-container -->

			<div class="divider"></div>
			
			<h6 class="widget-title">Buttons</h6>

			<a href="#" class="button yellow">Yellow Button</a>
			<a href="#" class="button orange">Orange Button</a>
			<a href="#" class="button red">Red Button</a>
			<a href="#" class="button pink">Pink Button</a>
			<a href="#" class="button purple">Purple Button</a>
			<a href="#" class="button emerald">Emerald Button</a>
			<a href="#" class="button lime">Lime Button</a>
			<a href="#" class="button green">Green Button</a>
			<a href="#" class="button dark-green">Dark Green Button</a>
			<a href="#" class="button mettalic-blue">Metallic Blue Button</a>
			<a href="#" class="button sky-blue">Sky Blue Button</a>
			<a href="#" class="button blue">Blue Button</a>
			<a href="#" class="button violet">Violet Button</a>
			<a href="#" class="button brown">Brown Button</a>
			<a href="#" class="button black">Black Button</a>
			<a href="#" class="button grey">Gray Button</a>
			<a href="#" class="button white">White Button</a>	


		</div><!--/ .one-half .last-->
		
		<!-- - - - - - - - - - - - - - end Elements - - - - - - - - - - - - - - - -->			
		
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
	
<? print wm.Include('footer.tpl'); ?>
</body>
</html>

