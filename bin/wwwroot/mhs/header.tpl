<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
	
    <meta http-equiv="X-UA-Compatible" content="edge" />
    <meta name="renderer" content="webkit|ie-comp|ie-stand">
	
	<link rel="icon" type="image/png" href="images/favicon.png" />
	
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<link rel="stylesheet" type="text/css" href="sliders/flexslider/flexslider.css" />
	<link rel="stylesheet" type="text/css" href="fancybox/jquery.fancybox.css" />

	<!-- HTML5 Shiv -->
	<script type="text/javascript" src="js/modernizr.custom.js"></script>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <meta name="description" content="Moon Http Server(MHS)是一个使用Pascal脚本的高性能Web服务器">
	<meta name="author" content="" />	

    <meta name="keywords" content="{keywords}">

	<!-- Title Variable -->
	<title>{title}</title>

	