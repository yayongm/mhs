<?
unit uView_Index;

interface

uses
  SysUtils, Variants, Classes, DB,
  BaseUI in '..\public\BaseUI.pp',
  BaseQuery in '..\public\BaseQuery.pp';

type 
  TIndexView = class
  public
    //侧边栏
	class procedure GetCatalog;
	class procedure GetUpcomingEvents;
	class procedure GetVideoNews;
	class procedure GetFacebookNews;
	class procedure GetHotNews;
	//指定地方使用的
	class procedure GetSearchWidget(ActionUrl: string = '');
	class procedure GetTestimonials;
	class procedure GetCalendarWidget;
  public
    //获取目录类型
    class function GetCatalogName: string;
    //获取分页,使用字符串拼接方式
    class function GetPagination(const ARecordCount: Integer; ARecordCountPerPage: Integer; APageUrl: string; AUlClass: string = ''): string;
    class function GetPaginationBySQL(ASQL: string; ARecordCountPerPage: Integer; APageUrl: string; AUlClass: string = ''): string;
  public
    //获取幻灯,使用字符串拼接方式
    class function GetSlidesText: string;
	//获取内容,直接打印方式
	class function GetContentText(const AContentPageUrl: string): Integer;
  end;

//侧边栏

implementation

class procedure TIndexView.GetCatalog;
begin
?>
			<div class="widget-container widget_categories">
				
				<h3 class="widget-title">目录</h3>
				
				<ul>
					
					<li><a href="./">新闻</a></li>
					<li><a href="blog-style-1.pp">博客</a></li>
					<li><a href="events.pp">教程</a></li>
					<li><a target="_blank" href="../lessons/">演示</a></li>
					
				</ul>
				
			</div><!--/ .widget-container-->
<?
end;

class procedure TIndexView.GetUpcomingEvents;
begin
?>
			<div class="widget-container eventsListWidget">

				<h3 class="widget-title">即将发布的新闻</h3>

				<ul>
					
					<li>
						<a href="#"><h6>喜羊羊表示MHS真的很赞</h6></a>
						<span class="widget-date">2月 29, 2019</span>
					
					</li>
					<li>
						<a href="#"><h6>迪迦奥特曼将出席MHS巡回推广活动</h6></a>
						<span class="widget-date">2月 30, 2029</span>
					</li>
					<li>
						<a href="#"><h6>塞伯坦星球表示将进一步深化小学生的MHS教育问题</h6></a>
						<span class="widget-date">2月 31, 2030</span>
					</li>
					
				</ul>
				
			</div><!--/ .widget-container-->

<?
end;

class procedure TIndexView.GetVideoNews;
begin
?>
			<div class="widget-container widget_video">
				
				<h3 class="widget-title">最新视频</h3>
				
				<div class="video-widget">
				<!--
					<iframe class="custom-frame" width="290" height="200" src="http://www.youtube.com/embed/jilRF0mocRQ?wmode=transparent" frameborder="0"></iframe><allowfullscreen></allowfullscreen>>
				-->	
				</div><!--/ .video-widget-->
				
				<div class="video-entry">
					<a href="#" class="video-title">
						<h5>计划中，不要等待。。。</h5>
					</a>
				</div><!--/ .video-entry-->
				
			</div><!--/ .widget-container-->
			
<?
end;

class procedure TIndexView.GetFacebookNews;
begin
?>
			<!--
			<div class="widget-container widget_facebook">
				
				<h3 class="widget-title">Become a Facebook Fan</h3>
				
				<div class="fb-like-box" data-href="http://www.facebook.com/pages/ThemeMakers/273813622709585" data-width="300" data-show-faces="true" data-stream="true" data-header="true"></div>
				
				<div id="fb-root"></div>
				<script>(function(d, s, id) {
				  var js, fjs = d.getElementsByTagName(s)[0];
				  if (d.getElementById(id)) return;
				  js = d.createElement(s); js.id = id;
				  js.src = "//connect.facebook.net/en_US/all.js#xfbml=1";
				  fjs.parentNode.insertBefore(js, fjs);
				}(document, 'script', 'facebook-jssdk'));</script>
				
				
			</div>--><!--/ .widget-container-->
			
<?
end;

class procedure TIndexView.GetHotNews;
begin
?>
			<div class="widget-container widget_popular_posts">
				
				<h3 class="widget-title">最热新闻</h3>
				
				<ul>
					
					<li>
						<a href="#"><h6>司机为躲酒精测试吃了路边一片草</h6></a>
						<span class="widget-date">2月 31, 2019, 5 评论</span>
					</li>
					<li>
						<a href="#"><h6>大妈网恋被骗60万，骗子是女婿</h6></a>
						<span class="widget-date">2月 31, 2019, 5 评论</span>
					</li>
					<li>
						<a href="#"><h6>男子为了不去上班，用刀插自己的腿</h6></a>
						<span class="widget-date">2月 31, 2019, 5 评论</span>
					</li>
					<li>
						<a href="#"><h6>毒贩做买卖，视力不好收冥币，一怒之下报警，双双落网</h6></a>
						<span class="widget-date">2月 29, 2019, 5 评论</span>
					</li>
					<li>
						<a href="#"><h6>男子借贷买手机一直没还钱，网贷催收人员竟把他亲生父母给找到了</h6></a>
						<span class="widget-date">2月 30, 2019, 5 评论</span>
					</li>
					<li>
						<a href="#"><h6>湖北男子误入传销窝点，因饭量过大遭强制遣返</h6></a>
						<span class="widget-date">2月 31, 2019, 5 评论</span>
					</li>
					
				</ul>
				
			</div><!--/ .widget-container-->

<?
end;

class procedure TIndexView.GetSearchWidget(ActionUrl: string);
var
  sKey, sUrl: string;
begin
  sKey := UrlDecode(Request.ContentFields.Values['searchkey']);
  sUrl := ActionUrl;
  if Length(sUrl) < 1 then
    sUrl := '#';

?>
			<div class="widget-container widget_search">

				<form action="<? print sUrl; ?>" id="searchform" name="searchform" method="post" role="search" />
					
					<fieldset>
						<input type="text" id="searchkey" name="searchkey" placeholder="查找" value="<? print sKey; ?>" />
						<button type="submit" id="searchsubmit"></button>
					</fieldset>
					
				</form><!--/ #searchform-->
			
			</div><!--/ .widget-container-->
			
<?
end;

class procedure TIndexView.GetTestimonials;
  procedure InnerGetTestimonials(const ACondition: string);
  var
    AQuery: TWmQuery;
    sSQL: string;
  begin
    if TBaseQry.DBQuerySel(AQuery, '*', 'usercomment', ACondition, Null) < 0 then
      Exit;
    if AQuery.RecordCount < 1 then
      Exit;    
    
    AQuery.DataSet.First;
    while not AQuery.DataSet.Eof do
    begin
?>      
							<li>
								<div class="quote-text">
									<? print AQuery.FS8['commenttext']; ?>
								</div><!--/ .quote-text-->
								<div class="quote-author">
									<? print AQuery.FS8['username']; ?>
									<span><? print AQuery.FS8['position']; ?></span>
								</div><!--/ .quote-author-->							
							</li>
<?      
      AQuery.DataSet.Next;
    end;
  end;

var
  iID: Integer;
begin
?>
			<div class="widget-container widget_testimonials">
				
				<h3 class="widget-title">热评</h3>
				
				<div class="testimonials">
					
					<div class="substrate-rotate-left"></div>
					<div class="substrate-rotate-right"></div>
					
					<div class="quoteBox">
						
						<ul class="quotes">
                            <? 
                            iID := StrToIntDef(mRPV('id'), -1);
                            if iID > 0 then
                              InnerGetTestimonials('(newsid=' + IntToStr(iID) + ') order by id desc limit 0, 3'); 

                            InnerGetTestimonials('(1=1) order by likecount desc limit 0, 3'); 
                            ?>
						</ul><!--/ .quotes-->
						
					</div><!--/ .quoteBox-->
					
				</div><!--/ .testimonials-->
				
			</div><!--/ .widget-container-->

<?
end;

class procedure TIndexView.GetCalendarWidget;
begin
?>
			<div class="widget-container widget_calendar">

				<h3 class="widget-title">日历</h3>

				<div id="calendar_wrap">

					<table id="wp-calendar">

						<caption>3月 2019</caption>

						<thead>
							<tr>
								<th scope="col" title="Monday">一</th>
								<th scope="col" title="Tuesday">二</th>
								<th scope="col" title="Wednesday">三</th>
								<th scope="col" title="Thursday">四</th>
								<th scope="col" title="Friday">五</th>
								<th scope="col" title="Saturday">六</th>
								<th scope="col" title="Sunday">日</th>
							</tr>
						</thead>

						<tfoot>
							<tr>
								<td colspan="3" id="prev"><a href="#" title="">2月</a></td>
								<td class="pad">&nbsp;</td>
								<td colspan="3" id="next" class="pad"><a href="#" title="">4月</a></td>
							</tr>
						</tfoot>

						<tbody>
							<tr>
								<td colspan="4" class="pad">&nbsp;</td>
								<td>1</td>
								<td>2</td>
								<td>3</td>
							</tr>
							<tr>
								<td>4</td>
								<td><a href="#">5</a></td>
								<td>6</td>
								<td>7</td>
								<td>8</td>
								<td id="today">9</td>
								<td>10</td>
							</tr>
							<tr>
								<td>11</td>
								<td>12</td>
								<td>13</td>
								<td>14</td>
								<td>15</td>
								<td>16</td>
								<td>17</td>
							</tr>
							<tr>
								<td>18</td>
								<td>19</td>
								<td>20</td>
								<td>21</td>
								<td>22</td>
								<td>23</td>
								<td>24</td>
							</tr>
							<tr>
								<td>25</td>
								<td>26</td>
								<td>27</td>
								<td>28</td>
								<td>29</td>
								<td>30</td>
								<td class="pad" colspan="1">&nbsp;</td>
							</tr>
						</tbody>

					</table><!--/ #wp-calendar-->

				</div><!--/ #calendar_wrap-->
				
			</div><!--/ .widget-container-->
			
<?
end;

class function TIndexView.GetCatalogName: string;
var
  iCatalog: Integer;
begin
  //['首页幻灯', '首页', '博客', '教程', '演示', '高级', '联系我们', '关于']),
  iCatalog := StrToIntDef(Request.QueryFields.Values['catalog'], -1);
  case iCatalog of
    0: Result := '幻灯详情';
	1: Result := '新闻详情';
	2: Result := '博客详情';
	3: Result := '教程详情';
	4: Result := '演示详情';
    else
	  Result := '详情';
  end;
end;

class function TIndexView.GetPagination(const ARecordCount: Integer; ARecordCountPerPage: Integer; APageUrl: string; AUlClass: string = ''): string;
var
  iRecordCount, iPages, iCurPage, iTemp: Integer;
  
  aryPages: array[0..4] of Integer;
begin
  Result := '';

  iRecordCount := ARecordCount;
  
  iCurPage := StrToIntDef(Request.QueryFields.Values['page'], 1);
  
  iPages := (iRecordCount + ARecordCountPerPage - 1) div ARecordCountPerPage;
  //writeln(iPages);
  if iPages < 2 then
    Exit;

  aryPages[0] := iCurPage - 2;
  aryPages[1] := iCurPage - 1;
  aryPages[2] := iCurPage;
  aryPages[3] := iCurPage + 1;
  aryPages[4] := iCurPage + 2;
  
  iTemp := Length(AUlClass);
  if iTemp > 0 then
    Result := Result + '		<ul ' + AUlClass + '>' + sLineBreak;
  
  if aryPages[0] > 0 then
    Result := Result + '			<li><a class="prevpostslink" href="' + APageUrl + IntToStr(aryPages[0]) + '"><span>&larr;</span>前一页</a></li>' + sLineBreak;  
  if aryPages[1] > 0 then
    Result := Result + '			<li><a href="' + APageUrl + IntToStr(aryPages[1]) + '">' + IntToStr(aryPages[1]) + '</a></li>' + sLineBreak;
  if aryPages[2] > 0 then
    Result := Result + '<li><a class="current" href="' + APageUrl + IntToStr(aryPages[2]) + '">' + IntToStr(aryPages[2]) + '</a></li>' + sLineBreak;  
  if aryPages[3] <= iPages then
    Result := Result + '			<li><a href="' + APageUrl + IntToStr(aryPages[3]) + '">' + IntToStr(aryPages[3]) + '</a></li>' + sLineBreak;
  if aryPages[4] <= iPages then
    Result := Result + '			<li><a class="nextpostslink" href="' + APageUrl + IntToStr(aryPages[4]) + '">后一页 <span>&rarr;</span></a></li>' + sLineBreak;  

  if iTemp > 0 then
    Result := Result + '</ul><!--/ .pagination-->' + sLineBreak;
end;

class function TIndexView.GetPaginationBySQL(ASQL: string; ARecordCountPerPage: Integer; APageUrl: string; AUlClass: string = ''): string;
var
  AQuery: TWmQuery;
  iRecordCount: Integer;
begin
  Result := '';

  if TBaseQry.DBQuery(AQuery, ASQL, VarArrayOf([])) >= 0 then
  begin
    iRecordCount := AQuery.Fields[0].Value;
    //FreeAndNil(AQuery);
  end
  else
    iRecordCount := 0;
  
  Result := GetPagination(iRecordCount, ARecordCountPerPage, APageUrl, AUlClass);
end;

class function TIndexView.GetSlidesText: string;
var
  AQuery: TWmQuery;
  sTemp: string;
begin
  Result := '';
  
  if TBaseQry.DBQuerySel(AQuery, '*', 'article', '(catalog=0) and (type < 3) order by createtime desc limit 5', Null) < 0 then
    Exit;
  
  //Result := Result + TBaseQry.DebugDataSet(AQuery.DataSet);

  if AQuery.RecordCount < 1 then
    Exit;
	
  AQuery.DataSet.First;
  while not AQuery.DataSet.Eof do
  begin
    //sTemp := 'id=' + DbUtf8(AQuery.FieldByName('id').AsString) + '&catalog=' + DbUtf8(AQuery.FieldByName('catalog').AsString);
    sTemp := 'id=' + DbUtf8(AQuery.FS['id']) + '&catalog=' + DbUtf8(AQuery.FS['catalog']);

Result := Result + '				<li>' + sLineBreak +
                   '                    <a href="single-post.pp?' + sTemp + '">' + sLineBreak +
                   '					<img src="' + DbUtf8(AQuery.FS['normalimg']) + '" alt="" /></a>' + sLineBreak +
                   '					<div class="caption">' + sLineBreak +
                   '						<div class="caption-entry">' + sLineBreak +
                   '							<div class="caption-title"><h2>' + DbUtf8(AQuery.FS['title']) + '</h2></div>' + sLineBreak +
                   '							<p>' + sLineBreak +
                   DbUtf8(AQuery.FS['summary']) + sLineBreak +
                   '							</p>' + sLineBreak +
                   '						</div><!--/ .caption-entry-->' + sLineBreak +
                   '					</div><!--/ .caption-->' + sLineBreak +
                   '				</li>' + sLineBreak;

    AQuery.DataSet.Next;
  end;
  
end;

class function TIndexView.GetContentText(const AContentPageUrl: string): Integer;
var
  AQuery: TWmQuery;
  sImg: string;
  sTemp, sKey: string;
  iPageCount, iPage: Integer;
begin
  Result := -1;
  
  iPageCount := 5;
  iPage := StrToIntDef(Request.QueryFields.Values['page'], 1) - 1;
  sKey := Utf8Decode(UrlDecode(Request.ContentFields.Values['searchkey']));
  
  if Length(sKey) > 0 then
  begin
    //计算记录个数并返回给分页进行分页处理
    sTemp := '%' + sKey + '%';
    
    if TBaseQry.DBQuerySel(AQuery, 'Count(*)', 'article', '(catalog=1) and (type < 3) and ((title like :title) or (keywords like :keywords))',
      VarArrayOf([sTemp, sTemp])) < 0 then
      Exit;
    Result := AQuery.Fields[0].AsInteger;
    
    //获取数据
    if TBaseQry.DBQuerySel(AQuery, '*', 'article', '(catalog=1) and (type < 3) and ((title like :title) or (keywords like :keywords)) order by type, createtime desc limit :Start, :End', 
      VarArrayOf([sTemp, sTemp, iPage * iPageCount, iPageCount])) < 0 then
      Exit;
  end  
  else
  begin  
    if TBaseQry.DBQuerySel(AQuery, 'Count(*)', 'article', '(catalog=1) and (type < 3)', Null) < 0 then
      Exit;
    Result := AQuery.Fields[0].AsInteger;

    if TBaseQry.DBQuerySel(AQuery, '*', 'article', '(catalog=1) and (type < 3) order by type, createtime desc limit :Start, :End', 
      VarArrayOf([iPage * iPageCount, iPageCount])) < 0 then
      Exit;
  end;
  
  //Result := Result + TBaseQry.DebugDataSet(AQuery.DataSet);

  if AQuery.RecordCount < 1 then
    Exit;
	
  AQuery.DataSet.First;
  while not AQuery.DataSet.Eof do
  begin
    	sTemp := AContentPageUrl + 'id=' + AQuery.FS8['id'] + '&catalog=' + AQuery.FS8['catalog'];
?>  
			<article class="post-item clearfix">
				
				<a href="<? print sTemp; ?>">
					<h3 class="title">
						<? print AQuery.FS8['title']; ?>
					</h3><!--/ .title -->
				</a>
				
				<section class="post-meta clearfix">
					
					<div class="post-date"><a href="#"><? print FormatDateTime('yy-mm-dd hh:mm:ss', AQuery.FD['createtime']); ?></a></div><!--/ .post-date-->
					<div class="post-tags">
						<a href="#"><? print AQuery.FS8['keywords']; ?></a>
						<!--
						<a href="#">News</a>
						<a href="#">Events</a>
						<a href="#">People</a>
						-->
					</div><!--/ .post-tags-->
					<div class="post-comments"><a href="#"><? print AQuery.FS8['viewcount']; print '&nbsp;'; ?>阅读</a></div><!--/ .post-comments-->
					
				</section><!--/ .post-meta-->
				
				<?
					sImg := Trim(AQuery.FS8['normalimg']);
					if Length(sImg) > 8 then
					begin
						print '				<a class="single-image" href="' + sImg + '">';
						print '					<img class="custom-frame" alt="" src="' + sImg + '" />';
						print '				</a>';
					end;
				?>
				
				<p>
					<? print AQuery.FS8['summary']; ?>
				</p>
				
				<a href="<? print sTemp; ?>" class="button gray">阅读更多... &rarr;</a>
				
			</article><!--/ .post-item-->
			



<?
    AQuery.DataSet.Next;
  end;
  

end;

end.
?>