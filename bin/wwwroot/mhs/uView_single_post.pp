<?
unit uView_single_post;

interface

uses
  SysUtils, Variants, Classes, DB,
  BaseUI in '..\public\BaseUI.pp',
  BaseQuery in '..\public\BaseQuery.pp';
  
type
  //注意必须从TComponent继承,然后让wm去对该对象进行管理
  TSinlgePostView = class
  public
    class function GetContentById(var AQuery: TWmQuery): Integer;
    class procedure GetContentTextFromQuery(AQuery: TWmQuery);
    class procedure GetComments;
    class function PostComments: Boolean;
  end;  

implementation

class function TSinlgePostView.GetContentById(var AQuery: TWmQuery): Integer;
var
  iID: Integer;
begin
  iID := StrToIntDef(Request.QueryFields.Values['id'], -1);
  
  if iID < 1 then
  begin
    Result := -3;
  
    Exit;
  end;
  
  Result := TBaseQry.DBQuerySel(AQuery, '*', 'article', '(id=:id) and (type < 3)', VarArrayOf([iID]));
  //writeln(Result);
end;

class procedure TSinlgePostView.GetContentTextFromQuery(AQuery: TWmQuery);
var
  iID: Integer;
begin
  if not (Assigned(AQuery) and (AQuery.Active) and (AQuery.RecordCount = 1)) then
  begin
    print '主题没找到';
  
    Exit;
  end;
  
  TBaseQry.DbExecUpd('article', 'viewcount = viewcount + 1', '(id=:id)', VarArrayOf([AQuery.FI['id']]));
  
?>
			<article class="post clearfix">
				
				<h3 class="title">
					<? print AQuery.FS8['title']; ?>
				</h3><!--/ .title -->
				
				<section class="post-meta clearfix">
					
					<div class="post-date"><a href="#"><? print FormatDateTime('yy-mm-dd hh:mm:ss', AQuery.FD['createtime']); ?></a></div><!--/ .post-date-->
					<div class="post-tags">
						<a href="#"><? print AQuery.FS8['keywords']; ?></a>
						<!--
						<a href="#">News</a>
						<a href="#">Events</a>
						<a href="#">People</a>
						-->
					</div><!--/ .post-tags-->
					<div class="post-comments"><a href="#"><? print AQuery.FS8['viewcount']; print ' 阅读'; ?></a></div><!--/ .post-comments-->
					
				</section><!--/ .post-meta-->
				<? print AQuery.FS8['content']; ?>

			</article><!--/ .post-->
<?
end;

class procedure TSinlgePostView.GetComments;
var
  AQuery: TWmQuery;
  iID, iCount: Integer;
begin
  iID := StrToIntDef(mRPV('id'), -1);
  
  if iID < 1 then
  begin
    iCount := 0;
  end
  else
  begin
    if TBaseQry.DBQuerySel(AQuery, '*', 'usercomment', '(newsid=:newsid) order by id', VarArrayOf([iID])) < 0 then
      Exit;
      
    iCount := AQuery.RecordCount;
  end;
  print '				<h5>' + IntToStr(iCount) + ' Comments</h5>';
  print '				<ol class="comments-list">';

  if iCount > 0 then
  begin
  
    AQuery.DataSet.First;
    while not AQuery.DataSet.Eof do
    begin
?>
					<li class="comment">

						<article>
							<img src="images/gravatar.png" alt="avatar" class="avatar" />
							<div class="comment-entry">
								<div class="comment-meta">
									<h6 class="author"><a href="#">
                                      <? 
                                        print AQuery.FS8['username'];  
                                        if Length(AQuery.FS8['position']) > 0 then
                                          print ' - ' + AQuery.FS8['position'];
                                      ?>
                                      </a></h6>
									<p class="date"><? print DbDateToStr(AQuery.FD['createtime']); ?></p>
								</div><!--/ .comment-meta -->
								<div class="comment-body">
									<p>
										<? print AQuery.FS8['commenttext']; ?>
									</p>
								</div><!--/ .comment-body -->
							</div><!--/ .comment-entry-->

						</article>
                        
                    </li><!--/ .comment-->
<?  
      AQuery.DataSet.Next;
    end;
  end;
  
  print '				</ol><!--/ .comments-list-->';
  
end;

class function TSinlgePostView.PostComments: Boolean;
var
  AQuery: TWmQuery;
  iID, iCount: Integer;
  sUserName, sUserMobile, sUserMail, sCommentText: string;
begin
  Result := False;
  
  iID := StrToIntDef(mRPV('id'), -1);
  
  if iID < 1 then
    Exit;
    
  sUserName := Trim(mRPV('username'));
  sUserMobile := Trim(mRPV('usermobile'));
  sCommentText := Copy(Trim(mRPV('commenttext')), 1, 2048);//限制长度
  sUserMail := Trim(mRPV('usermail'));

  if (sUserName = '') or (sUserMobile = '') or (sCommentText = '') then
    Exit;
    
  if TBaseQry.DBQuery(AQuery, 'select * from usercomment where (id=-1)', Null) < 0 then
    Exit;

  AQuery.Append;
  AQuery.FS['commenttext'] := StringReplaceAll(sCommentText, sLineBreak, '<br>');
  TBaseQry.FillRecord(AQuery, ['username', 'usermobile', 'usermail', 'position'], [sUserName, sUserMobile, sUserMail, mRPV('position')]);
  TBaseQry.FillRecord(AQuery, ['newsid', 'parentid', 'likecount', 'hatecount', 'createtime'], [iID, 0, 0, 0, Now]);
  AQuery.Post;
  
  Result := True;
end;


end.
?>