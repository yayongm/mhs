<?
unit uView_blog_style_1;

interface

uses
  SysUtils, Variants, Classes, DB,
  BaseQuery in '..\public\BaseQuery.pp';
  
type
  TBlogSytle1View = class
  public
    class function GetContentText(const AContentPageUrl: string): Integer;
  end;  

implementation

class function TBlogSytle1View.GetContentText(const AContentPageUrl: string): Integer;
var
  AQuery: TWmQuery;
  iID, iPage, iPageCount: Integer;
  sImg: string;
  sKey, sTemp, sSQL: string;
begin
  Result := -1;
  
  iPageCount := 10;
  iID := StrToIntDef(Request.QueryFields.Values['id'], -1);
  iPage := StrToIntDef(Request.QueryFields.Values['page'], 1) - 1;
  sKey := Utf8Decode(UrlDecode(Request.ContentFields.Values['searchkey']));
  
  if Length(sKey) > 0 then
  begin
    sTemp := '%' + sKey + '%';

    if TBaseQry.DBQuerySel(AQuery, 'Count(*)', 'article', '(catalog=2) and (type < 3) and ((title like :title) or (keywords like :keywords)) order by type, createtime desc', 
      VarArrayOf([sTemp, sTemp])) < 0 then
      Exit;
    Result := AQuery.Fields[0].AsInteger;  

    if TBaseQry.DBQuerySel(AQuery, '*', 'article', '(catalog=2) and (type < 3) and ((title like :title) or (keywords like :keywords)) order by type, createtime desc limit :Start, :End', 
      VarArrayOf([sTemp, sTemp, iPage * iPageCount, iPageCount])) < 0 then
      Exit;
  end  
  else
  begin  
    if TBaseQry.DBQuerySel(AQuery, 'Count(*)', 'article', '(catalog=2) and (type < 3) order by type, createtime desc', 
      VarArrayOf([])) < 0 then
      Exit;
    Result := AQuery.Fields[0].AsInteger;  

    if TBaseQry.DBQuerySel(AQuery, '*', 'article', '(catalog=2) and (type < 3) order by type, createtime desc limit :Start, :End', 
      VarArrayOf([iPage * iPageCount, iPageCount])) < 0 then
      Exit;
  end;
  
  if AQuery.RecordCount < 1 then
	Exit;
 
  AQuery.DataSet.First;
  while not AQuery.DataSet.Eof do
  begin
	sTemp := AContentPageUrl + 'id=' + AQuery.FS8['id'] + '&catalog=' + AQuery.FS8['catalog'];
?>  
			<article class="post clearfix">
				<a href="<? print sTemp; ?>">
					<h3 class="title">
						<? print AQuery.FS8['title']; ?>
					</h3><!--/ .title -->
				</a>
				
				<section class="post-meta clearfix">
					
					<div class="post-date"><a href="#"><? print FormatDateTime('yy-mm-dd hh:mm:ss', AQuery.FD['createtime']); ?></a></div><!--/ .post-date-->
					<div class="post-tags">
						<a href="#"><? print AQuery.FS8['keywords']; ?></a>
						<!--
						<a href="#">News</a>
						<a href="#">Events</a>
						<a href="#">People</a>
						-->
					</div><!--/ .post-tags-->
					<div class="post-comments"><a href="#"><? print AQuery.FS8['viewcount']; print ' 阅读'; ?></a></div><!--/ .post-comments-->
					
				</section><!--/ .post-meta-->
				
				<?
					sImg := Trim(AQuery.FS8['normalimg']);
					if Length(sImg) > 0 then
					begin
						print '				<a class="single-image" href="' + sImg + '">';
						print '					<img class="custom-frame" alt="" src="' + sImg + '" />';
						print '				</a>';
					end;
				?>
				
				<p>
					<? print AQuery.FS8['summary']; ?>
				</p>
				
				<a href="<? print sTemp; ?>" class="button gray">阅读更多... &rarr;</a>
				
			</article><!--/ .post-->

<?  
	AQuery.DataSet.Next;
  end;  
end;
?>




<?


end.
?>