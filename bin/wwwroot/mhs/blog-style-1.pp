<?
uses
  SysUtils, Variants, Classes, DB,
  uView_Index, uView_blog_style_1,
  uView_Header;

var
  iRecordCount: Integer;
  
begin
print wm.Include('header.tpl', '{title}=MHS | 博客');
?>

</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<?
  THeaderView.GetHeaderText('blog-style-1');
?>
	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container sbr clearfix">
		
		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	
		
		<div class="page-header">
			
			<h1 class="page-title">博客</h1>
			
		</div><!--/ .page-header-->
		
		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	
		

		<!-- - - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->		
		
		<section id="content" class="first">

			<? iRecordCount := TBlogSytle1View.GetContentText('single-post.pp?'); ?>
			
            <?
            println TIndexView.GetPagination(iRecordCount, 10, 'blog-style-1.pp?page=', 'class="pagination"');
            //println TIndexView.GetPaginationBySQL('select count(*) from article where (catalog=2) and (type < 3)', 10, 'blog-style-1.pp?page=', 'class="pagination"');
            ?>
		
		</section><!--/ #content-->
		
		<!-- - - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - - - - - - - Sidebar - - - - - - - - - - - - - - - - -->	
		
		<aside id="sidebar">
			
			<?
				TIndexView.GetSearchWidget('blog-style-1.pp');
				TIndexView.GetUpcomingEvents;
				TIndexView.GetCatalog;
				TIndexView.GetVideoNews;
				TIndexView.GetCalendarWidget;
				TIndexView.GetHotNews;
				TIndexView.GetTestimonials;
			?>
			
		</aside><!--/ #sidebar-->
		
		<!-- - - - - - - - - - - - - end Sidebar - - - - - - - - - - - - - - - - -->
		
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
<? print wm.Include('footer.tpl'); ?>
</body>
</html>

<? 
end.
?>

