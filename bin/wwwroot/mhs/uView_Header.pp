﻿<?
unit uView_Header;

interface

uses
  SysUtils, Variants, Classes, DB,
  BaseUI in '..\public\BaseUI.pp',
  BaseQuery in '..\public\BaseQuery.pp';
  
type
  THeaderView = class
    class function GetHeaderText(ACurPage: string): string;
  public
    //记录访问日志
    class function WriteVisitLog: Boolean;    
  end;  

implementation

class function THeaderView.WriteVisitLog: Boolean;    
var
  s: string;
begin
  Result := False;
  s := Request.Query;
  if Length(s) > 0 then
    s := Request.URL + '?' + s
  else
    s := Request.URL;
    
  if TBaseQry.DbExec('Insert into visit_log(ip, url, refer, useragent) values(:ip, :url, :refer, :useragent);',
    VarArrayOf([Request.RemoteAddr, s, Request.Referer, Request.UserAgent, Now])) > 0 then
    Result := True;
end;

class function THeaderView.GetHeaderText(ACurPage: string): string;

  function GetLiA(ADestPage: string): string;
  begin	  
	if SameText(ACurPage, ADestPage) then
      Result := '<li class="current" selected>'
    else  
      Result := '<li>'; 
	
	print Result;
  end;

begin
  WriteVisitLog;
?>
	<!-- - - - - - - - - - - - - - Header - - - - - - - - - - - - - - - - -->

	<header id="header" class="clearfix">

		<a href="index.pp" id="logo"><img src="images/logo.png" alt="" title="Logo" /></a>

		<ul class="social-links clearfix">
			<li class="twitter"><a href="#">Twitter<span></span></a></li>
			<li class="facebook"><a href="#">Facebook<span></span></a></li>
			<li class="dribbble"><a href="#">Dribbble<span></span></a></li>
			<li class="vimeo"><a href="#">Vimeo<span></span></a></li>
			<li class="youtube"><a href="#">YouTube<span></span></a></li>
			<li class="rss"><a href="#">Rss<span></span></a></li>
		</ul><!--/ .social-links-->

		<nav id="navigation" class="navigation">

			<ul>
                <? GetLiA('index'); ?><a href="index.pp">首页</a></li>
                <? GetLiA('blog-style-1'); ?><a href="blog-style-1.pp">博客</a>
					<!--
					<ul>
						<li><a href="blog-style-1.pp">Blog Page</a></li>
						<li><a href="blog-style-2.pp">Alternative Blog Page</a></li>
						<li><a href="single-post.pp">Blog Single</a></li>
					</ul>
					-->
				</li>
                <? GetLiA('download'); ?><a href="download.pp">下载</a></li>
                <? GetLiA('Gallery'); ?><a target="_blank" href="../lessons/">教程</a></li>
                <? GetLiA('contacts'); ?><a target="_blank" href="../admin/login.pp">后台管理</a></li>
                <? GetLiA('about'); ?><a href="about.pp">关于</a></li>
                <? GetLiA('events'); ?><a href="events.pp">案例</a>
					<!--
					<ul>
						<li><a href="events.pp">Events Page</a></li>
						<li><a href="single-events.pp">Detailed Event</a></li>
					</ul>
					-->
				</li>
                <? GetLiA('elements'); ?><a href="elements.pp">高级</a>
					<!--
					<ul>
						<li><a href="elements.pp">Elements</a></li>
						<li><a href="typography.pp">Typography</a></li>
						<li><a href="columns.pp">Columns</a></li>
						<li><a href="sidebar-left.pp">Sidebar Left</a></li>
					</ul>
					-->
				</li>
			</ul>

			<a href="#" class="donate">捐助</a>

		</nav><!--/ #navigation-->

	</header><!--/ #header-->

	<!-- - - - - - - - - - - - - - end Header - - - - - - - - - - - - - - - - -->
<?
end;
?>

<?
end.
?>
