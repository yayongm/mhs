<?
uses
  uView_Header;

print wm.Include('header.tpl', '{title}=MHS | 陈列室');
?>

</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<?
  THeaderView.GetHeaderText('gallery');
?>
	
	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container clearfix">
		
		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	
		
		<div class="page-header">
			
			<h1 class="page-title">Gallery</h1>
			
		</div><!--/ .page-header-->
		
		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - - Portfolio Filter - - - - - - - - - - -->	
		
		<ul id="portfolio-filter">

			<li><a data-categories="*">View all</a></li>
			<li><a data-categories="2007">2007</a></li>
			<li><a data-categories="2009">2009</a></li>
			<li><a data-categories="2012">2012</a></li>

		</ul><!--/ end #portfolio-filter -->
		
		<!-- - - - - - - - - end Portfolio Filter - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - - Portfolio Items - - - - - - - - - - -->	

		<section id="portfolio-items">

			<article class="one-fourth" data-categories="2007">

				<a href="images/gallery/fullscreen/01.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-1.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">Consectetur Ipsum</h6>
				</a>					

			</article><!--/ .one-fourth -->

			<article class="one-fourth" data-categories="2009">

				<a href="images/gallery/fullscreen/02.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-2.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">We Are Happy!</h6>
				</a>	

			</article><!--/ .one-fourth -->

			<article class="one-fourth" data-categories="2012">

				<a href="images/gallery/fullscreen/03.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-3.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">Eleifend Pretium Dolor</h6>
				</a>	

			</article><!--/ .one-fourth -->

			<article class="one-fourth" data-categories="2007">

				<a href="images/gallery/fullscreen/09.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-9.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">Fusce Ultrices</h6>
				</a>						

			</article><!--/ .one-fourth -->

			<article class="one-fourth" data-categories="2009">

				<a href="images/gallery/fullscreen/05.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-5.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">We Are Happy!</h6>
				</a>	

			</article><!--/ .one-fourth -->

			<article class="one-fourth" data-categories="2012">

				<a href="images/gallery/fullscreen/06.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-6.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">Eleifend Pretium Dolor</h6>
				</a>	

			</article><!--/ .one-fourth -->

			<article class="one-fourth" data-categories="2007">

				<a href="images/gallery/fullscreen/07.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-7.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">Eleifend Pretium Dolor</h6>
				</a>	

			</article><!--/ .one-fourth -->

			<article class="one-fourth" data-categories="2009">

				<a href="images/gallery/fullscreen/08.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-8.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">Eleifend Pretium Dolor</h6>
				</a>	

			</article><!--/ .one-fourth -->

			<article class="one-fourth" data-categories="2012">

				<a href="images/gallery/fullscreen/04.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-4.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">We Are Happy!</h6>
				</a>	

			</article><!--/ .one-fourth -->

			<article class="one-fourth" data-categories="2007">

				<a href="images/gallery/fullscreen/10.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-10.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">Eleifend Pretium Dolor</h6>
				</a>	

			</article><!--/ .one-fourth -->

			<article class="one-fourth" data-categories="2009">

				<a href="images/gallery/fullscreen/11.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-11.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">Eleifend Pretium Dolor</h6>
				</a>	

			</article><!--/ .one-fourth -->

			<article class="one-fourth" data-categories="2012">

				<a href="images/gallery/fullscreen/12.jpg" class="single-image picture-icon" rel="gallery_group">
					<img src="images/gallery/thumb/thumb-4col-12.jpg" alt="" />
				</a>

				<a href="#" class="project-meta">
					<h6 class="title-item">Eleifend Pretium Dolor</h6>
				</a>	

			</article><!--/ .one-fourth -->

		</section><!--/ #portfolio-items-->
		
		<!-- - - - - - - - - - end Portfolio Items - - - - - - - - - - -->
		
		
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
	
<? print wm.Include('footer.tpl'); ?>
</body>
</html>

