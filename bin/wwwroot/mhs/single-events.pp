<?
uses
  SysUtils, Variants, Classes, DB,
  uMhsBase,
  uView_Index, uView_single_events,  
  uView_Header;

{
//1.错误示例:请不要在此处定义实体对象.
//因为改单元功能类似于DPR文件,即工程组织文件,定义在此处的全局变量会出现各种问题.
//所以,请不要在此处定义实体对象

//2.因数据库原因,主要是玩不转ckeditor,所以,把展示的内容直接弄到summary字段里了.本应分开弄到content字段里的.
//所以,请不要对此演示做不必要的联想.能玩得转的也请玩好后更新到群里一起来嗨吧.
var
  ASEV: TSinlgeEventsView;
}

procedure DoAction;
var
  ASEV: TSinlgeEventsView;
begin
  //先实例化SingleEventView
  ASEV := NewCurentView;
  //查询数据
  ASEV.GetContentById;
end;

procedure DoViewHeader;
var
  AQuery: TWmQuery;
  sTemp: string;
begin
  AQuery := CurrentView.qryMain;
  
  if AQuery.RecordCount > 0 then
  begin
    sTemp := '{title}=MHS | ' + AQuery.FS8['title'] + sLineBreak + '{keywords}=' + AQuery.FS8['keywords'];
  end  
  else
  begin
    sTemp := '{title}=MHS | 案例详情';
  end;  

  print wm.Include('header.tpl', sTemp);
end;

//初始化View,或者控制页面
DoAction;

//显示标题
DoViewHeader;

?>

</head>	
<body class="style-1">
	
<div class="wrap-header"></div><!--/ .wrap-header-->

<div class="wrap">
	
<?
  THeaderView.GetHeaderText('events');
?>
	
	<!-- - - - - - - - - - - - - - - Container - - - - - - - - - - - - - - - - -->	
	
	<section class="container sbr clearfix">
		
		<!-- - - - - - - - - - Page Header - - - - - - - - - - -->	
		
		<div class="page-header">
			
			<h1 class="page-title"><? print CurrentView.qryMain.FS8['title']; ?></h1>
			
		</div><!--/ .page-header-->
		
		<!-- - - - - - - - - end Page Header - - - - - - - - - -->	
		

		<!-- - - - - - - - - - - - - - - Content - - - - - - - - - - - - - - - - -->		
		
		<section id="content">
			
			<section class="content-event">
				
				<article class="event-item detailed clearfix">
					
					<a href="events.pp" class="button gray">&larr; 返回案例页</a>	
					
						<? CurrentView.GetContentTextFromQuery; ?>
                        
				</article><!--/ .event-item-->
				
				<a href="#" class="button gray alignleft">&larr; 上一案例</a>
				<a href="#" class="button gray alignright">下一案例 &rarr;</a>
					
			</section><!--/ content-event-->
			
		</section><!--/ #content-->
		
		<!-- - - - - - - - - - - - - - end Content - - - - - - - - - - - - - - - - -->	
		
		
		<!-- - - - - - - - - - - - - - - Sidebar - - - - - - - - - - - - - - - - -->	
		
		<aside id="sidebar">
			
			<? 
				TIndexView.GetSearchWidget;
				TIndexView.GetUpcomingEvents;
				TIndexView.GetCatalog; 
				TIndexView.GetVideoNews;
				TIndexView.GetHotNews;
			?>
			
		</aside><!--/ #sidebar-->
		
		<!-- - - - - - - - - - - - - end Sidebar - - - - - - - - - - - - - - - - -->
		
		
	</section><!--/.container -->
		
	<!-- - - - - - - - - - - - - end Container - - - - - - - - - - - - - - - - -->	
	
	
<? print wm.Include('footer.tpl'); ?>

<!--
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&AMP;language=en"></script>
<script type="text/javascript">
  var stockholm = new google.maps.LatLng(59.32522, 18.07002);
  var parliament = new google.maps.LatLng(59.327383, 18.06747);
  var marker;
  var map;

  function initialize() {
    var mapOptions = {
      zoom: 16,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      center: stockholm
    };

    map = new google.maps.Map(document.getElementById("map-canvas"),
            mapOptions);
          
    marker = new google.maps.Marker({
      map:map,
      draggable:true,
      animation: google.maps.Animation.DROP,
      position: parliament
    });
    google.maps.event.addListener(marker, 'click', toggleBounce);
  }

	initialize();
	
  function toggleBounce() {

    if (marker.getAnimation() != null) {
      marker.setAnimation(null);
    } else {
      marker.setAnimation(google.maps.Animation.BOUNCE);
    }
  }
</script>
-->
</body>
</html>

